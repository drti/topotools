# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import numpy as np

import Muscat.Helpers.ParserHelper as PH
from Muscat.Helpers.Logger import Info
import Muscat.Containers.ElementsDescription as ED

import OpenPisco.TopoZones as TZ
import OpenPisco.PhysicalSolvers.FieldsNames as FN
from OpenPisco.PhysicalSolvers.AsterSolverBase import AsterSolverBase
from OpenPisco.PhysicalSolvers.AsterAnalysisSolverFactory import RegisterAsterClass
import OpenPisco.ExternalTools.Aster.AsterCommonWriter as AsterCommonWriter
import OpenPisco.ExternalTools.Aster.AsterModalWriter as AsterModalWriter

def CreateAsterModalPhysicalProblem(ops):
     res = AsterModal()
     del(ops['type'])

     for tag,child in ops["children"]:
        if tag.lower() == "material":
           if "eTag" in child:
              z = child["eTag"]
           else:
              z = "AllZones"
           material = {}
           material["young"] = PH.ReadFloat(child["young"])
           material["poisson"] = PH.ReadFloat(child["poisson"])
           material["density"] = PH.ReadFloat(child["rho"])
           res.materials.append( [z,material])

        elif tag.lower() == "dirichlet":
             if "eTag" in child:
                z = child["eTag"]
             if "dofs" in child and "value" in child:
                dofs = PH.ReadInts(child["dofs"])
                val = PH.ReadFloat(child["value"])
                U=[]
                for i in range(3):
                    if i in dofs:
                       U.append(val)
                    else:
                       U.append(None)
                res.dirichlet.append([z,U])
             else:
                raise Exception("Need a set of dofs (dofs) and a value (value) for dirichlet boundary condition")
     del ops["children"]
     PH.ReadProperties(ops,ops.keys(),res)
     return res


class AsterModal(AsterSolverBase):
    def __init__(self,modalParams):
      super(AsterModal,self).__init__()
      self.name = 'AsterModal'
      self.expectedParameters=["Numbermaxfreq"]
      if any(Parameter not in self.expectedParameters for Parameter in modalParams.keys()):
            Info("The following parameters are not available for a modal analysis ",modalParams.keys()-set(self.expectedParameters))
      elif any(Parameter not in modalParams.keys() for Parameter in self.expectedParameters):
            raise Exception("The following parameters were expected for a modal analysis ",set(self.expectedParameters)-modalParams.keys())

      assert self.conform,"Ersatz material approach in dynamic not handled (yet!)"

      self.modalParams=modalParams
      self.criteriaParams= None
      self.auxiliaryFieldGeneration[FN.EigenFreqSquared_sensitivity][FN.Nodes] = False
      self.auxiliaryScalarsbyOrderGeneration = {FN.EigenFreqSquared:False}

    def WriteParametersInput(self,writeFile):
        self.solverInterface.SetExtraFiles(True)
        AsterModalWriter.WriteMaterialParametersInput(writeFile,self.materials)
        dimensionality = self.originalSupport.GetElementsDimensionality()
        AsterCommonWriter.WriteDirichletParametersInput(writeFile,self.dirichlet,dimensionality)
        AsterCommonWriter.WriteVariable(writeFile=writeFile,
                                        variableName="NMAX_FREQ",
                                        variableValue="{'NMAX_FREQ' : "+str(self.modalParams['Numbermaxfreq'])+"}")
        if self.criteriaParams is not None:
            self.WriteCriteriaParametersInput(writeFile)

    def GetNodalSolution(self,i):
        solution = self.GetField("DEPL","nodes",i)
        self.u =self.ExtendingNodalFieldToOriginalSupport(solution)
        return self.u

    def GetNumberOfSolutions(self):
        return self.modalParams["Numbermaxfreq"]

    def WriteCriteriaParametersInput(self,writeFile):
        errorMessage = "The criteria can not ask for more eigenvalues than the physical analysis!"
        assert self.criteriaParams['NumModeOptim']<=self.modalParams['Numbermaxfreq'],errorMessage
        #Alternative using conversion in string to ignore detail of criteria (responsability of Aster script)
        writeFile.write("modalcritParams =")
        writeFile.write(str(self.criteriaParams)+"\n")

RegisterAsterClass("modal_elastic", AsterModal,CreateAsterModalPhysicalProblem)

from Muscat.Containers.MeshCreationTools import CreateCube
from OpenPisco.ExternalTools.Aster.AsterInterface import SkipAsterTest
from OpenPisco.Unstructured.Levelset import LevelSet

def CheckIntegrity(GUI=False):
    skipTestPhase,message = SkipAsterTest()
    if skipTestPhase:
        return message

    mesh = CreateCube(dimensions=[5,5,5],spacing=[1./4, 1./4, 1./4],origin=[0, 0, 0],ofTetras=True)
    tetra = mesh.GetElementsOfType(ED.Tetrahedron_4)
    tetra.tags.CreateTag(TZ.Inside3D,False).SetIds(np.arange(tetra.GetNumberOfElements()))
    tris = mesh.GetElementsOfType(ED.Triangle_3)
    tris.tags.CreateTag(TZ.ExterSurf,False).SetIds(np.arange(tris.GetNumberOfElements()))

    tets = mesh.GetElementsOfType(ED.Tetrahedron_4)
    tets.tags.CreateTag('Tipmass3').AddToTag(0)
    tets.tags.CreateTag('Tipmass2').AddToTag(1)
    tets.tags.CreateTag('Tipmass1',False).SetIds(np.arange(2,tets.GetNumberOfElements()))
    tets.tags.CreateTag(TZ.Inside3D,False).SetIds(np.arange(tets.GetNumberOfElements()))
    tris = mesh.GetElementsOfType(ED.Triangle_3)
    tris.tags.CreateTag(TZ.ExterSurf,False).SetIds(np.arange(tris.GetNumberOfElements()))
    ls = LevelSet( support=mesh)
    ls.conform = True
    ls.phi = np.empty(ls.support.GetNumberOfNodes())
    ls.phi.fill(-1)

    analysisParams={"Numbermaxfreq":3}
    criteriaParams={"NumModeOptim":1}
    PPM = AsterModal(analysisParams)
    PPM.conform=True
    PPM.SetAuxiliaryFieldGeneration(FN.EigenFreqSquared_sensitivity,on=FN.Nodes)
    PPM.SetAuxiliaryScalarsbyOrderGeneration(FN.EigenFreqSquared,listSize=PPM.GetNumberOfSolutions())
    PPM.criteriaParams=criteriaParams
    material1={"young":210000.0,"poisson":0.3,"density":7.85e-09}
    material2={"young":220000.0,"poisson":0.3,"density":7.85e-09}
    material3={"young":230000.0,"poisson":0.3,"density":7.85e-09}
    PPM.materials = [['Tipmass1',material1],['Tipmass2',material2],['Tipmass3',material3]]
    PPM.dirichlet= [['X0',(0.,0.,0.)]]
    PPM.problems = ['idx1']
    print("Run modal analysis...")
    PPM.SolveByLevelSet(ls)
    PPM.GetAuxiliaryField(FN.EigenFreqSquared_sensitivity,on=FN.Nodes)
    print("First "+str(analysisParams["Numbermaxfreq"])+" eigenfrequencies squared in conform: ",PPM.GetAuxiliaryScalarsbyOrders(FN.EigenFreqSquared))
    return "ok"

if __name__ == '__main__':
    import time
    stime = time.time()
    print(CheckIntegrity())
    print("Total Time " + str(time.time()-stime))
