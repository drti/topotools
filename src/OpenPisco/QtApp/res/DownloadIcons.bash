#/bin/bash

wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_add_circle_black_24px.svg -O ic_add_circle_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_autorenew_black_24px.svg -O ic_autorenew_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_cached_black_24px.svg -O ic_cached_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_clear_black_24px.svg -O ic_clear_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_exit_to_app_black_24px.svg -O ic_exit_to_app_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_folder_open_black_24px.svg -O ic_folder_open_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_play_arrow_black_24px.svg -O ic_play_arrow_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_play_circle_filled_black_24px.svg -O ic_play_circle_filled_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_remove_circle_outline_black_24px.svg -O ic_remove_circle_outline_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_replay_black_24px.svg -O ic_replay_1_black_24px.svg
#these files are rendered in the wrong way in qt a modificated version are avilable in the sources
#wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_replay_30_black_24px.svg -O ic_replay_30_black_24px.svg
#wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_replay_10_black_24px.svg -O ic_replay_10_black_24px.svg
#wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_replay_5_black_24px.svg -O ic_replay_5_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_replay_black_24px.svg -O ic_replay_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_vertical_align_center_black_24px.svg -O ic_vertical_align_center_black_24px.svg
wget --no-check-certificate https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_directions_walk_black_24px.svg -O ic_directions_walk_black_24px.svg

wget --no-check-certificate https://material.io/tools/icons/static/icons/baseline-save_alt-24px.svg -O baseline-save_alt-24px.svg
wget --no-check-certificate https://material.io/tools/icons/static/icons/baseline-layers-24px.svg -O baseline-layers-24px.svg
wget --no-check-certificate https://material.io/tools/icons/static/icons/baseline-visibility-24px.svg -O baseline-visibility-24px.svg