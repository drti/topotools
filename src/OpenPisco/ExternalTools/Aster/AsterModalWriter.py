# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import OpenPisco.ExternalTools.Aster.AsterCommonWriter as AsterCommonWriter 

def WriteMaterialParametersInput(writeFile,materials):
    affe_mater=[]
    for num_mater,(tagname,material) in enumerate(materials):
        AsterCommonWriter.WriteVariable(writeFile,variableName='young_modulus_'+str(num_mater),variableValue=material["young"])
        AsterCommonWriter.WriteVariable(writeFile,variableName='poisson_number_'+str(num_mater),variableValue=material["poisson"])
        AsterCommonWriter.WriteVariable(writeFile,variableName='density_'+str(num_mater),variableValue=material["density"])
        
        value={}
        value["E"]="young_modulus_"+str(num_mater)
        value["NU"]="poisson_number_"+str(num_mater)
        value["RHO"]="density_"+str(num_mater)

        writeFile.write('mater_'+str(num_mater)+' = DEFI_MATERIAU(ELAS=_F('+','.join([ key+"="+value[key] for key in value.keys()])+'))\n')
        if tagname=='AllZones':
            affe_mater.append("_F(MATER=("+'mater_'+str(num_mater)+", ),TOUT=\'OUI\')")
        else:
            affe_mater.append("_F(MATER=("+'mater_'+str(num_mater)+", ),GROUP_MA=(\""+str(tagname)+"\",))")
    writeFile.write('materials = {\'AFFE\' :'+str(affe_mater).replace("\'_","_").replace(")\'",")").replace("\"_","_").replace(")\"",")").replace("\"\'","\"")+',}\n')
