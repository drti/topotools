# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

from OpenPisco.ExternalTools.Aster.AsterCommonWriter import WriteVariable

def WriteMecaModelisation(writeFile,dimensionality):
    asterModelisationKeyword={2:'D_PLAN',3:'3D'}[dimensionality]
    writeFile.write("modelisation_meca = {'MODELISATION':('"+asterModelisationKeyword+"', ),'PHENOMENE':'MECANIQUE','TOUT':'OUI'}\n")

def WriteThermalModelisation(writeFile,dimensionality):
    asterModelisationKeyword={2:'PLAN',3:'3D'}[dimensionality]
    writeFile.write("modelisation_thermal = {'MODELISATION':('"+asterModelisationKeyword+"', ),'PHENOMENE':'THERMIQUE','TOUT':'OUI'}\n")

def WriteMaterialParametersInput(writeFile,materials):
    affe_mater=[]

    for num_mater,(tagname,material) in enumerate(materials):
        WriteVariable(writeFile,variableName='lambda_'+str(num_mater),variableValue=material["lambda"])
        rho_cp = material["rho"]*material["cp"]
        WriteVariable(writeFile,variableName='rho_cp_'+str(num_mater),variableValue=rho_cp)
        WriteVariable(writeFile,variableName='young_modulus_'+str(num_mater),variableValue=material["young"])
        WriteVariable(writeFile,variableName='poisson_number_'+str(num_mater),variableValue=material["poisson"])
        WriteVariable(writeFile,variableName='alpha_'+str(num_mater),variableValue=material["thermalExpansion"])
        WriteVariable(writeFile,variableName='reference_temperature_'+str(num_mater),variableValue=material["referenceTemperature"])
        value={}
        value["LAMBDA"]="lambda_"+str(num_mater)
        value["RHO_CP"]="rho_cp_"+str(num_mater)
        value_elas = {}
        value_elas["E"]="young_modulus_"+str(num_mater)
        value_elas["NU"]="poisson_number_"+str(num_mater)
        value_elas["ALPHA"]="alpha_"+str(num_mater)

        writeFile.write('mater_'+str(num_mater)+' = DEFI_MATERIAU(THER=_F('+','.join([ key+"="+value[key]
        for key in value.keys()])+'), ELAS=_F('+','.join([ key+"="+value_elas[key]
        for key in value_elas.keys()])+'))\n')
        if tagname=='AllZones':
            affe_mater.append("_F(MATER=("+'mater_'+str(num_mater)+", ),TOUT=\'OUI\')")
        else:
            affe_mater.append("_F(MATER=("+'mater_'+str(num_mater)+", ),GROUP_MA=(\""+str(tagname)+"\",))")
    writeFile.write('materials = {\'AFFE\' :'+str(affe_mater).replace("\'_","_").replace(")\'",")").replace("\"_","_").replace(")\"",")").replace("\"\'","\"")+',}\n')
