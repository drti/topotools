# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
POURSUITE(PAR_LOT='NON')

mesh = LIRE_MAILLAGE(FORMAT='MED')

model = AFFE_MODELE(AFFE=_F(MODELISATION=('3D', ),
                            PHENOMENE='MECANIQUE',
                            TOUT='OUI'),
                    MAILLAGE=mesh)


CHYOUNG = LIRE_CHAMP (
                  UNITE=20,
                  MAILLAGE = mesh,
                  NOM_MED = 'CHYOUNG',
                  TYPE_CHAM = 'CART_NEUT_R',
                  #NOM_CMP_IDEM='OUI',
                  NOM_CMP = 'X1',
                  NOM_CMP_MED='DX', 
                  PROL_ZERO = 'OUI',
                  INST=0.0,
                  INFO=1,
)

CHNU = LIRE_CHAMP (
                  UNITE=20,
                  MAILLAGE = mesh,
                  NOM_MED = 'CHNU',
                  TYPE_CHAM = 'CART_NEUT_R',
                  #NOM_CMP_IDEM='OUI',
                  NOM_CMP = 'X1',
                  NOM_CMP_MED='DX',  
                  PROL_ZERO = 'OUI',
                  INST=0.0,
                  INFO=1,
)                

E_F=DEFI_FONCTION(NOM_PARA='NEUT1',VALE=(-1.0,-1.0,1.0,1.0, ),PROL_DROITE='LINEAIRE',PROL_GAUCHE='LINEAIRE');
NU_F=DEFI_FONCTION(NOM_PARA='NEUT2',VALE=(-1.0,-1.0,1.0,1.0, ),PROL_DROITE='LINEAIRE',PROL_GAUCHE='LINEAIRE');

mater=DEFI_MATERIAU(ELAS_FO=_F(E=E_F, NU=NU_F,),);

fieldmat = AFFE_MATERIAU(AFFE=_F(MATER=(mater, ),
                                 TOUT='OUI'),
                         AFFE_VARC=(
                                    _F(NOM_VARC='NEUT1', CHAM_GD=CHYOUNG,),
                                    _F(NOM_VARC='NEUT2', CHAM_GD=CHNU,),
                                   ),
                         MODELE=model);

bloc = AFFE_CHAR_MECA(MODELE=model, **dirichlet)

forcef = AFFE_CHAR_MECA(MODELE=model, **neumann)

forcen = AFFE_CHAR_MECA(MODELE=model, **forcenodale)

pesa = AFFE_CHAR_MECA(MODELE=model, **bodyforces)

solveur = _F(METHODE='MUMPS', 
             RESI_RELA = resi_rela,
            STOP_SINGULIER='NON',
                      )
                      
reslin = MECA_STATIQUE(CHAM_MATER=fieldmat,
                       EXCIT=(_F(CHARGE=bloc),
                              _F(CHARGE=forcef),
                              _F(CHARGE=forcen),
                              _F(CHARGE=pesa)
                              ),
                       SOLVEUR=solveur,
                       MODELE=model,
                       )

reslin = CALC_CHAMP(reuse=reslin,
                    DEFORMATION=('EPSI_ELGA',),
                    RESULTAT=reslin)


IMPR_RESU(FORMAT='MED',
          VERSION_MED='4.0.0',
          RESU=(
                _F(RESULTAT=reslin, 
                   NOM_CHAM=('DEPL'),
                  NOM_CHAM_MED=('DEPL'),
                   ),
                _F(RESULTAT=reslin, 
                   NOM_CHAM=('EPSI_ELGA'),
                  NOM_CHAM_MED=('strain'),
                   ),
               ),
          UNITE=80,
          INFO=1)     

FIN()
