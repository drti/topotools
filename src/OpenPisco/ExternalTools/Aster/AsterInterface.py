# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
import os
import os.path
import shutil
import numpy as np
import csv
import subprocess

from Muscat.Helpers.IO.TemporaryDirectory import TemporaryDirectory
from Muscat.Helpers.IO.Which import Which
from Muscat.Helpers.Logger import Info, ForcePrint, Error
import Muscat.Helpers.ParserHelper as PH
import Muscat.Containers.ElementsDescription as ED

from OpenPisco.ExternalTools.Aster import GetScriptsPath as GetScriptsPath
from OpenPisco.Unstructured.AppExecutableTools import AppExecutableBase, CommandLineBuilder
from OpenPisco.MuscatExtentions.MedReader import MedReader
import OpenPisco.PhysicalSolvers.FieldsNames as FN
from OpenPisco import RETURN_SUCCESS, RETURN_FAIL

#Name of the required scalar value in csv file by type of post-processing
keysbyValue={}
keysbyValue[FN.int_potential_energy]='TRAV_ELAS'
keysbyValue[FN.EigenFreqSquared]='EigenFreqSquared'
keysbyValue[FN.ERP]='ERPABS'
keysbyValue[FN.eigen_Frequencies]="eigen_Frequencies"
keysbyValue[FN.GeneDisp]="GeneDisp"
keysbyValue[FN.load_crit]='CHAR_CRIT'
keysbyValue[FN.ModalCumulMassFractiondX] = 'ModalCumulMassFractiondX'
keysbyValue[FN.ModalCumulMassFractiondY] = 'ModalCumulMassFractiondY'
keysbyValue[FN.ModalCumulMassFractiondZ] = 'ModalCumulMassFractiondZ'
keysbyValue[FN.ModalLastEnergyRatio] = 'ModalLastEnergyRatio'

AsterElemNameToRuleNames = {}
AsterElemNameToRuleNames[ED.Tetrahedron_10] = ['T10_____FPG5']
AsterElemNameToRuleNames[ED.Tetrahedron_4] = ['TE4_____FPG1','TE4_____FPG4']
AsterElemNameToRuleNames[ED.Triangle_3] = ['TR3_____FPG1']
AsterElemNameToRuleNames[ED.Triangle_6] = ['TR6_____FPG3']


AsterComputedFieldsSupport={
                  FN.potential_energy:FN.IPField,
                  FN.strain:FN.IPField,
                  FN.strain_xx:FN.IPField,
                  FN.strain_yy:FN.IPField,
                  FN.strain_zz:FN.IPField,
                  FN.strain_xy:FN.IPField,
                  FN.strain_xz:FN.IPField,
                  FN.strain_yz:FN.IPField,
                  FN.stress:FN.IPField,
                  FN.stress_xx:FN.IPField,
                  FN.stress_yy:FN.IPField,
                  FN.stress_zz:FN.IPField,
                  FN.stress_xy:FN.IPField,
                  FN.stress_xz:FN.IPField,
                  FN.stress_yz:FN.IPField,
                  FN.von_mises:FN.IPField,
                  FN.flux_temperature:FN.IPField,
                  FN.EigenFreqSquared_sensitivity:FN.Nodes,
                  FN.ERP_sensitivity:FN.Nodes,
                  FN.ERP_density:FN.Nodes,
                  FN.dominant_SolutionMode:FN.Nodes,
                  FN.grad_Ure:FN.Nodes,
                  FN.grad_Uim:FN.Nodes,
                  FN.AllModeDEPL:FN.Nodes,
                  FN.elastic_energy_buckling:FN.IPField,
                  FN.strain_buckling:FN.IPField,
                  FN.strain_green_buckling:FN.IPField,
                  FN.mode_buckling:FN.Nodes,
                  FN.temperature:FN.Nodes,
                  FN.zzError:FN.Centroids,
}

def GetScalarFromFile(fileName,scalarName):
    value = []
    keyNameExist = False
    counter = 0
    lines = []
    with open(fileName) as csvfile:
        reader=csv.reader(csvfile)
        for row in reader:
            counter+=1
            lines.append(row)
            if scalarName in row:
                index = row.index(scalarName)
                icounter=counter
                keyNameExist=True

    assert keyNameExist,"The name "+scalarName+" was not found in "+fileName

    for i in range(icounter,len(lines)):
        if len(lines[i]):
            val = PH.ReadFloat(lines[i][index])
            value.append(val)
        else:
            break
    return value

class AsterPrecisionError(Exception):
    pass

class AsterExecutableNotFound(Exception):
    pass

asterdefaultExec = "as_run"
asterExecOptions = []
if 'ASTER_EXECUTABLE' not in os.environ.keys():
    asterExec = asterdefaultExec
else:
    asterExecCustom = os.environ['ASTER_EXECUTABLE']
    if len(asterExecCustom.split())>1:
        asterExecCustom = asterExecCustom.split()
        asterExec = asterExecCustom[0]
        asterExecOptions = asterExecCustom[1:]
    else:
        asterExec = asterExecCustom

ASTER_AVAILABLE = Which(asterExec) 
class AsterInterface(AppExecutableBase):
    """
    .. py:class:: AsterInterface

    Interface for the finite element solver Code_Aster
    """

    def __init__(self):
        super(AsterInterface, self).__init__(asterExec)
        self.extrafiles=False
        self.support=None
        self.reader = MedReader()
        self.filepath = GetScriptsPath()
        # since stderr of code_aster is too much verbose and the verbosity level cannot be tuned, by default we don't keep it
        self.keepStdErr = False
        self.fileCsvname = ""
        self.fileoutname = ""
        self.memoryLimit = None

    def SetExtraFiles(self,extrafiles):
        self.extrafiles=extrafiles

    def _executeSubprocess(self, cmd):
        #TODO: Should be possible to use run instead but annoying bug to solve first
        return subprocess.check_output(' '.join(cmd), shell=True,stderr=subprocess.STDOUT)

    def ComputeMemoryLimit(self):
        cmdBuilder = CommandLineBuilder(self.GetAppName())
        for option in asterExecOptions:
            cmdBuilder.argumentAdd(option)
        cmdBuilder.argumentAdd("--info")
        cmd = cmdBuilder.result()
        try:
            out = self._runCommand(cmd)
            import io
            keyword = "interactif_memmax : "
            for line in io.StringIO(out):
                if line.startswith(keyword):
                   self.memoryLimit = line.rstrip()[len(keyword):]
        except Exception as e:
            Info("CodeAster warning : unable to estimate memory limit.")
            Info(e)

    def IsAsterStudyFileExtension(self,f):
        for ext in ['.comm','.export']:
            if f.endswith(ext): return True
        return False

    def GetStudyFilesFromPath(self,path):
        studyFiles = [f for f in os.listdir(path) if f.startswith(self.filename+'.') and self.IsAsterStudyFileExtension(f)]
        if self.extrafiles:
           studyFiles+=[f for f in os.listdir(path) if f.startswith(self.filename) and self.IsAsterStudyFileExtension(f)]
        return studyFiles

    def GetSolverComputedError(self):
        import re
        logfile=self._filePath(self.filename+".log")
        with open(logfile) as myfile:
            targets = list(set([line for line in myfile if "Erreur calcul" in line]))
            errorComputed=[float(x) for x in re.findall(r'-?\d+.?\d*(?:[Ee]-\d+)?', targets[0])][0]
        return errorComputed


    def ExecuteAster(self):
        source = os.getcwd()
        studyFiles = self.GetStudyFilesFromPath(source)
        if not len(studyFiles):
            Info("Study not found in current directory; looking at templates ")
            source = self.filepath
            studyFiles = self.GetStudyFilesFromPath(source)
        assert len(studyFiles)>0,"Aster files corresponding to "+self.filename+" were not found."
        for f in studyFiles:
            if f.endswith(".export"):
                fileReader = open(os.path.join(source, f),"r+")
                fileWriter =open(os.path.join(self.workingDirectory, f) ,"w")
                lines = fileReader.readlines()
                for line in lines:
                    if line.find("P memory_limit ")>-1 and self.memoryLimit is not None:
                        fileWriter.write(line.replace(line,"P memory_limit "+str(self.memoryLimit)+"\n"))
                    else:
                        fileWriter.write(line)
                fileReader.close()
                fileWriter.close()
            else:
                shutil.copy(os.path.join(source,f), self.workingDirectory)

        cmdBuilder = CommandLineBuilder(self.GetAppName())
        for option in asterExecOptions:
            cmdBuilder.argumentAdd(option)
        cmdBuilder.argumentAdd(self._filePath(self.filename+".export"))
        #hack to redirect aster stderr
        if not self.keepStdErr:
           cmdBuilder.argumentAdd("2> /dev/null")
        cmd = cmdBuilder.result()

        #Run executable
        try:
            self._runCommand(cmd)
        except:
            workDir=self.workingDirectory
            asterLogFiles=[os.path.join(workDir, f) for f in os.listdir(workDir) if f.endswith(self.filename+'.log')]
            if len(asterLogFiles)==1:
                logfile=asterLogFiles[0]
            elif not asterLogFiles:
                if not ASTER_AVAILABLE:
                    raise AsterExecutableNotFound("Aster executable not found")
                raise Exception("Problem arising before Aster log was produced")
            else:
                logfile=asterLogFiles[0]
                Info("Warning: several log files found, only look at" + logfile +" file")

            with open(logfile) as myfile:
                if 'La solution du système linéaire est trop imprécise' in myfile.read():
                    raise AsterPrecisionError("Precision problem when solving FEM")
                else:
                    myfile.seek(0)
                    last_lines = myfile.readlines()[-200:-1]
                    for line in last_lines:
                        print(line)
                    Error("Aster executable ("+" ".join(cmd)+") had failed for the reason stated above")
                    return RETURN_FAIL
        return RETURN_SUCCESS

    def CleanWorkingDirectory(self,extensions):
        import os
        filesToDelete = [self.filename+extension for extension in extensions]
        for f in filesToDelete:
            try:
                filetodelete = self.workingDirectory+f
                if os.path.isfile(filetodelete):
                    os.remove(filetodelete)
            except FileNotFoundError:
                pass

    def GetField(self,fieldName,fieldSupport,index=None):
        self.fileoutname = self._filePath(self.filename+"output.rmed")
        self.support = self.reader.ReadExtraFields(self.support,self.fileoutname,fieldNames=[fieldName],fieldSupports=[fieldSupport],fieldNumOrders=[index])

        if fieldSupport=="nodes":
           return self.support.nodeFields[fieldName]
        else:
           return self.support.elemFields[fieldName]

    def GetIntegrationRuleFor(self,name):
        locname, _, gscoo , wg = self.reader.ReadGaussPointsLocalization(self.fileoutname)
        assert locname in AsterElemNameToRuleNames[name],"Cannot find integration rule for this element: "+str(name)
        return gscoo, wg

    def GetScalar(self,name):
        return self.GetScalarsbyOrders(name)[0]

    def GetScalarsbyOrders(self,name,listSize=1):
        self.fileCsvname = self._filePath(self.filename+"_objective.csv")
        keyName = keysbyValue[name]
        value = GetScalarFromFile(fileName=self.fileCsvname,scalarName=keyName)
        assert len(value)==listSize,("Given list size ("+str(listSize)+") is different from the size of scalar list computed by Code_Aster : ",len(value))
        return value

    def GetScalarbyLoadCase(self,name,loadcase):
        self.fileCsvname = self._filePath(self.filename+"_objective.csv")
        keyName = keysbyValue[name]+"_"+str(loadcase)
        value = GetScalarFromFile(fileName=self.fileCsvname,scalarName=keyName)
        return value[0]

def SkipAsterTest():
    from Muscat.Helpers.CheckTools import SkipTest
    if SkipTest("ASTER_NO_FAIL"):
        return True,"skip"

    from OpenPisco.MuscatExtentions.MedTools import MEDAvailable
    if not MEDAvailable:
        return True,"skip MED not Available"

    from OpenPisco.ExternalTools.Aster.AsterInterface import ASTER_AVAILABLE
    if not ASTER_AVAILABLE:
        return True,"skip Aster not Available"

    return False,"Aster test can be run"



def PrepareLinToQuadStudy():
    import OpenPisco.TopoZones as TZ
    from Muscat.Containers.MeshCreationTools import CreateCube

    mesh = CreateCube(dimensions=[3,3,3],spacing=[1./2, 1./2, 1./2],origin=[0, 0, 0],ofTetras=True)
    nbtets = mesh.GetElementsOfType(ED.Tetrahedron_4).GetNumberOfElements()
    mesh.GetElementsOfType(ED.Tetrahedron_4).GetTag(TZ.Bulk).SetIds(np.arange(nbtets) )

    solverInterface = AsterInterface()
    solverInterface.SetFilename("AsterLinToQuad")
    path=TemporaryDirectory.GetTempPath()
    solverInterface.SetWorkingDirectory(path)
    solverInterface.ComputeMemoryLimit()
    from OpenPisco.MuscatExtentions.MedWriter import WriteMed
    WriteMed(path+solverInterface.filename+".med",mesh)
    return solverInterface, path

def CheckIntegrity_LinToQuad(GUI=False):
    """
    Convert a linear mesh into a quadratic one.
    The extra nodes added for the quadratic elements are stored after the original ones in the mesh nodes storage.
    The following line allows to retrive the original nodes in the quadratic mesh
    quadMesh.nodes[0:inputmesh.GetNumberOfNodes(),:]
    The order of elements is left unchanged.
    For more information see https://www.code-aster.org/V2/doc/default/fr/man_u/u4/u4.23.02.pdf
    """
    skipTestPhase,message = SkipAsterTest()
    if skipTestPhase:
        return message

    solverInterface, path = PrepareLinToQuadStudy()
    solverInterface.ExecuteAster()
    from OpenPisco.MuscatExtentions.MedReader import ReadMed
    newMesh = ReadMed(path+solverInterface.filename+".rmed")
    nbTet10 = newMesh.GetElementsOfType(ED.Tetrahedron_10)
    assert nbTet10.GetNumberOfElements()>0, "There should be only Tet10 elements"
    nbTet4 = newMesh.GetElementsOfType(ED.Tetrahedron_4)
    assert nbTet4.GetNumberOfElements()==0, "There should not be any Tet4 element"
    return "ok"

def CheckIntegrity_AsterExecutableNotFound():
    os.environ["ASTER_EXECUTABLE"] = "as_run_dummy"

    from OpenPisco.MuscatExtentions.MedTools import MEDAvailable
    if not MEDAvailable:
        return "skip MED not Available"

    solverInterface, _ = PrepareLinToQuadStudy()
    try:
        solverInterface.ExecuteAster()
    except AsterExecutableNotFound:
        print("Aster executable not found")
        pass
    return "ok"

def CheckIntegrity():
    totest = [
        CheckIntegrity_AsterExecutableNotFound,
        CheckIntegrity_LinToQuad,
              ]
    for test in totest:
        res =  test()
        if  res.lower() != "ok" :
            return res
    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity())
