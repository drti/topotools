# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

def WriteModelisation(writeFile,dimensionality):
    asterModelisationKeyword={2:'PLAN',3:'3D'}[dimensionality]
    writeFile.write("modelisation = {'MODELISATION':('"+asterModelisationKeyword+"', ),'PHENOMENE':'THERMIQUE','TOUT':'OUI'}\n")

def WriteConvectionParametersInput(writeFile,convection):
    writeFile.write("convection = {'ECHANGE' : [\n")
    for tagname,params in convection:
        writeFile.write("          _F(COEF_H="+str(params["h"])+", TEMP_EXT="+str(params["temp_ext"])+", GROUP_MA=('"+str(tagname)+"', )),\n")
    writeFile.write("],\n")
    writeFile.write("}\n")

def WriteSourceParametersInput(writeFile,source):
    writeFile.write("source = {'SOURCE' : [\n")
    for tagname,params in source:
        if tagname=='AllZones':
            writeFile.write("          _F(SOUR="+str(params["source"])+", TOUT=\'OUI\')")
        else:
            writeFile.write("          _F(SOUR="+str(params["source"])+", GROUP_MA=('"+str(tagname)+"', )),\n")
    writeFile.write("],\n")
    writeFile.write("}\n")

def WriteThermalDirichletParametersInput(writeFile,dirichlet):
    writeFile.write("ther_impo = {'THER_IMPO' : [\n")
    for tagname,params in dirichlet:
        writeFile.write("          _F(TEMP="+str(params["temperature"])+", GROUP_MA=('"+str(tagname)+"', )),\n")
    writeFile.write("],\n")
    writeFile.write("}\n")

def WriteTimeParametersInput(writeFile,timeParameters):
    assert {"start","end","nsteps","starttemperature"} <= timeParameters.keys(), "Some parameter needed for the temporal discretization is missing"
    writeFile.write("t_start ="+str(timeParameters["start"])+"\n")
    writeFile.write("t_end ="+str(timeParameters["end"])+"\n")
    writeFile.write("n_steps ="+str(timeParameters["nsteps"])+"\n")
    writeFile.write("temp_0 ="+str(timeParameters["starttemperature"])+"\n")

def WriteTimeParametersInputForEigenValue(writeFile,timeParameters):
    assert {"start","end","starttemperature"} <= timeParameters.keys(), "Some parameter needed for the temporal discretization is missing"
    writeFile.write("t_start ="+str(timeParameters["start"])+"\n")
    writeFile.write("t_end ="+str(timeParameters["end"])+"\n")
    writeFile.write("temp_0 ="+str(timeParameters["starttemperature"])+"\n")


def WriteMaterialParametersInput(writeFile,materials):
    affe_mater=[]
    for num_mater,(tagname,material) in enumerate(materials):
        writeFile.write('lambda_'+str(num_mater)+'='+str(material["lambda"])+'\n')
        rho_cp = material["rho"]*material["cp"]
        writeFile.write('rho_cp_'+str(num_mater)+'='+str(rho_cp)+'\n')
        value={}
        value["LAMBDA"]="lambda_"+str(num_mater)
        value["RHO_CP"]="rho_cp_"+str(num_mater)
        writeFile.write('mater_'+str(num_mater)+' = DEFI_MATERIAU(THER=_F('+','.join([ key+"="+value[key] for key in value.keys()])+'))\n')
        if tagname=='AllZones':
            affe_mater.append("_F(MATER=("+'mater_'+str(num_mater)+", ),TOUT=\'OUI\')")
        else:
            affe_mater.append("_F(MATER=("+'mater_'+str(num_mater)+", ),GROUP_MA=(\""+str(tagname)+"\",))")
    writeFile.write('materials = {\'AFFE\' :'+str(affe_mater).replace("\'_","_").replace(")\'",")").replace("\"_","_").replace(")\"",")").replace("\"\'","\"")+',}\n')
