# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

asterKeywordsByConcept= {
    "dirichlet":{"BY_MULTIPLIER":"dirichlet = {'DDL_IMPO' : [\n",
                 "BY_ELIMINATION":"dirichlet = {'MECA_IMPO' : [\n"
                 },
    "neumann": {"force": {"2D":'FORCE_CONTOUR',"3D":'FORCE_FACE'},
                "pressure" : 'PRES_REP'
               },
    "nodalForce":'FORCE_NODALE',
    "bodyForce":'FORCE_INTERNE'
}

def OpenAsterParamFile(fileName):
    writeFile = open(fileName,"a")
    writeFile.write("DEBUT(PAR_LOT='NON'); \n")
    return writeFile

def CloseAsterParamFile(writeFile):
    writeFile.write("FIN()")
    writeFile.close()

def WriteVariable(writeFile,variableName,variableValue):
    writeFile.write(variableName+'='+str(variableValue)+'\n')

def WriteBodyForcesParametersInput(writeFile,bodyforce,dimensionality,problems):
    bodyForceKeyword=asterKeywordsByConcept["bodyForce"]
    WriteVariable(writeFile,"bodyforces","{'"+bodyForceKeyword+"' : [")

    forceDirections=["FX","FY","FZ"]
    indentSpace="                                  "
    for problemId in problems:
        if problemId in bodyforce.keys():
            for idl,loadcase in enumerate(bodyforce[problemId]):
                assert len(loadcase[1])==dimensionality,"body force provided ambiguous: number of components should match mesh dimension"
                if bodyforce[problemId][idl][0]=='AllZones':
                    zoneStr="TOUT='OUI'"
                else:
                    zoneStr="GROUP_MA=('"+"FE"+str(bodyforce[problemId][idl][0])+"', )"
                loadStr=[forceDirections[dimensionAxeId]+"="+str(loadcase[1][dimensionAxeId]) for dimensionAxeId in range(dimensionality)]
                bodyForceStr="_F("+", ".join(loadStr)+", "+zoneStr+"),\n"
                writeFile.write(indentSpace+bodyForceStr)
    writeFile.write("],\n")
    writeFile.write("}\n")

def WriteDirichletParametersInput(writeFile,dirichlet,dimensionality,method="BY_MULTIPLIER",group="OnElements"):
    try:
        writeFile.write(asterKeywordsByConcept["dirichlet"][method])
    except KeyError:
        raise Exception("Aster does know how to enforce the dirichlet condition with the method "+method)
    asterLocationByGroup={"OnElements":"GROUP_MA","OnNodes":"GROUP_NO"}
    dofDirections=["DX","DY","DZ"]

    for Id in dirichlet:
        try:
            asterLocation=asterLocationByGroup[group]
        except KeyError as e:
            raise Exception("This type of group "+str(e)+" does not exist (TODO: check if GROUP_NO really required here)")
        errorMessage = "Dirichlet boundary condition provided ambiguous: number of components should match mesh dimension"
        assert len(Id[1])==dimensionality,errorMessage

        diristr=""
        for dimensionAxeId in range(dimensionality):
            if Id[1][dimensionAxeId] is not None:
                diristr+=dofDirections[dimensionAxeId]+"="+str(Id[1][dimensionAxeId])+","

        writeFile.write("                   _F("+diristr+" "+asterLocation+"=('"+str(Id[0])+"', )),")

    writeFile.write("],\n")
    writeFile.write("}\n")

def WriteNeumannParametersInput(writeFile,neumann,dimensionality,problems):
    neumannForceKeyword=asterKeywordsByConcept["neumann"]["force"][str(dimensionality)+"D"]
    WriteVariable(writeFile,"neumann","{'"+neumannForceKeyword+"' : [")
    forceDirections=["FX","FY","FZ"]
    indentSpace="                                  "
    for problemId in problems:
        if problemId in neumann.keys():
            for idl,loadcase in enumerate(neumann[problemId]):
                assert len(loadcase[1])==dimensionality,"Neumann boundary condition provided ambiguous: number of components should match mesh dimension"
                loadStr=[forceDirections[dimensionAxeId]+"="+str(loadcase[1][dimensionAxeId]) for dimensionAxeId in range(dimensionality)]
                neumannStr="_F("+", ".join(loadStr)+", GROUP_MA=('"+str(neumann[problemId][idl][0])+"', )),\n"
                writeFile.write(indentSpace+neumannStr)
    writeFile.write("],\n")
    writeFile.write("}\n")

def WritePressureParametersInput(writeFile,neumann,problems):
    neumannPressureKeyword=asterKeywordsByConcept["neumann"]["pressure"]
    WriteVariable(writeFile,"neumann","{'"+neumannPressureKeyword+"' : [")
    for problemId in problems:
        if problemId in neumann.keys():
            for idl,loadcase in enumerate(neumann[problemId]):
                writeFile.write("                                  _F(PRES="+str(loadcase[1])+", GROUP_MA=('"+str(neumann[problemId][idl][0])+"', )),\n")
    writeFile.write("],\n")
    writeFile.write("}\n")

def WriteNodalForcesParametersInput(writeFile,forcenodale,dimensionality,problems):
    nodalForceKeyword=asterKeywordsByConcept["nodalForce"]
    writeFile.write("forcenodale = {'"+nodalForceKeyword+"' :  [\n")
    forceDirections=["FX","FY","FZ"]
    indentSpace="                                  "
    for problemId in problems:
        if problemId in forcenodale.keys():
            for idl,loadcase in enumerate(forcenodale[problemId]):
                assert len(loadcase[1])==dimensionality,"nodal force provided ambiguous: number of components should match mesh dimension"
                loadStr=[forceDirections[dimensionAxeId]+"="+str(loadcase[1][dimensionAxeId]) for dimensionAxeId in range(dimensionality)]
                nodalForceStr="_F("+", ".join(loadStr)+", GROUP_NO=('"+str(forcenodale[problemId][idl][0])+"', )),\n"
                writeFile.write(indentSpace+nodalForceStr)
    writeFile.write("],\n")
    writeFile.write("}\n")