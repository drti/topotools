# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

def WriteModelisation(writeFile,dimensionality,planeStress=True):
    planeStressDict={True:'C_PLAN',False:'D_PLAN'}
    asterModelisation = {"modelisationType": {"2D":planeStressDict[planeStress],"3D":"3D"}}
    asterModelisationKeyword = asterModelisation["modelisationType"][str(dimensionality)+"D"]
    writeFile.write("modelisation = {'MODELISATION':('"+asterModelisationKeyword+"', ),'PHENOMENE':'MECANIQUE','TOUT':'OUI'}\n")
