# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
from dataclasses import dataclass,asdict
from typing import Optional
import subprocess

from Muscat.Helpers.IO.Which import Which

from OpenPisco.Unstructured.AppExecutableTools import AppExecutableBase, CommandLineBuilder
from OpenPisco import RETURN_SUCCESS

openfoamExec = "foamRun"
openfoamExecPostProcess = "foamPostProcess"

@dataclass
class OpenFoamGeneralConfig:
    application :str = openfoamExec
    startFrom :str = "startTime"
    startTime :float = 0
    stopAt :str = "endTime"
    endTime : float = 1.0
    deltaT : float = 0.1
    writeControl :str = "timeStep"
    writeInterval : float = 1
    purgeWrite : float = 0
    writeFormat :str = "binary"
    writePrecision :int = 6
    writeCompression :str = "off"
    timeFormat :str = "general"
    timePrecision :int = 6
    runTimeModifiable :str = "true"  
    relativeTolerance :float = 0.0   

    def UpdateFromDict(self, params:dict):
        """Update instance from dict

        :param params: new parameters
        :type params: dict
        :return: instance
        :rtype: OpenFoamGeneralConfig
        """
        originalParams = self.ToDict()
        originalParams.update(params)
        return type(self)(**originalParams)

    def UpdateFromInstance(self,other:Optional["OpenFoamGeneralConfig"]=None):
        """Update instance from another instance

        :param other: another instance
        :type other: OpenFoamGeneralConfig,optional
        :return: instance
        :rtype: OpenFoamGeneralConfig
        """
        if other is None:
            newInstance = self
        else:
            params = other.ToDict()
            newInstance = self.UpdateFromDict(params)
        return newInstance

    def ToDict(self) -> dict:
        """Convert to dict

        :return: instance as a dict
        :rtype: dict
        """
        return asdict(self)

    def __str__(self)->str:
        """Overload str to get a class representation

        :return: string representation of the class
        :rtype: str
        """
        sInfo = ""
        constructorParams = self.ToDict()
        for paramName,paramVal in constructorParams.items():
            sInfo+="\t"+str(paramName)+": "+str(paramVal)+"\n"
        return sInfo


class OpenFoamInterface(AppExecutableBase):
    """
    .. py:class:: OpenFoamInterface

    Interface for the finite volume solver OpenFoam
    """

    def __init__(self):
        super(OpenFoamInterface, self).__init__(openfoamExec)

    def _executeSubprocess(self, cmd):
        return subprocess.check_output(cmd, shell=True,stderr=subprocess.STDOUT)
    
    def ExecuteOpenFoam(self):
        cmdBuilder = CommandLineBuilder(self.GetAppName())
        cmdBuilder.optionAdd("case")
        cmdBuilder.argumentAdd(self.GetWorkingDirectory())
        cmd = " ".join(cmdBuilder.result())
        self._runCommand(cmd)
        return RETURN_SUCCESS

    def ExecuteOpenFoamPostProcess(self,functionToCompute):
        cmdBuilder = CommandLineBuilder(openfoamExecPostProcess)
        cmdBuilder.optionAdd("func")
        cmdBuilder.argumentAdd("\""+functionToCompute+"\"")
        cmdBuilder.optionAdd("case")
        cmdBuilder.argumentAdd(self.GetWorkingDirectory())
        cmd = " ".join(cmdBuilder.result())
        self._runCommand(cmd)
        return RETURN_SUCCESS

def SkipFoamTest():
    from Muscat.Helpers.CheckTools import SkipTest
    if SkipTest("FOAM_NO_FAIL"):
        return True,"skip"

    if not Which(openfoamExec):
        return True,"skip OpenFoam not Available"

    return False,"Foam test can be run"

def CheckIntegrity():
    foamConfig = OpenFoamGeneralConfig()
    updatedConfiguration = {"deltaT":0.0001,"relativeTolerance":0.1}
    newConfig = foamConfig.UpdateFromDict(updatedConfiguration)
    assert newConfig.deltaT==updatedConfiguration["deltaT"]
    assert newConfig.relativeTolerance==updatedConfiguration["relativeTolerance"]

    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity())