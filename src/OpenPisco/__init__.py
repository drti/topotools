# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

_test = [
"Actions",
"CLApp",
"Optim",
"PhysicalSolvers",
"QtApp",
"Structured",
"TestData",
"Unstructured",
"ExtractConnexPart",
"ImplicitGeometry",
"LevelSetBase",
"ConceptionFieldFactory",
"TopoZones",
"ExternalTools",
"MuscatExtentions"
]

RETURN_SUCCESS = 0
RETURN_FAIL = 1
RETURN_FAIL_EXTERNAL_TOOL = 2
