# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import numpy as np
from Muscat.Types import MuscatIndex, MuscatFloat
import Muscat.Containers.ElementsDescription as ED
from Muscat.Containers.Mesh import Mesh
import Muscat.Containers.MeshCreationTools as MeshCreationTools
from Muscat.Helpers.IO.Which import Which
from Muscat.Helpers.Logger import Debug

from OpenPisco.Unstructured.AppExecutableTools \
        import AppExecutableBase, CommandLineBuilder
import OpenPisco.TopoZones as TZ

velextExec = "velext"
class VelocityExtension(AppExecutableBase):
    def __init__(self):
        super(VelocityExtension, self).__init__(velextExec)
        self.SetBinaryFlag(False);
        self.support = None
        self.basename = "velextdata"
        self.hasphi = False

    def SetSupport(self, support):
        self.hasphi = False
        self.support = support

    def WriteSupport(self):
        self.support.PrepareForOutput()

        # Shallow copy nodes
        supporttmp = Mesh()
        supporttmp.nodes = self.support.nodes
        supporttmp.originalIDNodes = np.arange(supporttmp.GetNumberOfNodes())

        # Shallow copy tets
        tets = self.support.GetElementsOfType(ED.Tetrahedron_4)
        supporttmp.elements.AddContainer(tets)

        # Copy of triangles in the surface
        tris = self.support.GetElementsOfType(ED.Triangle_3)
        ids = tris.GetTag(TZ.InterSurf).GetIds()

        ntris = supporttmp.GetElementsOfType(ED.Triangle_3)
        ntris.Allocate(len(ids))
        ntris.connectivity[:,:] = tris.connectivity[ids,:]

        supporttmp.PrepareForOutput()
        globaloffset = supporttmp.ComputeGlobalOffset()

        nodalRefNumber = np.zeros(supporttmp.GetNumberOfNodes(), dtype=MuscatIndex)
        nodalRefNumber[ntris.connectivity.flatten()] = 10

        elemRefNumber = np.ones(self.support.GetNumberOfElements(), dtype=MuscatIndex)
        elemRefNumber[globaloffset[ED.Tetrahedron_4] + tets.GetTag(TZ.Inside3D).GetIds()] = TZ.MG_MINUS
        elemRefNumber[globaloffset[ED.Tetrahedron_4] + tets.GetTag(TZ.Outside3D).GetIds()] = TZ.MG_PLUS

        elemRefNumber[globaloffset[ED.Triangle_3]:globaloffset[ED.Triangle_3]+ntris.GetNumberOfElements()] = 10

        filename = self._filePathFromSuffix("mesh")
        self.writer.Open(filename)
        self.writer.Write(supporttmp, nodalRefNumber=nodalRefNumber, elemRefNumber=elemRefNumber)
        self.writer.Close()

    def WriteLevelSet(self,levelset):
        self.hasphi = True
        filename = self._filePathFromSuffix("chi.mesh")
        self.writer.SetFileName(filename)
        self.writer.OpenSolutionFile(self.support)
        self.writer.WriteSolutionsFields(self.support, [levelset])
        self.writer.CloseSolutionFile()

    def WriteFieldToExtend(self,field):
        filename = self._filePathFromSuffix("V.mesh")
        self.writer.SetFileName(filename)

        self.writer.OpenSolutionFile(self.support)

        if len(field.shape) == 1 or (len(field.shape) == 2  and field.shape[-1] == 1) :
            field = np.hstack((field, field, field)).reshape((3, field.shape[-1])).T

        self.writer.WriteSolutionsFields(self.support, [field])
        self.writer.Close()


#Dirichlet
#2 (number of conditions)
#1 vertex v 0. 0.    (reference vertex/edge/triangle f/v ...)
#2 vertex f          (reference vertex/edge/triangle f/v )
#
#Neumann
#2 (number of conditions)
#1 vertex v 0. 0.    (reference vertex/edge/triangle f/n/v ...)
#1 vertex n 0.    (reference vertex/edge/triangle f/n/v ...)
#1 vertex f     (reference vertex/edge/triangle f/n/v ...)
#
#Gravity
#0. 0. 0.
#
#Domain
#1 (number of conditions)
#2 1.0   (reference alpha)

    def WriteConfigfile(self):
        filename = self._filePathFromSuffix("velext", binary=False)
        ops = open(filename, "w")

#2 vertex v 1.0 2.0 3.0
#neumann
##1
#10 vertex f
#
        ops.write("Dirichlet\n")
        ops.write("1\n")
        ops.write(str(TZ.MMG_ISO) + " vertex f\n")

        #ops.write(str(TZ.MMG_ISO) + " edge f\n")
        ##Velext does not work with triangles
        ##work in progress
        #ops.write(str(TZ.MMG_ISO) + " triangle f\n")

        ops.write("\n")
        ops.write("Domain\n")
        ops.write("2\n")
        ops.write(str(TZ.MG_MINUS) + " 1.0\n")
        ops.write(str(TZ.MG_PLUS) + " 1.0\n")
        ops.write("\n")
        ops.close()

    def ComputeExtension(self, verbose=0, nit=None):
        self.WriteConfigfile()

        # Command line structure:
        # velext
        # [+/-v | -h] [-a val] [-n nit] [-r res]
        # source_file[.mesh]
        # [-c chi_file.[sol]]
        # [-p param_file[.elas]]
        # [-s data_file[.sol]]
        # [-o output_file[.sol]]

        cmdBuilder = CommandLineBuilder(self.GetAppName())

        if verbose != 0:
            verbose_prefix = "-" if verbose < 0 else "+"
            cmdBuilder.optionAdd("v", prefix=verbose_prefix)

        if nit is not None:
            cmdBuilder.optionAdd("nit", arg=str(nit))

        cmdBuilder.argumentAdd(self._fileNameFromSuffix("mesh"))
        cmdBuilder.optionAdd("s", arg=self._fileNameFromSuffix("V.sol"))
        cmdBuilder.optionAdd("o", arg=self._fileNameFromSuffix("V.new.sol"))

        if self.hasphi :
            cmdBuilder.optionAdd("c", arg=self._fileNameFromSuffix("chi.sol"))

        cmd = cmdBuilder.result()
        self._executeCommand(cmd)

    def GetExtendedField(self):
        filename = self._filePathFromSuffix("V.new.sol")
        self.reader.SetFileName(filename)
        res = self.reader.Read()
        return res.nodeFields["SolAtVertices0"][:,0]

    def _filePathFromSuffix(self, suffix, binary=None):
        base_filename = self._baseFileNameFromSuffix(suffix)
        return self._filePath(base_filename, binary)

    def _fileNameFromSuffix(self, suffix, binary=None):
        base_filename = self._baseFileNameFromSuffix(suffix)
        return self._fileName(base_filename, binary)

    def _baseFileNameFromSuffix(self, suffix):
        return ".".join((self.basename, suffix))


def ExtendField(support, field):
    obj = VelocityExtension()
    obj.SetSupport(support)
    obj.WriteSupport()
    obj.WriteFieldToExtend(field)
    obj.ComputeExtension()
    newSpeed = obj.GetExtendedField()
    return newSpeed


def CheckIntegrity(GUI=False):
    if not Which("velext"):
        return "Not Ok, velext not found!!"

    from Muscat.Helpers.IO.TemporaryDirectory import TemporaryDirectory
    tempdir = TemporaryDirectory.GetTempPath()

    points = [[0,0,0],[1,0,0],[0,1,0],[0.5,0.5,1],[0,0, -1] ]
    tets = [[0,1,2,3],[0,2,1,4],]
    mesh = MeshCreationTools.CreateMeshOf(points,tets,ED.Tetrahedron_4)
    mesh.GetElementsOfType(ED.Tetrahedron_4).GetTag(TZ.Inside3D).SetIds([0])
    mesh.GetElementsOfType(ED.Tetrahedron_4).GetTag(TZ.Outside3D).SetIds([1])

    tris = mesh.GetElementsOfType(ED.Triangle_3)
    tris.Allocate(1)
    tris.connectivity[0,:] =  [0,1,2]
    tris.cpt =1
    tris.GetTag(TZ.InterSurf).AddToTag(0)

    tag = mesh.nodesTags.CreateTag(TZ.ALWAYS)
    tag.AddToTag(3)

    energy  = np.array([[-1.,-2,3],
                        [3,4,5],
                        [6,7,8],
                        [0,0,0],
                        [0,0,0]],dtype= MuscatFloat )

    obj = VelocityExtension()
    obj.SetWorkingDirectory(tempdir)
    obj.SetSupport(mesh)
    obj.WriteSupport()
    obj.WriteFieldToExtend(energy)
    obj.ComputeExtension()
    new_energy = obj.GetExtendedField()
    Debug(energy)
    Debug(new_energy)

    if GUI:
        from Muscat.Actions.OpenInParaView import OpenInParaView
        mesh.nodeFields["old"] = energy
        mesh.nodeFields["new"] = new_energy
        OpenInParaView(mesh)
        print(mesh)

    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity(True))# pragma: no cover
