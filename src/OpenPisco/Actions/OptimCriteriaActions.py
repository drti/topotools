# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

from Muscat.IO.IOFactory import CreateWriter,InitAllWriters

from OpenPisco.Actions.ActionBase import ActionBase
from OpenPisco.Actions.ActionFactory import RegisterClass
from OpenPisco import RETURN_SUCCESS

InitAllWriters()

class UpdateCriteriaValuesAction(ActionBase):
    """
    .. py:class:: UpdateCriteriaValuesAction

    Update optimization criteria values (function and sensitivity)
    """
    def __init__(self):
        super(UpdateCriteriaValuesAction,self).__init__()
    def Apply(self, app, ops):
        """Update instance from another instance

        :param ops: dictionary of options 
        :type ops: dict
        :return: RETURN_SUCCESS
        :rtype: bool 
        """
        ls = ops['ls']
        optimProblem = ops['optimProblem']
        res = optimProblem.UpdateValues()

        if 'saveToFile' in ops:
            filename = ops['saveToFile']
            writer = CreateWriter(filename )
            writer.SetFileName(filename )
            writer.SetTemporal()
            writer.SetBinary(False)
            writer.SetHdf5(True)
            writer.automaticOpen = True
            pointFields =  [ls.phi,optimProblem.objectiveCriterion.GetSensitivity()]
            pointFieldsNames=['phi',"O_"+optimProblem.objectiveCriterion.GetName()]

            for criterion in optimProblem.GetConstraintsCriteria():
                pointFields.append(criterion.GetSensitivity())
                pointFieldsNames.append("C_"+criterion.GetName())

            writer.Write(ls.support,PointFields=pointFields,PointFieldsNames=pointFieldsNames)
            writer.Close()

        return RETURN_SUCCESS

RegisterClass("UpdateCriteriaValues",UpdateCriteriaValuesAction)


def CheckIntegrity():
    UpdateCriteriaValuesAction()
    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity())
