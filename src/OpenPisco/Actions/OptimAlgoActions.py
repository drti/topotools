# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import Muscat.Helpers.ParserHelper as PH

from OpenPisco.Actions.ActionFactory import RegisterClass
from OpenPisco.Actions.ActionBase import ActionBase
from OpenPisco import RETURN_SUCCESS, RETURN_FAIL, RETURN_FAIL_EXTERNAL_TOOL

class RunAlgorithm(ActionBase):
    def __init__(self):
       super(RunAlgorithm,self).__init__()

    def Apply(self,app, ops):
       return app.optimAlgos[PH.ReadInt(ops['TopoOp'])].Start()


RegisterClass("RunTopoOp",RunAlgorithm)


class ResetAlgorithm(ActionBase):
    def __init__(self):
       super(ResetAlgorithm,self).__init__()

    def Apply(self,app, ops):

       app.optimAlgos[int(ops['TopoOp'])].ResetState()

       return RETURN_SUCCESS


RegisterClass("ResetTopoOp",ResetAlgorithm)

def CheckIntegrity():
    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity())
