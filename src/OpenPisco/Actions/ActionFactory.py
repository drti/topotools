# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
from Muscat.Helpers.Factory import Factory

def RegisterClass(name, classtype, constructor=None, withError = True):
    return ActionFactory.RegisterClass(name,classtype, constructor=constructor, withError = withError )


def Create(name,ops=None):
    return ActionFactory.Create(name,ops)

class ActionFactory(Factory):
    _Catalog = {}
    _SetCatalog = set()

    def __init__(self):
        super(ActionFactory,self).__init__()

def InitAllActions():
    import OpenPisco.Actions.GeneralActions
    import OpenPisco.Actions.UnstructuredActions
    import OpenPisco.Actions.FieldsActions
    import OpenPisco.Actions.OptimAlgoActions
    import OpenPisco.Actions.OptimCriteriaActions
    import OpenPisco.Actions.MeshActions
    import OpenPisco.Actions.AdditiveManufacturingActions
    import OpenPisco.Actions.PhysicalSolversActions


def CheckIntegrity():
    InitAllActions()
    return "ok"

if __name__ == '__main__':
        print(CheckIntegrity())
