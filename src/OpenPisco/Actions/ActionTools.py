# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
from typing import Dict

from Muscat.Containers.Mesh import Mesh

def GetSupportFromLsOrSupport(ops: Dict) -> Mesh :
    if 'ls' in ops:
        if 'support' in ops:
            raise RuntimeError('Error cant specify ls and support')
        else:
            return ops['ls'].support
    else:
        if 'support' in ops:
            return  ops['support']
        else:
            raise RuntimeError('Error need to specify ls or support')

def CheckIntegrity(GUI : bool= False):
    return "ok"

if __name__ == "__main__":  # pragma: no cover
    print(CheckIntegrity(GUI=True))

