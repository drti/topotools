# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
import numpy as np
import Muscat.Helpers.ParserHelper as PH

from OpenPisco.Actions.ActionFactory import RegisterClass
from OpenPisco.Actions.ActionBase import ActionBase
from OpenPisco.BaseElements import elementsTypeSupportedForDimension
from OpenPisco.Unstructured.LevelsetTools import TakePhiandMeshFromXdmf

from OpenPisco import RETURN_SUCCESS


class UpdateDistance(ActionBase):

    def __init__(self):
       super(UpdateDistance,self).__init__()

    def Apply(self,app, ops):
        ls = ops['ls']
        length = None
        if "length" in ops:
            length = PH.ReadFloat(ops["length"])

        ls.Reinitialize(length=length)
        return RETURN_SUCCESS

RegisterClass("UpdateDistance",UpdateDistance)

class FieldTransfert(ActionBase):
    def __init__(self):
       super(FieldTransfert,self).__init__()

    def Apply(self, app, ops):
          from Muscat.Containers.ConstantRectilinearMeshTools import GetNodeTransferMatrix, GetElementTransferMatrix

          fieldFrom = app.levelSets[int(ops['from'])]
          gridFrom = fieldFrom.support

          fieldTo = app.levelSets[int(ops['to'])]
          gridTo = fieldTo.support

          if fieldFrom.IsNodal():
              if gridFrom.props.get("IsConstantRectilinear",False):
                  tMatrix = GetNodeTransferMatrix(gridFrom,gridTo)
              else:
                  from Muscat.FE.Fields.FEField import FEField
                  from Muscat.FE.FETools import PrepareFEComputation
                  space, numberings, _, _ = PrepareFEComputation(gridFrom)
                  phiFEField = FEField(name="phi",mesh=gridFrom,space=space,numbering=numberings[0])
                  from Muscat.Containers.MeshFieldOperations import GetFieldTransferOp
                  tMatrix,_, _ = GetFieldTransferOp(phiFEField,gridTo.nodes,method = "Interp/Clamp",verbose=False)
          else:
              assert gridFrom.props.get("IsConstantRectilinear",False),"Not coded jet"
              tMatrix = GetElementTransferMatrix(gridFrom,gridTo)

          fieldTo.phi = tMatrix*fieldFrom.phi

          return RETURN_SUCCESS

RegisterClass("LevelSetTransfert",FieldTransfert)
RegisterClass("FieldTransfert",FieldTransfert)


class SetOriginalSupport(ActionBase):
    def __init__(self):
        super(SetOriginalSupport,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']
        ls.originalSupport = ls.support
        return RETURN_SUCCESS

RegisterClass("SetOriginalSupport",SetOriginalSupport)

class InitLevelset(ActionBase):
    def __init__(self):
        super(InitLevelset,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']
        if ops['zone'] == "support":
            from Muscat.ImplicitGeometry.ImplicitGeometryBase import ImplicitGeometryDelayedInit
            ls.Initialize(ImplicitGeometryDelayedInit("All",{"support":ls.support}).ApplyVector)
        else:
            ls.Initialize(ops['zone'])

        if "UpdateDistance" in ops and PH.ReadBool(ops["UpdateDistance"]):
            return UpdateDistance().Apply(app, ops)
        return RETURN_SUCCESS

RegisterClass("InitLevelset",InitLevelset)

class LevelSetOffset(ActionBase):
    def __init__(self):
        super(LevelSetOffset,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']
        val = PH.ReadFloat(ops['val'])
        ls.phi += val

        return RETURN_SUCCESS

RegisterClass("LevelSetOffset",LevelSetOffset)

class LevelSetChangePhiByZones(ActionBase):
    def __init__(self):
        super(LevelSetChangePhiByZones,self).__init__()

    def Apply(self,app, ops):
         ls = ops['ls']
         if 'sign' in ops:
             sgn = PH.ReadFloat(ops['sign'])
         else:
             sgn = 1.

         on = ops['zone'].ApplyVector(ls.support)
         ls.phi[:] = sgn*on

         return RETURN_SUCCESS
RegisterClass("ChangePhiByZones",LevelSetChangePhiByZones)

class InitFrom(ActionBase):
    def __init__(self):
        super(InitFrom,self).__init__()

    def Apply(self, app, ops):
        #<Action type="InitFrom" ls="1" file="/path/to/file.xmf" />

        fname = ops['file']
        ls = ops['ls']

        TakePhiandMeshFromXdmf(ls,fname)

        return RETURN_SUCCESS

RegisterClass("InitFrom",InitFrom)

class InitWith(ActionBase):
    def __init__(self):
        super(InitWith,self).__init__()

    def Apply(self, app, ops):
        #<Action type="InitWith" ls="1" file="/path/to/file.xmf" />

        fname = ops['file']
        ls = ops['ls']

        from Muscat.IO.XdmfReader import XdmfReader
        f = XdmfReader(fname)
        f.lazy = True
        f.Read()
        gridcpt = -1
        def GetGrid():
            print(f"using grid {gridcpt}")
            return f.xdmf.GetDomain(0).GetGrid(gridcpt)
        lastgrid = GetGrid()

        # verification of the number of nodes
        if lastgrid.GetSupport().GetNumberOfNodes() != ls.support.GetNumberOfNodes():
            gridcpt -= 1
            lastgrid = GetGrid()

        if "field" in ops:
            fieldToRead = ops["field"]
        else:
            fieldToRead = "phi"

        if fieldToRead not in lastgrid.GetPointFieldsNames():
            gridcpt -= 1
            print(f"Field not found in the last grid. trying in grind {gridcpt}")
            lastgrid = GetGrid()

        ls.phi = lastgrid.GetFieldData(fieldToRead,noTranspose=True)

        if lastgrid.geometry.Type == "ORIGIN_DXDYDZ":
            #dont know why I have to do this
            s = ls.phi.shape
            ls.phi.shape = np.fliplr([s])[0]
            ls.phi = ls.phi.T.flatten()

        return RETURN_SUCCESS

RegisterClass("InitWith",InitWith)

class ComputeLevelsetStatistics(ActionBase):
    """ Action to compute and print statistics of a levelset:
        ls :

    """
    def __init__(self):
        super(ComputeLevelsetStatistics,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']
        label = ops.get('label', id(ls))

        lsmin = np.min(ls.phi)
        lsmax = np.max(ls.phi)
        from OpenPisco.Optim.Criteria.GeoCriteria import TopoCriteriaVolume
        tc = TopoCriteriaVolume()
        tc.UpdateValues(ls)
        lsVolumeInside3D = tc.GetValue()

        tc.UpdateValuesUsingPhi(ls,ls.phi)
        lsVolume = tc.GetValue()


        tc.UpdateValuesUsingPhi(ls,np.zeros_like(ls.phi) - 1 )

        totalVolume = tc.GetValue()
        percentageVolume = lsVolume/totalVolume
        boundingMin, boundingMax = ls.support.ComputeBoundingBox()

        res  = " *** LS ({}) Statistics *** \n".format(label)
        res += " Mesh Min Bounding: {} \n".format(" ".join([str(x) for x in boundingMin]))
        res += " Mesh Max Bounding: {} \n".format(" ".join([str(x) for x in boundingMax]))
        res += " Ls Minimum       : {} \n".format(lsmin)
        res += " Ls Maximum       : {} \n".format(lsmax)
        res += " Ls Volume        : {} \n".format(lsVolume)
        res += " Inside3D Volume  : {} \n".format(lsVolumeInside3D)
        res += " Mesh Total Volume: {} \n".format(totalVolume)
        res += " Ls Percent Volume: {:.2%}%\n".format(percentageVolume)

        print(res)

        return RETURN_SUCCESS

RegisterClass("LsStats", ComputeLevelsetStatistics)

class ComputeLevelSetSupportLengthScale(ActionBase):
    """ Action to compute and print level set support min/max lengthscale:
        ls : level set

    """
    def __init__(self):
        super(ComputeLevelSetSupportLengthScale,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']
        label = ops.get('label', id(ls))

        from  Muscat.Containers.MeshInspectionTools import ComputeMeshMinMaxLengthScale
        hmin,hmax =  ComputeMeshMinMaxLengthScale(ls.support)

        res  = " *** LS ({}) Support LengthScale *** \n".format(label)
        res += " Mesh HMin : {} \n".format(hmin)
        res += " Mesh HMax : {} \n".format(hmax)

        print(res)

        return RETURN_SUCCESS

RegisterClass("LsSupportLengthScale", ComputeLevelSetSupportLengthScale)

class ComputeLevelsetQualityInfo(ActionBase):
    """ Action to compute and print mesh quality info of a levelset:
        ls :

    """
    def __init__(self):
        super(ComputeLevelsetQualityInfo,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']

        from OpenPisco.MuscatExtentions.MeshQualityControlTools import ComputeAspectRatioOnBodyElements
        qualityInfo = ComputeAspectRatioOnBodyElements(ls.support)

        dimensionality = ls.support.GetElementsDimensionality()
        elementType=elementsTypeSupportedForDimension[dimensionality]

        res  = " *** LS Mesh Quality Info *** \n"
        typeOfQuantity = "body element Aspect Ratio       : {} \n"
        res += " Best "+str(elementType)+" "+typeOfQuantity.format(np.min(qualityInfo))
        res += " Worst "+str(elementType)+" "+typeOfQuantity.format(np.max(qualityInfo))
        res += " Mean  "+str(elementType)+" "+typeOfQuantity.format(np.mean(qualityInfo))
        print(res)
        return RETURN_SUCCESS

RegisterClass("LsQualityInfo", ComputeLevelsetQualityInfo)

class AddExteriorsZerosAction(ActionBase):
    def __init__(self):
        super(AddExteriorsZerosAction,self).__init__()

    def Apply(self, app, ops):
        import Muscat.ImplicitGeometry.ImplicitGeometryObjects as IGObs
        import Muscat.ImplicitGeometry.ImplicitGeometryOperators as IGOps
        ls = ops['ls']

        OnZone = None
        if 'OnZone' in ops:
            onZone =  app.zones[int(ops['OnZone'])]

        if ls.support.props.get("IsConstantRectilinear",False) :
            exterieur = IGObs.ImplicitGeometryAxisAlignBox(origin=ls.support.GetOrigin(),size=(ls.support.props.get("dimensions")-1)*ls.support.props.get("spacing"))

        else:
            from Muscat.Containers.MeshModificationTools import ComputeSkin
            skinmesh = ComputeSkin(ls.support )
            exterieur = IGObs.ImplicitGeometryStl()
            exterieur.SetSurface(skinmesh )

        offset = float(ops.get('offset',-1))
        if 'nTags' in ops:
            offset = float(ops.get('offset',-1))
            tagnames = PH.ReadStrings(ops['nTags'])
            ll = abs(np.copy(ls.phi))
            for tn in tagnames :
                ids = ls.support.nodesTags[tn].GetIds()
                ll[ids] = -abs(ls.phi[ids]) + offset

            on = IGObs.ImplicitGeometryWrapped(field=ll)
            exterieur = IGOps.ImplicitGeometryUnion([exterieur,on])

        if 'eTags' in ops:
            tagnames = PH.ReadStrings(ops['eTags'])
            ll = abs(np.copy(ls.phi))
            from Muscat.Containers.Filters.FilterObjects import ElementFilter
            ff = ElementFilter(tags=tagnames)
            for _, data, ids in ff(ls.support):
                ids = data.GetNodesIdFor(ids)
                ll[ids] = -abs(ls.phi[ids]) + offset

            on = IGObs.ImplicitGeometryWrapped(field=ll)
            exterieur = IGOps.ImplicitGeometryUnion([exterieur,on])

            # intersection of the exterieus surface
        ls.phi[:] = np.maximum(exterieur.ApplyVector(ls.support),ls.phi)

        return RETURN_SUCCESS

RegisterClass("AddExteriorsZeros",AddExteriorsZerosAction)


def CheckIntegrity():
    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity())
