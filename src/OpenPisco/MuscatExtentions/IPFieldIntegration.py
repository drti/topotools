# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
import numpy as np
from Muscat.FE.Spaces.FESpaces import ConstantSpaceGlobal
from Muscat.FE.DofNumbering import ComputeDofNumbering
from Muscat.FE.Fields.FEField import FEField
from Muscat.FE.SymWeakForm import GetField
from Muscat.FE.SymWeakForm import GetTestField
from Muscat.FE.Integration import IntegrateGeneral

def IPFieldIntegration(ipfield, ff):

    mesh = ipfield.mesh

    space = ConstantSpaceGlobal

    ipfield_name = ipfield.name
    # create a constant  test field to do the integration
    numbering = ComputeDofNumbering(mesh,space)
    ## this is trivial. all values goes to the same "dof"
    testfield = FEField("Testfunc",mesh=mesh,space=space,numbering=numbering)

    ########### Symbolic part ###########

    # this is the test fuction. we must use the same name as before
    Tt = GetTestField("Testfunc",1)
    ## this is the symboly variable to be integrated, same as
    rho = GetField(ipfield_name,1)
    ## the weak formulation to be integrated
    wf = rho.T*Tt
    ########### end of the symbolic part ###########

    #### the integration ###
    ### the constants : (empty here )
    constants = {}
    ### the list of the extra fields
    fields = [ipfield]
    ### the unknownFields (the order is important for the order in the matrix )
    ### only test field is given  -> galerking projection test functions are the
    ### same as the trial functions

    unknownFields = [ testfield ]

    ### do the integration
    _,F = IntegrateGeneral(mesh=mesh,
                    wform=wf,
                    constants=constants,
                    fields=fields,
                    unknownFields=unknownFields,
                    integrationRule=ipfield.rule,
                    elementFilter=ff)

    res = F[0]

    return res

def CheckIntegrity():
    import Muscat.Containers.ElementsDescription as ED
    from Muscat.Containers.MeshCreationTools import CreateCube
    mesh = CreateCube([2.,3.,4.],[-1.0,-1.0,-1.0],[2./10, 2./10,2./10])
    from Muscat.FE.Fields.IPField import IPField
    from Muscat.FE.IntegrationRules import GetIntegrationRuleByName
    LP1 = GetIntegrationRuleByName("LagrangeP1Quadrature")
    rho_field = IPField("rho",mesh=mesh,rule=LP1)
    factor = 1
    rho_field.Allocate(factor)
    print("field for Hexaedron 6 elements, 8 integration points")
    print(rho_field.GetDataFor(ED.Hexahedron_8))
    from Muscat.Containers.Filters.FilterObjects import ElementFilter
    ff = ElementFilter(dimensionality=3)
    print("volume is " , IPFieldIntegration(rho_field,ff ) )
    ff = ElementFilter(dimensionality=2)
    print("surface is " , IPFieldIntegration(rho_field,ff ) )
    return "ok"

if __name__ == "__main__":# pragma: no cover
    print(CheckIntegrity())
