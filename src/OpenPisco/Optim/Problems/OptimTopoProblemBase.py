# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import numpy as np

from Muscat.Containers.Filters.FilterObjects import ElementFilter
import Muscat.Helpers.ParserHelper as PH
from Muscat.ImplicitGeometry.ImplicitGeometryObjects import ImplicitGeometryWrapped
from Muscat.Containers.Filters.FilterTools import ElementFilterToImplicitField
from Muscat.Helpers.Logger import Info, Debug, GetDiffTime

import OpenPisco.ExtractConnexPart as ECP
from OpenPisco.Optim.Problems.OptimProblemConcrete import OptimProblemConcrete

class OptimTopoProblemBase(OptimProblemConcrete):
    def __init__(self,other = None, dataDeepCopy = True):
        super(OptimTopoProblemBase,self).__init__(other=other)

        if  other is not None:
            self.point = type(other.point)(other.point)

            self.TakeValuesFrom(other)
            if hasattr(other.point, 'originalSupport'):
                self.point.originalSupport = other.point.originalSupport
            else:
                Debug("originalSupport not found")

            self.e2 = other.e2
            self.OnZone = other.OnZone
            self.OnFilter = other.OnFilter
            self.OffZone = other.OffZone
            self.OffFilter = other.OffFilter
            self.OnETags = PH.ReadStrings(other.OnETags)
            self.OffETags = PH.ReadStrings(other.OffETags)
            self.GenerateImplicitZonesFromTags()
            self.writer = other.writer
            self.hj_iter = other.hj_iter
            self.outputEveryLevelsetUpdate = other.outputEveryLevelsetUpdate
            if other.connexPartOp is None :
                 other.connexPartOp = ECP.ConnexPart(self.point.support)
            self.connexPartOp = other.connexPartOp
            self.maxChange = other.maxChange
            self.printMeshQualityInfo = other.printMeshQualityInfo
            self.adaptRegularizationToMeshSizeMin = other.adaptRegularizationToMeshSizeMin
        else:
            ##Regularisation Parameter
            self.e2 = 2.
            ## max volume change
            self.maxChange = {}
            self.maxChange['Volume'] =  0.2  # [0 1]
            self.point = None
            self.OnZone = None
            self.OnFilter = None
            self.OffZone = None
            self.OffFilter = None
            self.OnETags = np.zeros(0,dtype=str)
            self.OffETags = np.zeros(0,dtype=str)
            self.writer = None
            self.hj_iter = 10
            self.outputEveryLevelsetUpdate = False
            self.connexPartOp = None
            self.adaptRegularizationToMeshSizeMin = False
            self.printMeshQualityInfo = False

    def GetCurrentPoint(self):
        return self.point

    def TakeValuesFrom(self,other):
        super(OptimTopoProblemBase,self).TakeValuesFrom(other)
        self.point.TakeValuesFrom(other.point)

    def UpdateValues(self):
        self.point.StoreState()
        res = super().UpdateValues()
        self.point.DiscardState()
        return res

    def ApplyDomainConstraints(self,point):

        if self.OnZone is not None:
            on = self.OnZone(point.support)
            res = np.minimum(point.phi,on)
        else:
            res = np.copy(point.phi)

        if self.OnFilter is not None:
            onphi = ElementFilterToImplicitField(self.OnFilter, mesh=point.support )
            np.minimum(res,onphi,out=res)

        if self.OffZone is not None:
            off = self.OffZone(point.support)
            np.maximum(res,-off,out=res)

        if self.OffFilter is not None:
            offphi = ElementFilterToImplicitField(self.OffFilter, mesh=point.support )
            np.maximum(res,-offphi,out=res)
        return  res

    def PrintHeader(self):
        print("Laplace filter constant : " + str(self.e2))
        super().PrintHeader()

    def GenerateImplicitZonesFromTags(self):
        offset = 1e-3
        tags = [self.OnETags,self.OffETags]
        zones = ["OnZone","OffZone"]
        for tag,zone in zip(tags,zones):
            if len(tag):
                EF = ElementFilter(eTag=tag)
                implicitfield = ElementFilterToImplicitField(EF, self.point.support) - offset
                setattr(self,zone,ImplicitGeometryWrapped(implicitfield))

    def PreStartCheck(self):
        boundaryFilters = self.RetrievePhysicalBoundaryFilters()
        for boundaryFilter in boundaryFilters:
            boundaryNodes = boundaryFilter.GetNodesIndices(self.point.support)
            if (self.OffZone is not None)  and np.any(self.OffZone(self.point.support)[boundaryNodes] <=(1.e-16)):# pragma: no cover
                print('Error : "Boundary conditions cannot be applied to off region')
                return False
        self.point.StoreState()
        self.point.AcceptChanges(self.ApplyDomainConstraints(self.point))
        return True

    def GetDirectionFromGradient(self,raw_v):
        if self.adaptRegularizationToMeshSizeMin and self.point.conform:
            from  Muscat.Containers.MeshInspectionTools import ExtractElementByTags
            import OpenPisco.TopoZones as TZ
            surf = ExtractElementByTags(self.point.support, [TZ.InterSurf])
            if surf.GetNumberOfElements() == 0 :
               print("Warning iso not found")
            from  Muscat.Containers.MeshInspectionTools import ComputeMeshMinMaxLengthScale
            h =  ComputeMeshMinMaxLengthScale(surf)[0]
            alpha = np.maximum(2*h**2,self.e2)
        else:
            alpha = self.e2

        temp_velo = -self.point.Regularize(raw_v,lengthscaleParameter=alpha)

        return temp_velo.ravel()

    def DirectionMagnitude(self, direction):
        """
        Parameters
        ----------
        direction : np.ndarray
            The (scalar) advection field

        Returns
        -------
        scalar
            The magnitude, more precisely the volume swept
        """
        return self.point.InterfaceIntegral(np.abs(direction))

    def Advance(self, direction, step_length):
        self.point.extraNodesFields['Direction'] = direction

        """
        Try to advance the levelset using an advection equation.

        Parameters
        ----------
        direction : np.ndarray
            The (scalar) advection velocity field.
        step_length : scalar
            The length of the pseudo-time integration interval.

        Returns
        -------
        bool
            Whether the advance was successful.

        Notes
        -----
        Two constraints are imposed on the advection process:
            1. The new optimization point must be valid, meaning that optimization criteria must be able to update their values.
            For instance, if it is relevant, areas with Neumann conditions must always be connected to
            areas with Dirichet conditions. However, this remains the responsibility of each underlying physical problem.
            2. A change in the volume must be less than a given threshold.
        """
        assert self.point is not None,'Please allocate the levelset first'

        # Make a copy of the level-set function in case the step fails
        self.point.StoreState()

        # Force recomputation of volume for current configuration
        # TODO Avoid a spurious volume evaluation
        phitemp = self.point.phi

        from OpenPisco.Optim.Criteria.GeoCriteria import TopoCriteriaVolume
        volumeCalculation = TopoCriteriaVolume()
        volumeCalculation.UpdateValuesUsingPhi(self.point,phitemp)
        vol_0 = volumeCalculation.GetValue()
        Debug(vol_0)

        # Transport parameters

        # Do the levelset update
        Debug("LevelSetUpdate")
        self.LevelSetUpdate(direction, step_length)

        Debug("Apply Constraints")
        phitemp = self.ApplyDomainConstraints(self.point)
        
        Debug("Apply ConnexPart")
        boundaryFilters = list(self.RetrievePhysicalBoundaryFilters())
        if boundaryFilters:
            boundaryNodes = boundaryFilters[0].GetNodesIndices(self.point.support)
            fix = np.ones_like(self.point.phi)
            fix[boundaryNodes] *=-1
            if  self.connexPartOp is None or self.point.support is not self.connexPartOp.support:
                self.connexPartOp = ECP.ConnexPart(self.point.support)
            self.point.phi = self.connexPartOp.Apply(phitemp, fix, self.point.phi)
            phitemp = self.connexPartOp.Apply(phitemp, fix, phitemp)
        
        # Check both failure conditions
        # 1) Verify optim domain is valid
        ok = self.VerifyOptimDomainValidity(self.point.support,phitemp)
        if ok:
            # 2) Verify that volume variation is not too large (only if precedent test is ok )
            volumeCalculation.UpdateValuesUsingPhi(self.point,phitemp)
            vol_c = volumeCalculation.GetValue()
            Debug(vol_c)
            relative_volume_change = abs(vol_c - vol_0) / vol_0
            if relative_volume_change > self.maxChange['Volume']:
                ok = False
                Info(" ".join(("Volume change too large :",
                        str(relative_volume_change), ">",
                        str(self.maxChange['Volume']))))
        else:
            vol_c = -1

        if ok:
            self.point.AcceptChanges(phitemp)
        else:
            Info(" Rejected Point ")
            # Reject step
            if self.outputEveryLevelsetUpdate:
                self.SaveData(data={"ControlVolume":vol_c,"OK":0.0}, point=self.point)
            self.point.DiscardState()

        return ok

    def LevelSetUpdate(self, direction, step_length):
        Debug("in LevelSetUpdate")
        self.point.TransportAndReinitialize(direction, step_length)
        Debug("in LevelSetUpdate Done")

    def ResetState(self):
        if self.point is not None:
            self.point.ResetState()

    def SaveData(self,data,point=None, gradient= None, direction = None, onlyphi=None, ):

        if self.writer is None: return

        Info("********************** outout ***********************")

        GridFieldsNames = list(data.keys())
        GridFields = list(data.values())

        def GetValsOfCriteriaAndSubs(c):
            res = []
            prefix = c.GetName()
            from OpenPisco.Optim.Criteria.ComposedCriteria import ComposedCriteria
            if issubclass(type(c), ComposedCriteria):
                for i in range(c.GetNumberOfInternalCriteria()):
                    ic = c.GetInternalCriteria(i)
                    res.extend([ (prefix+"_"+n,v) for n,v in GetValsOfCriteriaAndSubs(ic) ])
            res.append((prefix,c.GetValue()))
            return res

        res = GetValsOfCriteriaAndSubs(self.objectiveCriterion)
        GridFieldsNames.extend([n for n,v in res])
        GridFields.extend([v for n,v in res])

        for c_i in range(self.GetNumberOfInequalityConstraints()):
            res = GetValsOfCriteriaAndSubs(self.inequalityconstraintsCriteria[c_i])
            GridFieldsNames.extend([n for n,v in res])
            GridFields.extend([v for n,v in res])


        for c_i in range(self.GetNumberOfEqualityConstraints()):
            res = GetValsOfCriteriaAndSubs(self.equalityConstraintsCriteria[c_i])
            GridFieldsNames.extend([n for n,v in res])
            GridFields.extend([v for n,v in res])

        ## add time for debugging
        GridFieldsNames.append("time")
        GridFields.append(GetDiffTime() )

        scalarOutputs = self.GetScalarsOutputs()

        if self.point.conform and self.printMeshQualityInfo:
           from OpenPisco.MuscatExtentions.MeshQualityControlTools import ComputeAspectRatioOnBodyElements
           qualityInfo = ComputeAspectRatioOnBodyElements(self.point.support)
           scalarOutputs["MeshQualityInfo"] = np.max(qualityInfo)

        GridFieldsNames.extend( scalarOutputs.keys() )
        GridFields.extend( scalarOutputs.values() )

        if onlyphi is not None:
            if self.point.IsNodal():
                self.writer.Write(self.point.support,
                                  PointFields     = [onlyphi],
                                  PointFieldsNames= ['phi'],
                                  GridFields      = GridFields ,
                                  GridFieldsNames = GridFieldsNames
                                  )
            else:
                self.writer.Write(self.point.support,
                                  CellFields     = [onlyphi],
                                  CellFieldsNames= ['phi'],
                                  GridFields      = GridFields ,
                                  GridFieldsNames = GridFieldsNames
                                  )
            return

        PointFields = []
        PointFieldsNames = []

        CellFields = []
        CellFieldsNames = []

        if self.point.IsNodal():
            Fields =  PointFields
            FieldsNames = PointFieldsNames
        else:
            Fields =  CellFields
            FieldsNames = CellFieldsNames

        if point is None:
            f = self.point.phi
        else:
            f = point.phi
        Fields.append(f)
        FieldsNames.append('phi')

        if direction is not None:
            Fields.append(direction)
            FieldsNames.append('direction')

        if gradient is not None:
            Fields.append(gradient)
            FieldsNames.append('grad')

        if self.OnZone is not None:
            Fields.append(self.OnZone(self.point.support))
            FieldsNames.append('On')

        if self.GetNumberOfSolutions() == 1:
            PointFields.append(self.GetSolution(0))
            PointFieldsNames.append(self.GetSolutionName(0))
        else:
            # if we have a solution with the same number we add a '_' at the end
            almanac = {}
            for d in range(self.GetNumberOfSolutions()):
                PointFields.append(self.GetSolution(d))
                name = self.GetSolutionName(d)
                while name in almanac:
                    name += '_'
                PointFieldsNames.append(name)

        self.writer.Write(self.point.support,
                PointFields     = PointFields,
                PointFieldsNames= PointFieldsNames,
                CellFields      = CellFields,
                CellFieldsNames = CellFieldsNames,
                GridFields      = GridFields ,
                GridFieldsNames = GridFieldsNames
                )

def CheckIntegrity():
    OptimTopoProblemBase()
    return 'ok'

if __name__ == '__main__':# pragma: no cover
    print(CheckIntegrity())
