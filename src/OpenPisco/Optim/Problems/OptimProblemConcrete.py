# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import itertools

from Muscat.Helpers.CPU import GetNumberOfAvailableCores
from Muscat.Helpers.Logger import Info

from OpenPisco import RETURN_SUCCESS
from OpenPisco.Optim.Problems.OptimProblemBase import OptimProblemBase
from OpenPisco.Optim.Criteria.Criteria import PhysicalCriteriaBase

class OptimProblemConcrete(OptimProblemBase):
    def __init__(self, other=None, dataDeepCopy=True):
        super(OptimProblemConcrete, self).__init__(other)

        self.objectiveCriterion = None
        self.inequalityconstraintsCriteria = []
        self.equalityConstraintsCriteria = []
        self.inequalityconstraintsStatus = []
        self.equalityConstraintsStatus = []
        self.nbCpu = GetNumberOfAvailableCores()
        self.problemFields = []
        self.problemScalars = []

        if other is not None:
            # copy the ObjectiveCriterion
            self.SetObjectiveCriterion( type(other.objectiveCriterion)(other.objectiveCriterion) )

            # copy the Constraints
            for crit,status in zip(other.inequalityconstraintsCriteria,other.inequalityconstraintsStatus):
                self.AddInequalityConstraint(type(crit)(crit),status)
            for crit,status in zip(other.equalityConstraintsCriteria,other.equalityConstraintsStatus):
                self.AddEqualityConstraint(type(crit)(crit),status)

            # copy the extra field and scalars
            for problem,fieldName,location in other.problemFields:
                self.AddProblemFields(problem=problem,fieldNames=[fieldName],locations=[location])
            for problem,scalarName in other.problemScalars:
                self.AddProblemScalars(problem=problem,scalarNames=[scalarName])

        else:
            self.CleanConstraints()

    def TakeValuesFrom(self,other):
         OptimProblemConcrete.__init__(self,other,True)

    def UpdateValues(self):
        #Modify problems contained in criteria for auxiliary quantities and retrieve unique ones
        uniquePhysicalProblems = self.RetrieveUniquePhysicalProblems()
        self.SolvePhysicalProblems(uniquePhysicalProblems)
        res = self.UpdateAllCriteria()
        return res

    def VerifyOptimDomainValidity(self,support,phi):
        uniquePhysicalProblems = self.RetrieveUniquePhysicalProblems()
        for physicalProblem in uniquePhysicalProblems:
            physicalProblemValidityState = physicalProblem.VerifyPhysicalDomainValidity(support,phi)
            if not physicalProblemValidityState:
                Info("Physical problem "+str(physicalProblem)+" domain not valid")
                return False
        return True

    def RetrievePhysicalBoundaryFilters(self):
        uniquePhysicalProblems = self.RetrieveUniquePhysicalProblems()
        physicalBoundaryFilters = itertools.chain.from_iterable([ itertools.chain.from_iterable(physicalProblem.GetBoundaryConditionsFilters()) 
                                                 for physicalProblem in uniquePhysicalProblems])
        return physicalBoundaryFilters

    def RetrieveUniquePhysicalProblems(self):
        optimProblemComponents=[self.objectiveCriterion]+self.GetConstraintsCriteria()

        #Specify to the problems all the auxiliary quantities (fields/scalars) required by each criterion
        uniquePhysicalProblems=set()
        def AddProblems(optimProblemComponent):
            if isinstance(optimProblemComponent, PhysicalCriteriaBase):
                optimProblemComponent.SetAuxiliaryQuantities(self.point)
                uniquePhysicalProblems.add(optimProblemComponent.problem)

            if hasattr( optimProblemComponent, "internalCriteria"):
                for c in optimProblemComponent.internalCriteria:
                    AddProblems(c)

        for optimProblemComponent in optimProblemComponents:
            AddProblems(optimProblemComponent)
        return uniquePhysicalProblems

    def SolvePhysicalProblems(self,physicalProblems):
        #Prepare to generate extra fields/scalars
        for problem,field,location in self.problemFields:
            problem.SetAuxiliaryFieldGeneration(field,on=location)

        for problem,scalar in self.problemScalars:
            problem.SetAuxiliaryFieldGeneration(scalar)

        #Now, we solve each unique physical problems once in parallel
        if self.nbCpu == 1:
            for physicalProblem in physicalProblems:
                result = physicalProblem.SolveByLevelSet(self.point)
                assert result== RETURN_SUCCESS,"Error solving " + str(physicalProblem)
        else:
            import concurrent.futures
            with concurrent.futures.ThreadPoolExecutor(max_workers=self.nbCpu) as executor:
                allDirectProblems = [
                                     executor.submit(physicalProblem.SolveByLevelSet, self.point) 
                                     for physicalProblem in physicalProblems
                                     ]
                for problem in allDirectProblems:
                    assert problem.result() == RETURN_SUCCESS,"Error solving " + str(problem)

    def UpdateAllCriteria(self):
        res = self.objectiveCriterion.UpdateValues(self.point)
        if res == RETURN_SUCCESS:
            for Criterion in self.GetConstraintsCriteria():
                res = Criterion.UpdateValues(self.point)
                if res != RETURN_SUCCESS:
                    print("Error on the criterion " + str(Criterion.GetName())  )
                    print(str(Criterion)  )
                    break
        else:
            print("Error on the Objective Criterion" + str(self.objectiveCriterion.GetName())  )
            print(str(self.objectiveCriterion)  )
        return res

    def UpdateProblemWhenIterationAccepted(self):
        self.objectiveCriterion.UpdateCriteriaWhenIterationAccepted()
        for criterion in self.GetConstraintsCriteria():
           criterion.UpdateCriteriaWhenIterationAccepted()

    def GetConstraintsCriteria(self):
        return self.inequalityconstraintsCriteria+self.equalityConstraintsCriteria

    def GetNumberOfSolutions(self):
        n = self.objectiveCriterion.GetNumberOfSolutions()
        for crit in self.GetConstraintsCriteria():
            n += crit.GetNumberOfSolutions()
        n +=len(self.problemFields)
        return n

    def GetSolutionName(self,idx):
        internalcpt = 0
        for i in range(self.objectiveCriterion.GetNumberOfSolutions()):
            if internalcpt == idx:
                return "O_" + self.objectiveCriterion.GetSolutionName(i)
            internalcpt += 1

        for crit,_ in zip(self.inequalityconstraintsCriteria,range(len(self.inequalityconstraintsCriteria))):
            for i in range(crit.GetNumberOfSolutions()):
               if internalcpt == idx:
                   return f"C_{crit.GetName()}_{crit.GetSolutionName(i)}"
               internalcpt += 1

        for crit,_ in zip(self.equalityConstraintsCriteria,range(len(self.equalityConstraintsCriteria))):
            for i in range(crit.GetNumberOfSolutions()):
               if internalcpt == idx:
                    return f"EC_{crit.GetName()}_{crit.GetSolutionName(i)}"
               internalcpt += 1

        for _,fieldName,location in self.problemFields:
            if internalcpt == idx:
                return f"XF_{fieldName}_{location}"
            internalcpt += 1

        raise Exception("SolutionName "+str(idx)+" not avilable ")

    def GetSolution(self,idx):
        internalcpt = 0
        for i in range(self.objectiveCriterion.GetNumberOfSolutions()):
            if internalcpt == idx:
                return self.objectiveCriterion.GetSolution(i)
            internalcpt += 1

        for crit in self.GetConstraintsCriteria():
            for i in range(crit.GetNumberOfSolutions()):
               if internalcpt == idx:
                   return crit.GetSolution(i)
               internalcpt += 1

        for problem,fieldName,location in self.problemFields:
            if internalcpt == idx:
                return problem.GetAuxiliaryField(fieldName,on=location)
            internalcpt += 1

        raise Exception("Solution "+str(idx)+" not available ")

    def GetScalarsOutputs(self):
        res = dict()
        for name,value in self.objectiveCriterion.GetScalarsOutputs().items():
            res["O_"+ name] = value

        for crit,ccpt in zip(self.inequalityconstraintsCriteria,range(len(self.inequalityconstraintsCriteria))):
            for name,value in crit.GetScalarsOutputs().items():
                res["IC"+ str(ccpt)+"_"+name] = value

        for crit,ccpt in zip(self.equalityConstraintsCriteria,range(len(self.equalityConstraintsCriteria))):
            for name,value in crit.GetScalarsOutputs().items():
                res["EC"+ str(ccpt)+"_"+name] = value

        for problem,scalarName,ccpt in zip(self.problemScalars,range(len(self.problemScalars))):
            res["XS"+ str(ccpt)+"_"+scalarName] = problem.GetAuxiliaryScalar(scalarName)

        return res

    def SetObjectiveCriterion(self,objCrit):
        self.objectiveCriterion = objCrit

    def GetObjectiveFunctionName(self):
        return self.objectiveCriterion.GetName()

    def GetObjectiveFunctionVal(self):
        return self.objectiveCriterion.GetValue()

    def GetObjectiveFunctionSensitivity(self):
        return self.objectiveCriterion.GetSensitivity()

    def CleanConstraints(self):
        self.inequalityconstraintsCriteria = []
        self.inequalityconstraintsStatus = []

    def AddInequalityConstraint(self,objCrit,status=True):
        self.inequalityconstraintsCriteria.append(objCrit)
        self.inequalityconstraintsStatus.append(status)

    def AddEqualityConstraint(self,objCrit,status=True):
        self.equalityConstraintsCriteria.append(objCrit)
        self.equalityConstraintsStatus.append(status)

    def AddProblemFields(self,problem,fieldNames,locations):
        for fieldName,location in zip(fieldNames,locations):
            self.problemFields.append((problem,fieldName,location))

    def AddProblemScalars(self,problem,scalarNames):
        for scalarName in scalarNames:
            self.problemScalars.append((problem,scalarName))

    def GetNumberOfConstraints(self):
        return self.GetNumberOfInequalityConstraints()+self.GetNumberOfEqualityConstraints()

    def GetNumberOfInequalityConstraints(self):
        return len(self.inequalityconstraintsCriteria)

    def GetNumberOfEqualityConstraints(self):
        return len(self.equalityConstraintsCriteria)

    def SetInequalityConstraintStatus(self,idx,status):
        self.inequalityconstraintsStatus[idx] = status

    def GetInequalityConstraintStatus(self,idx):
        return self.inequalityconstraintsStatus[idx]

    def SetEqualityConstraintStatus(self,idx,status):
        self.equalityConstraintsStatus[idx] = status

    def GetEqualityConstraintStatus(self,idx):
        return self.equalityConstraintsStatus[idx]

    def GetInequalityConstraintName(self,idx):
        return self.inequalityconstraintsCriteria[idx].GetName()

    def GetEqualityConstraintName(self,idx):
        return self.equalityConstraintsCriteria[idx].GetName()

    def GetInequalityConstraintVal(self,idx):
        return self.inequalityconstraintsCriteria[idx].GetValue()

    def GetEqualityConstraintVal(self,idx):
        return self.equalityConstraintsCriteria[idx].GetValue()

    def GetInequalityConstraintValOriginal(self, idx):
        return self.inequalityconstraintsCriteria[idx].GetValue()

    def GetInequalityConstraintUpperBoundOriginal(self,i):
        return self.inequalityconstraintsCriteria[i].GetUpperBound()

    def GetInequalityConstraintSensitivityVal(self,idx):
        return self.inequalityconstraintsCriteria[idx].GetSensitivity()

    def GetEqualityConstraintSensitivityVal(self,idx):
        return self.equalityConstraintsCriteria[idx].GetSensitivity()

    def GetInequalityConstraintSlacks(self):
        return {self.GetInequalityConstraintName(i):(self.GetInequalityConstraintValOriginal(i) - self.GetInequalityConstraintUpperBoundOriginal(i)) for i in range(self.GetNumberOfInequalityConstraints())}

    def GetInequalityConstraintUpperBound(self,i):
        return self.inequalityconstraintsCriteria[i].GetUpperBound()

    def GetEqualityConstraintTargetValue(self,i):
        return self.equalityConstraintsCriteria[i].GetTargetValue()


    def __str__(self):
        sInfo="Instance of "+type(self).__name__+"\n"
        if self.objectiveCriterion is not None:
            sInfo+="Objective criterion: "+self.GetObjectiveFunctionName()+"\n"
        sInfo+="Number of inequality constraint(s): "+str(self.GetNumberOfInequalityConstraints())+"\n"
        for inequalityConstraintCriteria,inequalityConstraintStatus in zip(self.inequalityconstraintsCriteria,self.inequalityconstraintsStatus):
            sInfo+="\t Name: "+str(inequalityConstraintCriteria.GetName())
            sInfo+=", Upperbound: "+str(inequalityConstraintCriteria.GetUpperBound())
            sInfo+=", Status: "+str(inequalityConstraintStatus)+"\n"
        sInfo+="Number of equality constraint(s): "+str(self.GetNumberOfEqualityConstraints())+"\n"
        for equalityConstraintCriteria,equalityConstraintStatus in zip(self.equalityConstraintsCriteria,self.equalityConstraintsStatus):
            sInfo+="\t Name: "+str(equalityConstraintCriteria.GetName())
            sInfo+=", TargetValue: "+str(equalityConstraintCriteria.GetTargetValue())
            sInfo+=", Status: "+str(equalityConstraintStatus)+"\n"
        return sInfo

def CheckIntegrity():
    OPC = OptimProblemConcrete()
    print(OPC)
    return "OK"

if __name__ == '__main__':
    print(CheckIntegrity()) # pragma: no cover
