# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

from Muscat.Helpers.Factory import Factory
import Muscat.Helpers.ParserHelper as PH

def RegisterClass(name, classtype, constructor=None, withError=True):
    return ProblemFactory.RegisterClass(name, classtype, constructor=constructor, withError=withError)

def Create(name, ops=None):
   res = ProblemFactory.Create(name,ops, propertiesAssign = False)

   ## Just assign props
   props = ["OnZone", "OffZone"]
   PH.ReadProperties(ops, props, res, typeConversion=False)

   ## delete the already asigned info
   for key in props:
       del ops[key]

   if "ls" in ops:
       res.point = ops["ls"]
       del ops["ls"]

   PH.ReadProperties(ops, ops, res, typeConversion=True)

   return res

def CreateDerived(name, ops=None):
   ops["internalOptimProblem"] = ops.get("optimProblem", None)
   del ops["optimProblem"]
   res = ProblemFactory.Create(name)
   PH.ReadProperties(ops, ops, res, typeConversion=True)
   return res

class ProblemFactory(Factory):
    _Catalog = {}
    _SetCatalog = set()

    def __init__(self):
        super(ProblemFactory, self).__init__()

def InitAllProblems():
    import OpenPisco.Optim.Problems.OptimProblemTopoGeneric

def CheckIntegrity():
    return "OK"

if __name__ == '__main__':
    print(CheckIntegrity())
