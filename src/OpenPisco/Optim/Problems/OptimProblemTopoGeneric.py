# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

from Muscat.Helpers.Decorators import froze_it
import Muscat.Helpers.ParserHelper  as PH
from Muscat.Containers.Filters.FilterObjects import ElementFilter

from OpenPisco.Optim.Problems.ProblemFactory import RegisterClass as RegisterProblemClass
from OpenPisco.Optim.Problems.OptimTopoProblemBase import OptimTopoProblemBase
from OpenPisco.Optim.Criteria.CriteriaFactory import Create as CreateCriteria
from OpenPisco.Optim.Criteria.SemiInfiniteCriteria import SemiInfiniteCriteriaBase

def AddInequalityConstraintToOptimProblem(POptim,criterion):

    if isinstance(criterion,SemiInfiniteCriteriaBase):
        newCriteria = criterion.GetDiscreteImplementation()
        for newCriterion in newCriteria:
            POptim.AddInequalityConstraint(newCriterion)
    else :
        POptim.AddInequalityConstraint(criterion)

@froze_it
class OptimProblemTopoGeneric(OptimTopoProblemBase):
  def __init__(self, other=None, dataDeepCopy=True):
        super(OptimProblemTopoGeneric, self).__init__(other=other)

#####
def CreateOptimProblemTopoGeneric(ops):
    res = OptimProblemTopoGeneric()
    for typeName,data in ops["children"]:
        if "type" in data:
            criteriaType = data['type']
            del data['type']
            crit = CreateCriteria(criteriaType,data)
            if typeName == "Objective":
                res.SetObjectiveCriterion(crit)
            elif typeName == "Constraint":
                if "UpperBound" in data or "upperBound" in data:
                   AddInequalityConstraintToOptimProblem(res,crit)
                   res.SetInequalityConstraintStatus(res.GetNumberOfInequalityConstraints()-1,True)
                elif "TargetValue" in data or "targetValue" in data:
                   res.AddEqualityConstraint(crit)
                   res.SetEqualityConstraintStatus(res.GetNumberOfEqualityConstraints()-1,True)
                else:
                    raise Exception("Error: need an UpperBound or a TargetValue")
            elif typeName == "Criterion":
                res.AddInequalityConstraint(crit)
                res.SetInequalityConstraintStatus(res.GetNumberOfInequalityConstraints()-1,False)
            else:
                raise Exception("Criterion no available " + str(typeName) )
        elif typeName == "ExportExtraFields":
            assert "problem" in data,"Can not add field to export without problem"
            problem=data["problem"]
            if "fields" in data and "locations" in data:
                fieldNames,locations=PH.ReadStrings(data["fields"]),PH.ReadStrings(data["locations"])
                assert len(fieldNames)==len(locations),"Number of fields to export should match numer of locations"
                res.AddProblemFields(problem=problem,
                                        fieldNames=fieldNames,
                                        locations=locations)
            if "scalars" in data:
                res.AddProblemScalars(problem=problem,scalarNames=PH.ReadStrings(data["scalars"]))
        else:
            raise Exception("typeName" + str(typeName) + "not handled" )


    del ops["children"]

    res.writer = ops.get("writer",None)
    if res.writer is not None:
        del ops["writer"]

    res.point = ops.get("point",None)
    if res.point is not None:
        del ops["point"]


    if "e2" not in ops:
        if res.point.support.props.get("IsConstantRectilinear",False):
            ops["e2"] = 3 * min(res.point.support.props.get("spacing")) **2
        else:
            from Muscat.Containers.MeshInspectionTools import ComputeMeshMinMaxLengthScale
            ops["e2"]  = 3 * ComputeMeshMinMaxLengthScale(res.point.support)[0] **2
        print("Setting lengthscale regularization parameter automatically : " + str( ops["e2"]))

    for key in list(ops.keys()):
        if "maxChange" in key:
            print(key)
            res.maxChange[key[9:]] = PH.ReadFloat(ops[key])
            del ops[key]

    if "OnTags" in ops:
        res.OnFilter = ElementFilter()
        res.OnFilter.eTags = ops.get("OnTags","").split()
        del ops["OnTags"]

    if "OffTags" in ops:
        res.OffFilter = ElementFilter()
        res.OffFilter.eTags = ops.get("OffTags","").split()
        del ops["OffTags"]

    PH.ReadProperties(ops,ops,res)

    return res
#####
RegisterProblemClass("TopoGeneric", OptimProblemTopoGeneric,CreateOptimProblemTopoGeneric)

def CheckIntegrity():
    from OpenPisco.Optim.Criteria.PhyCriteria import TopoCriteriaCompliance
    from OpenPisco.Optim.Criteria.GeoCriteria import TopoCriteriaVolume
    a = OptimProblemTopoGeneric()
    complianceCrit = TopoCriteriaCompliance()
    a.SetObjectiveCriterion(complianceCrit)
    volumeCrit = TopoCriteriaVolume()
    a.AddInequalityConstraint(volumeCrit)
    return 'ok'

if __name__ == '__main__':# pragma: no cover
    print(CheckIntegrity())
