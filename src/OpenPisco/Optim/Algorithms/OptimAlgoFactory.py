# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#


from Muscat.Helpers.Factory import Factory

def RegisterClass(name, classtype, constructor=None, withError = True):
    return OptimAlgoFactory.RegisterClass(name,classtype, constructor=constructor, withError = withError )

def Create(name,ops=None):
   return OptimAlgoFactory.Create(name,ops)


class OptimAlgoFactory(Factory):
    _Catalog = {}
    _SetCatalog = set()

    def __init__(self):
        super(OptimAlgoFactory,self).__init__()

def InitAllAlgos():
    import OpenPisco.Optim.Algorithms.OptimAlgoUnconstrained
    import OpenPisco.Optim.Algorithms.OptimAlgoNullSpace


def CheckIntegrity():
    return "OK"

if __name__ == '__main__':
        print(CheckIntegrity())
