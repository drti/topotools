# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
import abc

import numpy as np

from Muscat.Helpers.TextFormatHelper import TFormat
from Muscat.Helpers.Logger import Info, Debug,Error

from OpenPisco.Actions.ActionBase import ActionBase
from OpenPisco import RETURN_SUCCESS, RETURN_FAIL

class OptimAlgoBase(metaclass=abc.ABCMeta):
    def __init__(self):
        super(OptimAlgoBase,self).__init__()
        self.run = -1
        self.output_number = 0
        self.step = 0
        self.iter = 0
        self.iterOk = True
        self.stepSizeMax = 1.
        self.stepSize = self.stepSizeMax
        self.last_good_stepSize = self.stepSizeMax
        self.direction = None
        self.optimProblem = None
        self.outputEveryIter = False
        self.beforeWritingCallBack = None
        self.outputStatus = 'Unknown' # "Converged", "MaxIterReached", "Diverged", "Broken"

    def PreStart(self):
        self.run  += 1
        if not self.optimProblem.PreStartCheck():
            Error("Error in PreStartCheck")
            return RETURN_FAIL
        return RETURN_SUCCESS

    def RunOptimization(self): 
        Debug("steps")
        self.PrintStateHeader()
        self.step = 0
        while self.step < self.numberOfDesignStepsMax:
            status = self.DoOneStep()
            if status != RETURN_SUCCESS :
                status=self.StatusFromResult(status)

            if status==RETURN_SUCCESS:
                self.step+=1
            else:
                return status

    def UpdateOptimProblem(self,optimProblem=None):
        if optimProblem is None:
            optimProblem = self.optimProblem

        res=optimProblem.UpdateValues()
        assert res == RETURN_SUCCESS, "Physical problem evaluation failed"

    def CopyCurrentOptimProblem(self):
        return type(self.optimProblem)(self.optimProblem)

    def ComputeConstraintsSlacks(self,optimProblem=None):
        if optimProblem is None:
            optimProblem = self.optimProblem

        return optimProblem.GetInequalityConstraintSlacks()

    def ComputeSensitivityInterfaceProjection(self,DJ,Dg=None,Dh=None):
        if hasattr(self.optimProblem.GetCurrentPoint(), "ComputeInterfaceProjection"):
            DJ = self.optimProblem.GetCurrentPoint().ComputeInterfaceProjection(DJ)
            for i in range(len(Dg)):
                Dg[i,:] = self.optimProblem.GetCurrentPoint().ComputeInterfaceProjection(Dg[i,:])
            for i in range(len(Dh)):
                Dh[i,:] = self.optimProblem.GetCurrentPoint().ComputeInterfaceProjection(Dh[i,:])
        return DJ,Dg,Dh

    def GetValuesFromOptimProblem(self,optimProblem=None):
        if optimProblem is None:
           optimProblem = self.optimProblem

        J = optimProblem.GetObjectiveFunctionVal()
        g = np.array([x-b for x,b in zip(optimProblem.GetActiveEqualityConstraintsVals(),optimProblem.GetActiveEqualityConstraintsTargetValues())])
        h = np.array([x-b for x,b in zip(optimProblem.GetActiveInequalityConstraintsVals(),optimProblem.GetActiveInequalityConstraintsUpperBound())])

        return (J, g, h)

    def GetSensitivitiesFromOptimProblem(self,optimProblem=None):
        if optimProblem is None:
           optimProblem = self.optimProblem
        DJ = optimProblem.GetObjectiveFunctionSensitivity()
        Dg = np.array(optimProblem.GetActiveEqualityConstraintsSensitivityVals())
        Dh = np.array(optimProblem.GetActiveInequalityConstraintsSensitivityVals())

        if not len(Dg):
           Dg = np.zeros((0, len(DJ)))
        if not len(Dh):
           Dh = np.zeros((0, len(DJ)))

        return (DJ,Dg, Dh)

    def GetDirectionsFromGradients(self,DJ,Dg,Dh,optimProblem=None):
        DgExt = np.zeros_like(Dg)
        DhExt = np.zeros_like(Dh)

        DJExt =  -self.ComputeDirectionFromGradient(gradient=DJ,optimProblem=optimProblem)
        for i in range(len(Dg)):
            DgExt[i,:] =  -self.ComputeDirectionFromGradient(gradient=Dg[i,:],optimProblem=optimProblem)
        for i in range(len(Dh)):
            DhExt[i,:] =  -self.ComputeDirectionFromGradient(gradient=Dh[i,:],optimProblem=optimProblem)

        return (DJExt,DgExt,DhExt)

    def ComputeDirectionFromGradient(self,gradient,optimProblem=None):
        if optimProblem is None:
           optimProblem=self.optimProblem

        return optimProblem.GetDirectionFromGradient(gradient)

    @abc.abstractmethod
    def StatusFromResult(self,resultVal):
        pass

    def NeedToInterrupt(self):
        return ActionBase.NeedToInterrupt()

    @abc.abstractmethod
    def DoOneStep(self):
        pass

    def Advance(self,optimProblem):
        return optimProblem.Advance(self.direction, self.stepSize)

    def ComputeGain(self, newObjective):
        if np.sign(newObjective) == 1:
            if np.sign(self.oldObjective) == 1:
                self.gain = (newObjective / self.oldObjective - 1) * 100
            else:
                self.gain = -(newObjective / self.oldObjective - 1) * 100
        else:
            if np.sign(self.oldObjective) == 1:
                self.gain = -(newObjective / self.oldObjective - 1) * 100
            else:
                self.gain = (newObjective / self.oldObjective - 1) * 100

    def DecreaseStepSize(self):
        self.stepSize *= self.stepDown
        Debug("stepSize divided by "+str(1./self.stepDown)+" :" + str(self.stepSize))

    def IncreaseStepSize(self):
        self.stepSize *= self.stepUp
        Debug("stepSize multiplied by "+str(self.stepUp)+" :" + str(self.stepSize))

        if self.stepSize > self.stepSizeMax:
            self.stepSize = self.stepSizeMax
            Debug("Saturated stepSize :" + str(self.stepSize))

    def AcceptOptimProblemIteration(self,optiProblem):
        optiProblem.UpdateProblemWhenIterationAccepted()
        self.optimProblem.TakeValuesFrom(optiProblem)

    def ResetState(self):
        self.stepSize = self.stepSizeMax
        self.step = 0
        self.iter = 0
        self.iterOk = True
        self.outputStatus = "Unknown"

    def PrintStateHeader(self):
        self.optimProblem.PrintHeader()
        print(TFormat.Center('run : %i' % (self.run),width= 80 ) )
        res = '|cpt| step | iter '
        res += self.optimProblem.PrintStateHeader()
        res += '|'+ TFormat.Center('Objective',fill=" ",width=16)  + '|  Gain % |StepSize|'
        print(res)

    def PrintState(self):

        output_number_str =  TFormat.Center(str(self.output_number), fill=' ',width=3)
        res = ('|%s| %s|      ') % (output_number_str, format(self.step, " 5") )
        res += self.optimProblem.PrintState()
        res += '|' + TFormat.Center( '%.8e'  % self.oldObjective,fill=" ",width=16)
        res += '|         |%s|' %  TFormat.Center("{0:1.4f}".format(self.stepSize),fill=" ",width=8 )

        print(res)

    def PrintCurrentState(self, NOF,newObjective):
        output_number_str =  TFormat.Center(str(self.output_number), fill=' ',width=3)
        res = "|%s|      |%s |" % (output_number_str, format(self.iter, "5"))

        res += self.optimProblem.PrintCurrentState(NOF)

        gain = self.gain

        if gain < 0:
            gain = TFormat.InGreen(TFormat.Center("{:+6.3f}%".format(gain),fill=" ",width=9))
        else:
            if gain >= 100:
                gain = TFormat.InRed(TFormat.Center("{:+6.0f}%".format(gain),fill=" ",width=9))
            else:
                gain = TFormat.InRed(TFormat.Center("{:+6.3f}%".format(gain),fill=" ",width=9))

        if newObjective < self.oldObjective:
            comp_vol_obj = TFormat.InGreen(TFormat.Center( "%.8e"% newObjective,fill=" ",width=16) )+"|"
        else:
            comp_vol_obj = TFormat.InRed(TFormat.Center("%.8e"% newObjective,fill=" ",width=16) )+"|"

        res +=  ('%s%s|%s|' % (comp_vol_obj, gain, TFormat.Center("{:1.4f}".format(self.stepSize),fill=" ",width=8) ) )
        print(res)

    def SaveData(self, OF = None):

        if self.beforeWritingCallBack is not None:
            self.beforeWritingCallBack()

        data = {}
        data["run"]  = self.run
        data["step"] = self.step
        data["iter"] = self.iter
        data["cpt"]  = self.output_number
        data["OK" ]  = 1 if self.iterOk else 0

        try:
            gradient = self.gradient
        except AttributeError:
            gradient = None

        try:
            direction = self.direction
        except AttributeError:
            direction = None

        if OF is not None:
            OF.SaveData(data, gradient = gradient, direction=direction )
        else:
            self.optimProblem.SaveData(data, gradient = gradient, direction=direction)

        self.output_number = self.output_number + 1
        Info("Saving Data Done")

def CheckIntegrity():
    return "OK"
if __name__ == '__main__':

    print(CheckIntegrity()) # pragma: no cover
