# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
import re

import sympy as sy
import numpy as np
import math

import Muscat.Helpers.ParserHelper as PH
from Muscat.Types import MuscatFloat
from Muscat.FE.FETools import PrepareFEComputation
from Muscat.FE.Fields.FEField import FEField
from Muscat.Containers.MeshFieldOperations import GetFieldTransferOp

from OpenPisco.Optim.Criteria.CriteriaFactory import RegisterClass as RegisterCriteriaClass
from OpenPisco.Optim.Criteria.Criteria  import CriteriaBase
from OpenPisco import RETURN_SUCCESS

class ComposedCriteria(CriteriaBase):
    def __init__(self, other=None):
        super().__init__(other)

        self.internalCriteria = []
        self.numberOfAllowedSubCriteria = 0
        self.useSubObjectBounds = True

        if other is not None:
            self.numberOfAllowedSubCriteria = other.numberOfAllowedSubCriteria
            self.internalCriteria = [ type(c)(c) for c in other.internalCriteria]
            self.useSubObjectBounds = other.useSubObjectBounds

    def GetInternalCriteria(self,i):
        return self.internalCriteria[i]

    def GetNumberOfInternalCriteria(self):
        return len(self.internalCriteria)

    def SetUpperBound(self,val):
        self.useSubObjectBounds = False
        super().SetUpperBound(val)

    def GetUpperBound(self):
        if self.useSubObjectBounds:
            return self.internalCriteria[0].GetUpperBound()
        else:
            return super().GetUpperBound()

    def UpdateValues(self,point):
        res = RETURN_SUCCESS
        for c in self.internalCriteria:
            res = c.UpdateValues(point)
            if res != RETURN_SUCCESS:
                return res

        return res

    def GetNumberOfSolutions(self):
         return np.sum([c.GetNumberOfSolutions() for c in  self.internalCriteria])

    def GetSolution(self,i):
        for c in self.internalCriteria:
            nSols = c.GetNumberOfSolutions()
            if i < nSols:
                return c.GetSolution(i)
            i -= nSols

        raise Exception("solution not available ")

    def GetSolutionName(self,i):
        for cpt,c in enumerate(self.internalCriteria):
            nSols = c.GetNumberOfSolutions()
            if i < nSols:
                return f"C{cpt}_"+c.GetSolutionName(i)
            i -= nSols
        assert i<= 0,"solution not available "

    def GetScalarsOutputs(self):
        res = dict()
        for c in self.internalCriteria:
            res.update(c.GetScalarsOutputs())
        return res


def OptToCriteria(ops, keysToRead = [],  numberOfExpectedCriterion=1):
    """_summary_

    Parameters
    ----------
    ops : _type_
        _description_
    keysToRead : list, optional
        _description_, by default []
    numberOfExpectedCriterion : int, optional
        -1 means at least one, by default 1

    Returns
    -------
    _type_
        _description_

    Raises
    ------
    Exception
        _description_
    Exception
        _description_
    """

    from OpenPisco.Optim.Criteria.CriteriaFactory import Create
    res = []
    for child in ops["children"] :
        assert child[0] == "Criteria",f"Must be a Criteria object not : {child[0]}"
        data = child[1]
        objtype = data["type"]
        del data["type"]
        keysAndVals = {}
        for key in keysToRead:
            keysAndVals[key] = PH.ReadFloat(data[key])
            del data[key]
        criteria = Create(objtype,data)
        res.append( (keysAndVals,criteria) )
    del ops["children"]

    if numberOfExpectedCriterion < 0:
        assert len(res) >= abs(numberOfExpectedCriterion),f"This criterion needs at least {abs(numberOfExpectedCriterion)} sub criteria"
    else:
        assert len(res) == numberOfExpectedCriterion,f"This criterion needs exactly {numberOfExpectedCriterion} sub criteria"

    return res

def CreateCriteriaOffset(ops):
    res = CriteriaOffset()
    subres = OptToCriteria(ops,[],1 )
    res.internalCriteria.append(subres[0][1])
    PH.ReadProperties(ops, ops, res, typeConversion=True)
    return res

class CriteriaOffset(ComposedCriteria):
    """Composed Criteria to apply an offset and/or the absolute values
    This can be used to simulate a target values with tolerance
       g(x) = |f(x)-offset|
       g'(x) =  g(x) * f'(x)
               -----
               |g(x)|


    this is implemented with a sign function : sign(g(x)) = g(x)/|g(x)|


    Parameters
    ----------
    offset : float
        offset to apply, by default 0.
    absValue : bool
        if the absolute values is applied to the function, by default False
    """
    def __init__(self, other=None):
        super().__init__(other)
        self.numberOfAllowedSubCriteria = 1

        if other is None:
            self.SetName("Offset")
            self.offset = float(1.)
            self.absValue = False
        else:
            self.offset = other.offset
            self.absValue = other.absValue

    def GetValue(self):
        res = self.GetInternalCriteria(0).GetValue() - self.offset
        if self.absValue :
            return abs(res)
        else:
            return res

    def GetSensitivity(self):
        if self.absValue :
            s = np.sign(self.GetInternalCriteria(0).GetValue() - self.offset)
            return self.GetInternalCriteria(0).GetSensitivity()*s
        else:
            return self.GetInternalCriteria(0).GetSensitivity()

RegisterCriteriaClass("CriteriaOffset", CriteriaOffset,CreateCriteriaOffset)

###############################################################################################

def CreateCriterionCyclicSymmetry(ops):
    res = CriterionCyclicSymmetry()
    subres = OptToCriteria(ops,[],1 )
    res.internalCriteria.append(subres[0][1])
    PH.ReadProperties(ops, ops, res, typeConversion=True)
    return res

class CriterionCyclicSymmetry(ComposedCriteria):
    """Composed Criterion to impose a cyclic symmetry to the given sensitivity 
    This means that the sensitivity field looks the same after being rotated by an angle of ( \frac{2\pi}{nFold} ) radians.
    It can be useful when the optimal geometry is expected to be symmetric but the boundary conditions are not
    The implementation uses the field transfer feature 

    Parameters
    ----------
    nFold : int
        fraction angle
    axis : str
        perform a planar rotation about a fixed axis
    """
    def __init__(self, other=None):
        super().__init__(other)
        self.numberOfAllowedSubCriteria = 1

        if other is None:
            self.SetName("CyclicSymmetry")
            self.nFold = int(1)
            self.axis= "x"
        else:
            self.nFold = other.nFold
            self.axis= other.axis

    def GetValue(self):
        return self.GetInternalCriteria(0).GetValue() 
    
    def GetTransferedNodalField(self,inField,inMesh,targetNodes):
        space, numberings, _, _ = PrepareFEComputation(inMesh,numberOfComponents=1)
        tempfield = FEField("",mesh=inMesh,space=space,numbering=numberings[0])
        op, _ , _ = GetFieldTransferOp(tempfield,targetNodes,method="Interp/Clamp" )
        return op.dot(inField)
    
    def GetRotationMatrixAroundAxis(self,angle):
        axis = self.axis
        assert axis in ["x","y","z"],"Axe %s not covered (x,y,z allowed)"
        if axis=="x":
            return  np.array((
                        [1,0,0],
                        [0.0,math.cos(angle),-math.sin(angle)],
                        [0,math.sin(angle),math.cos(angle)],               
                           ))
        elif axis=="y":
            return np.array((
                        [math.cos(angle),0,math.sin(angle)],
                        [0.0,1.0,0.0],
                        [-math.sin(angle),0,math.cos(angle)],               
                           ))
        elif axis=="z":
            return np.array((
                        [math.cos(angle),-math.sin(angle),0],
                        [math.sin(angle),math.cos(angle),0],
                        [0,0,1],               
                           ))

    def GetSensitivity(self):
        criterion = self.GetInternalCriteria(0)
        res =  criterion.GetSensitivity()
        transferedField = np.zeros( ( criterion.point.support.GetNumberOfNodes(),self.nFold), dtype = MuscatFloat)
        transferedField[:,0] = res
        for i in range(1,self.nFold):
            angle = i*2*np.pi/self.nFold
            rotMat = self.GetRotationMatrixAroundAxis(angle)
            mesh = criterion.point.support.View()
            nodes = mesh.nodes.transpose(1,0).copy()
            mesh.nodes = rotMat.dot(nodes).transpose(1,0).copy()
             
            transferedField[:,i] = self.GetTransferedNodalField(res,mesh,criterion.point.support.nodes)
            
        meanField =  np.mean(transferedField, axis=1)
        return meanField 
         
RegisterCriteriaClass("CriterionCyclicSymmetry", CriterionCyclicSymmetry,CreateCriterionCyclicSymmetry)

###############################################################################################

def CreateCriteriaPower(ops):
    res = CriteriaPower()
    subres = OptToCriteria(ops,[],1 )
    res.internalCriteria.append(subres[0][1])
    PH.ReadProperties(ops, ops, res, typeConversion=True)
    return res

class CriteriaPower(ComposedCriteria):
     def __init__(self, other=None):
        super().__init__(other)

        if other is None:
            self.power = 1.
            self.SetName("Power")
        else:
            self.power = other.power
        self.__name__ = "Power"
     def SetPower(self,value):
        self.power = PH.ReadFloat(value)

     def GetValue(self):
        return abs(self.GetInternalCriteria(0).GetValue())**self.power

     def GetSensitivity(self):

        return self.power*abs(self.GetInternalCriteria(0).GetValue())**(self.power-1)*np.sign(self.GetInternalCriteria(0).GetValue())*self.GetInternalCriteria(0).GetSensitivity()

RegisterCriteriaClass("Power", CriteriaPower,CreateCriteriaPower)

###############################################################################################

def CreateCriteriaFactor(ops):
    res = CriteriaFactor()
    subres = OptToCriteria(ops,[],1 )
    res.internalCriteria.append(subres[0][1])
    PH.ReadProperties(ops, ops, res, typeConversion=True)
    return res

class CriteriaFactor(ComposedCriteria):
     def __init__(self, other=None):
        super().__init__(other)

        self.numberOfAllowedSubCriteria = 1

        if other is None:
            self.factor = float(1.)
        else:
            self.factor = other.factor

     def GetValue(self):
         return self.factor*self.GetInternalCriteria(0).GetValue()

     def GetSensitivity(self):
         return self.factor*self.GetInternalCriteria(0).GetSensitivity()

RegisterCriteriaClass("CriteriaFactor", CriteriaFactor,CreateCriteriaFactor)
###############################################################################################
def CreateCriteriaWeighted(ops):
    res = CriteriaWeighted()
    subres = OptToCriteria(ops,["weight"], -1 )
    res.internalCriteria.append(subres[0][1])
    for c_ops, subCrit in subres :
        res.internalCriteria.append(subCrit)
        res.factor[subCrit.GetName()] = PH.ReadFloat(c_ops["weight"])

    PH.ReadProperties(ops, ops, res, typeConversion=True)
    return res

class CriteriaWeighted(ComposedCriteria):
     def __init__(self, other=None):
        super().__init__(other)

        if other is None:
            self.factor = {}
            self.__name__ = "Weighted"
            self.firstTime = True
            self.automaticPenalisation = True
            self.useGradientForAutomaticPenalisation = True
        else:
            self.factor = other.factor.copy()
            self.firstTime = other.firstTime
            self.automaticPenalisation = other.automaticPenalisation
            self.useGradientForAutomaticPenalisation = other.useGradientForAutomaticPenalisation

     def SetUpperBound(self,val):
         raise NotImplementedError()

     def GetUpperBound(self):
         raise NotImplementedError()

     def GetValue(self):
         if self.firstTime and self.automaticPenalisation:
            self.firstTime = False
            for c in self.internalCriteria:
                oldweight = self.factor[c.GetName()]
                if self.useGradientForAutomaticPenalisation:
                    weight = np.linalg.norm(c.GetSensitivity())
                else:
                    weight = np.linalg.norm(c.GetValue())
                errorMessage = f"Automatic weight for criterion '{c.GetName()}' is 0: automatic penalization failed  "
                assert not np.isclose(weight,0.),errorMessage
                self.factor[c.GetName()] = oldweight/weight

         res = 0.
         for i in range(self.GetNumberOfInternalCriteria()):
             c = self.GetInternalCriteria(i)
             res += self.factor[c.GetName()]*c.GetValue()
         return res

     def GetSensitivity(self):
         c0 = self.GetInternalCriteria(0)
         res = c0.GetSensitivity()*self.factor[c0.GetName()]
         for i in range(1,self.GetNumberOfInternalCriteria()):
            c = self.GetInternalCriteria(i)
            w = self.factor[c.GetName()]
            s = c.GetSensitivity()
            res += w*s
         return res

RegisterCriteriaClass("Weighted", CriteriaWeighted,CreateCriteriaWeighted)

###############################################################################################

def CreateCriteriaRatio(ops):
    res = CriteriaWeighted()
    subres = OptToCriteria(ops,[], 2 )
    res.internalCriteria.append(subres[0][1])
    for _, subCrit in subres :
        res.internalCriteria.append(subCrit)
    PH.ReadProperties(ops, ops, res, typeConversion=True)
    return res

class CriteriaRatio(ComposedCriteria):
     """
     Compute the derivative of the ratio of two given criteria
     """
     def __init__(self, other=None):
        super().__init__(other)

        self.numberOfAllowedSubCriteria = 2
        if other is None:
            self.SetName("Ratio")

     def SetInternalCriteria(self, numcriteria, dencriteria):
        self.internalCriteria = [numcriteria, dencriteria]

     def GetNumeratorCriteria(self):
         return self.internalCriteria[0]

     def GetDenominatorCriteria(self):
         return self.internalCriteria[1]

     def GetValue(self):
        weight = self.GetDenominatorCriteria().GetValue()
        assert not np.isclose(weight,0.),f"Criterion '{self.GetDenominatorCriteria().GetName()}' is 0: Impossible to compute the ratio "
        return self.GetNumeratorCriteria().GetValue()/weight

     def GetSensitivity(self):
        numValue = self.GetNumeratorCriteria().GetValue()
        numSens = self.GetNumeratorCriteria().GetSensitivity()
        denValue = self.GetDenominatorCriteria().GetValue()
        denSens = self.GetDenominatorCriteria().GetSensitivity()
        return  ( numSens*denValue - numValue*denSens)/denValue**2

RegisterCriteriaClass("CriteriaRatio",CriteriaRatio, CreateCriteriaRatio)

###############################################################################################

def CreateCriteriaPolynomialVanishing(ops):
    res = CriteriaPolynomialVanishing()
    subres = OptToCriteria(ops,[], 1 )
    res.internalCriteria.append(subres[0][1])
    for c_ops, subCrit in subres :
        res.internalCriteria.append(subCrit)
    PH.ReadProperties(ops, ops, res, typeConversion=True)
    return res

class CriteriaPolynomialVanishing(ComposedCriteria):
    """
    Allow to generate a polynomial constraint from a given one.

    if J is the internal criterion and B is its upperbound,
    then the polynomial function is B*(J/B-1)*( (J/B-1)**2+1 )+ B
    https://royalsocietypublishing.org/doi/10.1098/rspa.2019.0861
    """
    def __init__(self, other=None):
        super().__init__(other)

        self.numberOfAllowedSubCriteria = 1

        if other is None:
            self.SetName("PolynomialVanishing")

        self.useSubObjectBounds = True
        self.slack = 0.

    def GetSlack(self):
        return self.GetInternalCriteria(0).GetValue()/self.GetUpperBound() - 1.

    def GetValue(self):
        slack = self.GetSlack()
        return self.GetUpperBound()*slack*( slack**2 + 1) + self.GetUpperBound()

    def GetSensitivity(self):
        slack = self.GetSlack()
        return ( 3*slack**2 +1 )*self.internalCriteria.GetSensitivity()

RegisterCriteriaClass("PolynomialVanishingCriteria",CriteriaPolynomialVanishing,CreateCriteriaPolynomialVanishing)
RegisterCriteriaClass("CriteriaPolynomialVanishing",CriteriaPolynomialVanishing,CreateCriteriaPolynomialVanishing)

class SymbolicComposedCriteria(CriteriaBase):
     def __init__(self, other=None):
        super(SymbolicComposedCriteria, self).__init__(other)

        if other is None:
            self.SetName("SymbolicComposed")
            self.compoundSymbExpression = dict()
            self.symbolicCritExpr=None
            self.symbolicSensitivExpr=None
            self.symbolicSensitivFunctor=None
        else:
            self.compoundSymbExpression = other.symbExpression
            self.symbolicCritExpr=other.symbolicCritExpr
            self.symbolicSensitivExpr=other.symbolicSensitivExpr
            self.symbolicSensitivFunctor=other.symbolicSensitivFunctor

     def SetComposedSymbExpression(self,criteriaExpression,criteriaByName):
        self.compoundSymbExpression={"expr":criteriaExpression,"criteria":criteriaByName}
        self.Eval()

     def Eval(self):
        expression=self.compoundSymbExpression.get("expr")
        criteria=self.compoundSymbExpression.get("criteria")
        splitted_expr=re.split('(\W)', expression)
        new_expr=[None]*len(splitted_expr)

        for termId,term in enumerate(splitted_expr):
            if term in criteria.keys():
                new_expr[termId]=term+"(omega)"
            else:
                new_expr[termId]=term
        self.symbolicCritExpr=sy.sympify(''.join(new_expr))

        symbolicSensitivExpr=sy.Derivative(self.symbolicCritExpr).doit()
        for symbolName in criteria.keys():
            symbolicSensitivExpr=symbolicSensitivExpr.subs(sy.sympify(symbolName+"(omega)"),sy.sympify(symbolName))
            symbolicSensitivExpr=symbolicSensitivExpr.subs(sy.Derivative(sy.sympify(symbolName), sy.sympify("omega")), sy.sympify("Derivative"+symbolName))
        self.symbolicSensitivExpr=symbolicSensitivExpr

        criteriaArguments=[(sy.sympify(symbolName),sy.sympify("Derivative"+symbolName)) for symbolName in criteria.keys()]
        criteriaArguments=[item for sublist in criteriaArguments for item in sublist]
        self.symbolicSensitivFunctor = sy.lambdify(criteriaArguments, symbolicSensitivExpr, 'numpy')

     def UpdateValues(self,point):
         self.point = point
         return RETURN_SUCCESS

     def GetNumberOfSolutions(self):
         return 0

     def GetValue(self):
        criteriaBySymbolicName=self.compoundSymbExpression.get("criteria")
        criteriaValues={sy.sympify(symbolName+"(omega)"):criteria.GetValue() for symbolName,criteria in criteriaBySymbolicName.items()}
        compoundValue=self.symbolicCritExpr.evalf(subs=criteriaValues)
        return compoundValue

     def GetSensitivity(self):
        criteriaBySymbolicName=self.compoundSymbExpression.get("criteria")
        criteriaValuesByName={symbolName:criteria.GetValue() for symbolName,criteria in criteriaBySymbolicName.items()}
        criteriaSensitivitiesByName={"Derivative"+symbolName:criteria.GetSensitivity()\
                               for symbolName,criteria in criteriaBySymbolicName.items()
                               }
        criteriaEvalByName={**criteriaValuesByName,**criteriaSensitivitiesByName}
        compoundSensitivities=self.symbolicSensitivFunctor(**criteriaEvalByName)
        return compoundSensitivities

def CheckIntegrity_SymbolicComposedCriteria(GUI=False):
    class DummyCriteria:
        def __init__(self,multFactor):
            __name__="DummyCriteria"
            self.multFactor=multFactor

        def GetValue(self):
            return self.multFactor

        def GetSensitivity(self):
            return self.multFactor*np.ones(10)

    dummy1=DummyCriteria(multFactor=1)
    dummy2=DummyCriteria(multFactor=2)
    dummy3=DummyCriteria(multFactor=3)
    dummy4=DummyCriteria(multFactor=4)

    b=SymbolicComposedCriteria()
    criteriaExpression='2*crit1*crit2-crit3/crit4'
    criteriaByName={"crit1":dummy1,"crit2":dummy2,"crit3":dummy3,"crit4":dummy4}
    b.SetComposedSymbExpression(criteriaExpression=criteriaExpression,criteriaByName=criteriaByName)
    value=b.GetValue()
    expectedValue=2*dummy1.GetValue()*dummy2.GetValue()-dummy3.GetValue()/dummy4.GetValue()
    np.testing.assert_almost_equal(value,expectedValue)

    sensitivity=b.GetSensitivity()
    expectedSensitivity=2*dummy1.GetSensitivity()*dummy2.GetValue()\
                  +2*dummy1.GetValue()*dummy2.GetSensitivity()\
                  - (dummy3.GetSensitivity()*dummy4.GetValue() -dummy3.GetValue()*dummy4.GetSensitivity() )/dummy4.GetValue()**2
    np.testing.assert_almost_equal(sensitivity,expectedSensitivity)
    return "OK"

def CheckIntegrity(GUI=False):
    CriteriaPolynomialVanishing()
    CriteriaRatio()
    CriteriaWeighted()
    CriteriaFactor()
    CriteriaPower()
    CriteriaOffset()
    ComposedCriteria()
    assert CheckIntegrity_SymbolicComposedCriteria()== "OK"
    return "OK"

if __name__ == "__main__":# pragma: no cover
    print(CheckIntegrity())
