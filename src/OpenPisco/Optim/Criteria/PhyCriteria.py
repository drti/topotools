# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
import numpy as np

from Muscat.Types import MuscatFloat
import Muscat.Helpers.ParserHelper as PH
from Muscat.Helpers.Logger import Info
from Muscat.FE.SymWeakForm import GetField, GetTestField, Strain, ToVoigtEpsilon
from Muscat.FE.MaterialHelp import HookeIso
from Muscat.FE.Fields.FieldTools import VectorToFEFieldsData
from Muscat.FE.Fields.FEField  import FEField
from Muscat.FE.Integration import IntegrateGeneral
from Muscat.FE.DofNumbering import ComputeDofNumbering
from Muscat.FE.Spaces.FESpaces import ConstantSpaceGlobal
from Muscat.Containers.Filters.FilterObjects import ElementFilter

from OpenPisco.Optim.Criteria.CriteriaFactory import RegisterClass as RegisterCriteriaClass
from OpenPisco.Optim.Criteria.Criteria import PhysicalCriteriaBase
import OpenPisco.TopoZones as TZ
import OpenPisco.PhysicalSolvers.FieldsNames as FN
from OpenPisco import RETURN_SUCCESS

class TopoCriteriaCompliance(PhysicalCriteriaBase):
     def __init__(self, other=None):
        super(TopoCriteriaCompliance, self).__init__(other)
        self.globalCompliance = 0.0
        self.compliance = None
        self.update = True

        if other is None:
            self.SetName("Compliance")
        else:
            self.globalCompliance = other.globalCompliance
            self.compliance = np.copy(other.compliance)
            self.point = other.point

     def GetNumberOfSolutions(self):
         return 1

     def GetCriteriaSolution(self,i):
         return self.problem.GetNodalSolution()

     def GetCriteriaSolutionName(self,i):
         return "Dep"

     def SetAuxiliaryQuantities(self,levelSet):
         self.problem.SetAuxiliaryFieldGeneration(FN.potential_energy,on=FN.Nodes)
         self.problem.SetAuxiliaryScalarGeneration(FN.int_potential_energy)

     def UpdateValues(self,point):
         """
         .. py:classmethod:: UpdateValues(self, point)

         Update values of criterion and sensitivity for the current levelset

         :param LevelSet point : current level set
         :return: paramater indicating if the update was successfull
         :rtype: bool
         """
         self.point = point
         return RETURN_SUCCESS

     def GetValue(self):
         res = self.problem.GetAuxiliaryScalar(FN.int_potential_energy)*2
         return res

     def GetSensitivity(self):
         res = self.problem.GetAuxiliaryField(FN.potential_energy,on=FN.Nodes)*2
         return -res.flatten()

RegisterCriteriaClass("Compliance", TopoCriteriaCompliance)

class TopoCriteriaDisplacementDeviation(PhysicalCriteriaBase):
    def __init__(self, other=None):
        super().__init__(other)

        if other is None:
            self.SetName("DisplacementDeviation")

            ### parameter of the compliant mechanism
            self.nTag1 = None
            self.nTag2 = None
            self.__dir = np.array([1,0,0],dtype=MuscatFloat)
            self.f_val = -1.
            self.absValue = False
        else:
            self.nTag1 = other.nTag1
            self.nTag2 = other.nTag2
            self.__dir = np.copy(other.__dir)
            self.f_val = other.f_val
            self.absValue = other.absValue

    def SetDeviationDirection(self, data):
        val = PH.ReadFloats(data)
        u0 = np.linalg.norm(val)
        self.__dir = val/u0

    def UpdateValues(self,levelSet):

        cleanMesh = self.problem.internalSolveur.unknownFields[0].mesh
        cleanMeshSpace = self.problem.internalSolveur.unknownFields[0].space
        cleanMeshNumbering = self.problem.internalSolveur.unknownFields[0].numbering

        self.u_num = self.problem.GetNodalSolution().copy()

        # save
        uSave = self.problem.internalSolveur.unknownFields
        saveOldSol = self.problem.internalSolveur.sol
        # put new fields in the internal solver
        # we do this to reuse the same solver for the adjoint problem
        self.problem.internalSolveur.unknownFields = [f.copy() for f in self.problem.internalSolveur.unknownFields]

        u = np.array(uSave)

        id1 = cleanMesh.nodesTags[self.nTag1].GetIds()
        assert len(id1) == 1, "Case not handled"

        dof1 = cleanMeshNumbering.GetDofOfPoint(id1)
        u1 = np.array([ self.problem.internalSolveur.unknownFields[x].data[dof1] for x in range(3)])

        if self.nTag2 is not None:
            id2 = cleanMesh.nodesTags[self.nTag2].GetIds()
            assert len(id2) == 1, "the ntag must contain only one node"
            dof2 = cleanMeshNumbering.GetDofOfPoint(id2)
            u2 = np.array([ self.problem.internalSolveur.unknownFields[x].data[dof2] for x in range(3)])
        else:
            u2 = np.array([0,0,0], dtype=MuscatFloat)

        if self.absValue :
            sign = np.sign((u1-u2).dot(self.__dir))
        else:
            sign = 1

        self.f_val = sign*((u1-u2).dot(self.__dir))
        nbdofs = cleanMeshNumbering.size
        f = np.zeros(nbdofs * 3, dtype=MuscatFloat)
        f[[0*nbdofs+dof1,1*nbdofs+dof1,2*nbdofs+dof1]] = self.__dir*sign

        if self.nTag2 is not None:
            f[[0*nbdofs+dof2,1*nbdofs+dof2,2*nbdofs+dof2]] = -self.__dir*sign

        self.problem.internalSolveur.Resolve(f)
        self.problem.internalSolveur.PushSolutionVectorToUnkownFields()

        self.p_num = self.problem.GetNodalSolution()

        fields = []

        for i,f in  enumerate(self.problem.internalSolveur.unknownFields):
            f_field = f.copy()
            f_field.name = "p_"+str(i)
            fields.append(f_field)

        VectorToFEFieldsData(self.problem.internalSolveur.sol,fields)

        self.problem.internalSolveur.sol = saveOldSol
        fields.extend(u)

        ### symbolic part ###
        p = GetField("p",3)
        u = GetField("u",3)

        ### P1 interpolation
        p1space = GetField("P1",1)
        p1spaceTest = GetTestField("P1",1)

        mecaPhysics = self.problem.internalSolveur.physics[0]
        AOperator = HookeIso(GetField("young",1)[0],mecaPhysics.coeffs["poisson"],dim=3)

        if levelSet.conform:
            factor= 1
        else:
            factor = GetField("factor",1)[0]

        numwform = factor*ToVoigtEpsilon(Strain(p)).T*AOperator*ToVoigtEpsilon(Strain(u))*p1spaceTest + p1space.T*p1spaceTest

        grad = FEField("P1",
                      cleanMesh,
                      cleanMeshSpace,
                      cleanMeshNumbering)

        fields.extend(list(self.problem.internalSolveur.fields.values()))

        m,f = IntegrateGeneral( mesh= cleanMesh,
                               wform= numwform,
                               constants= {},
                               fields= fields,
                               unknownFields= [grad]  ,
                               testFields= None,
                               elementFilter = ElementFilter(tag=self.problem.tagForPostProcess),
                               integrationRuleName="NodalEvalGeo",
                               onlyEvaluation=True)

        diag = m.diagonal()
        diag[diag==0] = 1
        f /= diag

        grad.data = f

        self.fSensitivity_val = np.zeros_like(levelSet.phi)
        self.fSensitivity_val[self.problem.cleanMeshToOriginalNodal] = -grad.GetPointRepresentation()

        self.problem.internalSolveur.unknownFields = uSave
        self.problem.internalSolveur.PushUnkownFieldsToSolutionVector()
        return RETURN_SUCCESS

    def GetNumberOfSolutions(self):
        return 2

    def GetCriteriaSolution(self,i):
        if i == 0:
            return self.u_num
        else:
            return self.p_num

    def GetCriteriaSolutionName(self,i):
        if i ==0:
            return "Dep"
        else:
            return "Adjoint"

RegisterCriteriaClass("DisplacementDeviation", TopoCriteriaDisplacementDeviation)

class TopoCriteriaTargetDisp(PhysicalCriteriaBase):
    def __init__(self, other=None):
        super(TopoCriteriaTargetDisp, self).__init__(other)

        self.__alpha = 2

        if other is None:
            self.SetName("TargetDisp")

            ### parameter of the compliant mechanism
            self.eTag = None
            self.__dir = np.array([1,0,0],dtype=MuscatFloat)
            self.__u0 = 0.
            self.f_val = -1.
        else:
            self.eTag = other.eTag
            self.__dir = np.copy(other.__dir)
            self.__u0 = other.__u0
            self.f_val = other.f_val

    def SetTargetDisplacement(self, data):
        val = PH.ReadFloats(data)

        self.__u0 = np.linalg.norm(val)
        self.__dir = val/self.__u0

    def UpdateValues(self,levelSet):
        cleanMesh = self.problem.internalSolveur.unknownFields[0].mesh
        cleanMeshSpace = self.problem.internalSolveur.unknownFields[0].space
        cleanMeshNumbering = self.problem.internalSolveur.unknownFields[0].numbering


        self.u_num = self.problem.GetNodalSolution().copy()

        # save
        uSave = self.problem.internalSolveur.unknownFields
        saveOldSol = self.problem.internalSolveur.sol
        # put new fields in the internal solver
        # we do this to reuse the same solver for the adjoint problem
        self.problem.internalSolveur.unknownFields = [f.copy() for f in self.problem.internalSolveur.unknownFields]

        """ J_2(\omega) = (\int_{\omega} k(x) |u-u0|^{\alpha} dx)^\frac{1}{\alpha}
        Update internal quantities
        must compute the value of J_2
        """

        """ |u*dir - u0|^2  """
        # we generate a full field
        u = np.array(uSave)

        disp_error = np.dot(u, self.__dir ) - self.__u0
        disp_error.name = "disp_error"

        #this is a field, we have to do the integration
        C0_arg = np.abs( disp_error )**2
        C0_arg.name = "C0_arg"

        """ Generation of a weak formulation for the integration of c0_args """
        """Sym part """
        gt = GetTestField("gt",1) ## unique test function to compute integrals
        C0_arg_sym = GetField("C0_arg",1)
        wf = gt*C0_arg_sym

        # numbering for an itegration
        g_numbering = ComputeDofNumbering(cleanMesh,ConstantSpaceGlobal)

        gt_num = FEField("gt", cleanMesh, ConstantSpaceGlobal, g_numbering,data=None)
        _,J2_arg = IntegrateGeneral( mesh=cleanMesh, wform=wf, constants={}, fields=[C0_arg], unknownFields=[gt_num], elementFilter= ElementFilter(tag=self.eTag) )
        self.f_val = J2_arg[0]**(1./self.__alpha)
        Info(self.f_val)

        C0 =J2_arg[0]**((1./self.__alpha )-1.)

        #########################   computation of the adjoint problem  #########################

        """
        -div(A*e(p)) = -C_0 k(x) |u -u0|**(\alpha-2) *(u-u0)  // volume loading
                   p = 0                                      // Dirichlet zero
          (A*e(p))*n = 0                                      // no Newmann


        note (u-u0) -> disp_error
        """
        adjoint_volume_loading  = -C0*disp_error
        adjoint_volume_loading.name = "avj"

        mecaPhysics = self.problem.internalSolveur.physics[0]
        sym_adjoint_volume_loading = (ElementFilter(tag=self.eTag), mecaPhysics.GetForceFormulation(self.__dir,"avj")  )

        self.problem.AddField(adjoint_volume_loading)
        self.problem.Resolve([sym_adjoint_volume_loading])

        self.p_num = self.problem.GetNodalSolution()
        fields = []
        for i,f in  enumerate(self.problem.internalSolveur.unknownFields):
            f_field = f.copy()
            f_field.name = "p_"+str(i)
            fields.append(f_field)

        VectorToFEFieldsData(self.problem.internalSolveur.sol,fields)

        self.problem.internalSolveur.sol = saveOldSol
        fields.extend(u)

        """ We compute the value of the derivative of J2

            J2'(\omega)(\theta) =  \int_{\omega_N} (  C0/{\alpha}*|u-u0|^2  +       # A)
                                                     A*e(u)*e(p) +                  # B)
                                                    -f*p         +                  # C)
                                                    -d(g*p)/dn   +                  # D)
                                                    -H*g*p                          # E)
                                                  ) \theta*n ds +
                                   \int_{\omega_D} (  C0/{\alpha}*|u-u0|^2  +       # F)
                                                     A*e(u)*e(p)                    # G)
                                                  ) \theta*n ds


        """
        ### symbolic part ###
        p = GetField("p",3)
        u = GetField("u",3)

        ### P1 interpolation
        p1space = GetField("P1",1)
        p1spaceTest = GetTestField("P1",1)

        AOperator = HookeIso(GetField("young",1)[0],mecaPhysics.coeffs["poisson"],dim=3)

        if levelSet.conform:
            factor= 1
        else:
            factor = GetField("factor",1)[0]

        numwform = factor*ToVoigtEpsilon(Strain(p)).T*AOperator*ToVoigtEpsilon(Strain(u))*p1spaceTest + p1space.T*p1spaceTest

        ufs = FEField("P1",
                      cleanMesh,
                      cleanMeshSpace,
                      cleanMeshNumbering)

        fields.extend(list(self.problem.internalSolveur.fields.values()))

        m,f = IntegrateGeneral( mesh= cleanMesh,
                               wform= numwform,
                               constants= {},
                               fields= fields,
                               unknownFields= [ufs]  ,
                               testFields= None,
                               elementFilter = ElementFilter( tag=TZ.Bulk),
                               integrationRuleName="NodalEvalGeo",
                               onlyEvaluation=True)

        diag = m.diagonal()
        diag[diag==0] = 1
        f /= diag

        grad = FEField("grad", cleanMesh,
                               cleanMeshSpace,
                               cleanMeshNumbering,
                               data=f)

        self.fSensitivity_val = np.zeros_like(levelSet.phi)
        self.fSensitivity_val[self.problem.cleanMeshToOriginalNodal] = grad.GetPointRepresentation()
        self.problem.internalSolveur.unknownFields = uSave
        self.problem.internalSolveur.PushUnkownFieldsToSolutionVector()

        return RETURN_SUCCESS

    def GetValue(self):
        return self.f_val

    def GetSensitivity(self):
        return self.fSensitivity_val

    def GetNumberOfSolutions(self):
        return 2

    def GetCriteriaSolution(self,i):
        if i == 0:
            return self.u_num
        else:
            return self.p_num

    def GetCriteriaSolutionName(self,i):
        if i ==0:
            return "Dep"
        else:
            return "Adjoint"

RegisterCriteriaClass("TargetDisp", TopoCriteriaTargetDisp)

def CheckIntegrity(GUI=False):
    TopoCriteriaCompliance()
    TopoCriteriaTargetDisp()
    return "OK"

if __name__ == "__main__":# pragma: no cover
    print(CheckIntegrity())
