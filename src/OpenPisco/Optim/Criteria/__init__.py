# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

_test = [
"Criteria",
"CriteriaFactory",
"GeoCriteria",
"PhyCriteria",
"GeoThicknessCriteria",
"PhyMecaCriteria",
"SemiInfiniteCriteria",
"ComposedCriteria",
"Regionalization"
]
