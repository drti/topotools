# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
import numpy as np
import Muscat.Helpers.ParserHelper as PH
from OpenPisco.Optim.Criteria.CriteriaFactory import RegisterClass as RegisterCriteriaClass
from OpenPisco.Optim.Criteria.Criteria import PhysicalCriteriaBase
import OpenPisco.PhysicalSolvers.FieldsNames as FN
import math
from OpenPisco import RETURN_SUCCESS


def CreateTopoCriteriaModal(ops):
    res=TopoCriteriaModal()
    assert "NUM_MODE_OPTIM" in ops,"NUM_MODE_OPTIM is mandatory for modal analysis"
    res.params={"NumModeOptim": PH.ReadInt(ops["NUM_MODE_OPTIM"])}
    del(ops["NUM_MODE_OPTIM"])
    PH.ReadProperties(ops,ops.keys(),res)
    return res


class TopoCriteriaModal(PhysicalCriteriaBase):
    """
    .. py:class:: TopoCriteriaModal

    Physical criteria for the modal analysis.
    """
    def __init__(self,other=None):
        """
        .. py:method:: __init__(other=None)

        Constructor of the class TopoCriteriaModal
        :param TopoCriteriaModal other : another instance of this very class
        """
        super(TopoCriteriaModal, self).__init__(other)

        self.update = True
        self._params=None

        if other is None:
            self.SetName("Modal")
        else:
            self.f_val = other.f_val
            self.fSensitivity_val = np.copy(other.fSensitivity_val)
            self._params = other._params

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, value):
        expectedCriteriaParameters=["NumModeOptim"]
        #Pseudo Invariants
        if any(Parameter not in expectedCriteriaParameters for Parameter in value.keys()):
            raise Exception("The following parameters are not available for a modal-related criteria ",value.keys()-set(expectedCriteriaParameters))
        elif any(Parameter not in value.keys() for Parameter in expectedCriteriaParameters):
            raise Exception("The following parameters were expected for a modal-related criteria but were not provided",set(expectedCriteriaParameters)-value.keys())
        elif value["NumModeOptim"]>self.problem.modalParams["Numbermaxfreq"]:
            raise Exception("The criteria can not ask for more eigenvalues than the physical analysis!")
        else:
            self._params = value


    def GetNumberOfSolutions(self):
        """
        .. py:method:: GetNumberOfSolutions()

        :return: the number of solutions in the problem
        """
        return self.problem.GetNumberOfSolutions()

    def GetCriteriaSolution(self,i):
        """
        .. py:method:: GetCriteriaSolution(i)

        :param int i : i-th solution of the problem
        :return: the i-th solution of the problem
        :rtype numpy array
        """
        return self.problem.GetNodalSolution(i)

    def GetCriteriaSolutionName(self,i):
        """
        .. py:method:: GetCriteriaSolutionName(i)

        :param int i : i-th solution of the problem
        :return: name of the i-th solution of the problem
        :rtype string
        """
        return f"DepModal_{i}"

    def GetScalarsOutputs(self):
        """
        .. py:method:: GetScalarsOutputs()

        :return: name of the scalars associated to their respective value to be saved through the iterations
        :rtype dict scalar by string
        """
        scalarOutputs={}
        pulsSquared = self.problem.GetAuxiliaryScalarsbyOrders(FN.EigenFreqSquared)
        frequencies=[math.sqrt(-pulsSqu)/(2*math.pi) for pulsSqu in pulsSquared]
        scalarOutputs={"frequency_"+str(indexFrequency+1):frequency for indexFrequency,frequency in enumerate(frequencies)}
        return scalarOutputs

    def SetAuxiliaryQuantities(self,levelSet):
        self.problem.SetAuxiliaryFieldGeneration(FN.EigenFreqSquared_sensitivity,on=FN.Nodes)
        self.problem.SetAuxiliaryScalarsbyOrderGeneration(FN.EigenFreqSquared,listSize=self.problem.modalParams["Numbermaxfreq"])
        assert self.params is not None,"This criteria can not be computed without its associated parameters."
        self.problem.criteriaParams=self.params

    def UpdateValues(self,point):
        """
        .. py:classmethod:: UpdateValues(point)

        Update values of criterion and sensitivity for the current levelset

        :param LevelSet point : current level set
        :return: paramater indicating if the update was successfull
        :rtype: bool
        """
        self.point = point
        pulsSquared = self.problem.GetAuxiliaryScalarsbyOrders(FN.EigenFreqSquared)
        self.f_val=pulsSquared[self.params["NumModeOptim"]-1]
        self.fSensitivity_val = self.problem.GetAuxiliaryField(FN.EigenFreqSquared_sensitivity,on=FN.Nodes).flatten()
        return RETURN_SUCCESS


RegisterCriteriaClass("Modal", TopoCriteriaModal)


def CheckIntegrity(GUI=False):
    TopoCriteriaModal()
    return "OK"

if __name__ == "__main__":# pragma: no cover
    print(CheckIntegrity())
