# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

# -*- coding: utf-8 -*-

from Muscat.Helpers.Factory import Factory
import Muscat.Helpers.ParserHelper as PH

def RegisterClass(name, classtype, constructor=None, withError=True):
    return CriteriaFactory.RegisterClass(name, classtype, constructor=constructor, withError=withError)

def Create(name, ops=None):
   res = CriteriaFactory.Create(name,ops)

   ## Just assign props without convertion
   props = ["problem"]
   PH.ReadProperties(ops, props, res, typeConversion=False)
   ## delete the already asigned info
   for key in props:
       if key in ops:
          del ops[key]

   PH.ReadProperties(ops, ops, res, typeConversion=True)

   return res

class CriteriaFactory(Factory):
    _Catalog = {}
    _SetCatalog = set()

    def __init__(self):
        super(CriteriaFactory, self).__init__()

def InitAllCriteria():
    import OpenPisco.Optim.Criteria.ComposedCriteria
    import OpenPisco.Optim.Criteria.GeoCriteria
    import OpenPisco.Optim.Criteria.GeoThicknessCriteria
    import OpenPisco.Optim.Criteria.PhyCriteria
    import OpenPisco.Optim.Criteria.PhyMecaCriteria
    import OpenPisco.Optim.Criteria.PhyThermalCriteria
    import OpenPisco.Optim.Criteria.PhyModalCriteria
    import OpenPisco.Optim.Criteria.PhyHarmonicCriteria
    import OpenPisco.Optim.Criteria.PhyRobustCriteria
    import OpenPisco.Optim.Criteria.PhyBucklingCriteria
    import OpenPisco.Optim.Criteria.AMInherentStrainCriteria

def CheckIntegrity():
    return "OK"

if __name__ == '__main__':
        print(CheckIntegrity())
