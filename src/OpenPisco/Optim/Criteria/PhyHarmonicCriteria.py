# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import numpy as np
import math

import Muscat.Helpers.ParserHelper as PH
import Muscat.Containers.ElementsDescription as ED
from Muscat.Types import MuscatFloat

from OpenPisco.Optim.Criteria.CriteriaFactory import RegisterClass as RegisterCriteriaClass
from OpenPisco.Optim.Criteria.Criteria import PhysicalCriteriaBase
import OpenPisco.PhysicalSolvers.FieldsNames as FN
from OpenPisco import RETURN_SUCCESS

def CellToPointDataOnSurface(support,surfaceTag,field):
    nodalField = np.zeros( (support.GetNumberOfNodes(),3), dtype = MuscatFloat)
    weight = np.zeros( support.GetNumberOfNodes(), dtype = MuscatFloat)
    tria = support.GetElementsOfType(ED.Triangle_3)
    elems=tria.tags[surfaceTag]
    conn = tria.connectivity   
    from OpenPisco.Unstructured.Levelset import triangle_3d_areas
    weightAtElement = triangle_3d_areas(support.nodes[conn,:])
    for elId in elems.GetIds():
        nodalField[conn[elId,:]] += field[elId]*weightAtElement[elId]
        weight[ conn[elId,:] ] += weightAtElement[elId] 
    ids = np.nonzero (weight > 0)[0]
    redWeight=np.expand_dims(weight[ids], axis=1)
    nodalField[ids] = nodalField[ids]/redWeight
    return nodalField

def triangle_3d_normals(coords):
    assert coords.shape[-1] == 3 and coords.shape[-2] == 3
    return np.cross(
        coords[..., 1, :] - coords[..., 0, :],
        coords[..., 2, :] - coords[..., 0, :])


def CreateTopoCriteriaERP(ops):
    # <Objective  type="ERP"  name="ERP" useProblem="1" density_flu="1.189e-12" veloc_flu="3.43e5" radia_fact="1.0" radia_Surface="ET3"/>
    res=TopoCriteriaERP()
    try:
        params={
                "density_flu":PH.ReadFloat(ops["density_flu"]),
                "veloc_flu":PH.ReadFloat(ops["veloc_flu"]),
                "radia_fact":PH.ReadFloat(ops["radia_fact"]),
                "radia_Surface" :PH.ReadString(ops["radia_Surface"])
                  }
    except KeyError as e:
        raise Exception("parameter "+e.args[0]+" not properly defined for this criteria")

    if "zone" in ops:
        res.nonOptimSurfZone=ops["zone"]
        del(ops["zone"])

    if "optimizableRadiatingSurface" in ops:
        res.optimizableRadiatingSurface=PH.ReadBool(ops["optimizableRadiatingSurface"])
        del(ops["optimizableRadiatingSurface"])

    for parameterName in params.keys():
        del(ops[parameterName])
    res.params=params
    PH.ReadProperties(ops,ops.keys(),res)
    return res

class TopoCriteriaERP(PhysicalCriteriaBase):
    """
    .. py:class:: TopoCriteriaERP

    Physical criterion for the ERP in harmonic analysis.
    """
    def __init__(self,other=None,):
        super(TopoCriteriaERP, self).__init__(other)
        self.update = True

        if other is None:
            self.SetName("ERP")
            self.problem = None
            self.f_val = 0.0
            self.fSensitivity_val = None
            self._params = None
            self.onzone=None
            self.optimizableRadiatingSurface=False
            self.nonOptimSurfZone = None
            self.pureSurfacicOptim = False
        else:
            self.f_val = other.f_val
            self.fSensitivity_val = np.copy(other.fSensitivity_val)
            self._params = other._params
            self.onzone = other.onzone 
            self.optimizableRadiatingSurface=other.optimizableRadiatingSurface
            self.nonOptimSurfZone = other.nonOptimSurfZone
            self.pureSurfacicOptim = other.pureSurfacicOptim

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, value):
        expectedCriteriaParameters=["density_flu","veloc_flu","radia_fact","radia_Surface"]
        #Pseudo Invariants
        if any(Parameter not in expectedCriteriaParameters for Parameter in value.keys()):
            raise Exception("The following parameters are not available for the ERP criteria ",value.keys()-set(expectedCriteriaParameters))
        elif any(Parameter not in value.keys() for Parameter in expectedCriteriaParameters):
            raise Exception("The following parameters were expected for the ERP criteria but were not provided",set(expectedCriteriaParameters)-value.keys())
        self._params = value

    def GetNumberOfSolutions(self):
        """
        .. py:method:: GetNumberOfSolutions()

        :return: the number of solutions in the problem
        """
        return self.problem.GetNumberOfSolutions()

    def GetCriteriaSolution(self,i,onCleanMesh=False):
        """
        .. py:method:: GetCriteriaSolution(i)

        :param int i : i-th solution of the problem
        :return: the i-th solution of the problem
        :rtype numpy array
        """
        return self.problem.GetNodalSolution(i,onCleanMesh=onCleanMesh)

    def GetCriteriaSolutionName(self,i):
        """
        .. py:method:: GetCriteriaSolutionName(i)

        :param int i : i-th solution of the problem
        :return: name of the i-th solution of the problem
        :rtype string
        """
        return f"DepErp_{i}"

    def GetScalarsOutputs(self):
        """
        .. py:method:: GetScalarsOutputs()

        :return: name of the scalars associated to their respective value to be saved through the iterations
        :rtype dict scalar by string
        """
        scalarOutputs={}
        if self.problem.name=='AsterHarmonic':
            modalCumulMassdX=self.problem.GetAuxiliaryScalar(FN.ModalCumulMassFractiondX)
            modalCumulMassdY=self.problem.GetAuxiliaryScalar(FN.ModalCumulMassFractiondY)
            modalCumulMassdZ=self.problem.GetAuxiliaryScalar(FN.ModalCumulMassFractiondZ)
            scalarOutputs={
                              "modalCumulMassdX": modalCumulMassdX,
                              "modalCumulMassdY": modalCumulMassdY,
                              "modalCumulMassdZ": modalCumulMassdZ,
                           }
            eigenFreqs = self.problem.GetAuxiliaryScalarsbyOrders(FN.eigen_Frequencies)
            for eigenNum,eigenFreq in enumerate(eigenFreqs):
                scalarOutputs["eigenFreq_"+str(eigenNum)]=eigenFreq

        return scalarOutputs

    def SetAuxiliaryQuantities(self,levelSet):
        if not self.pureSurfacicOptim:
            self.problem.SetAuxiliaryFieldGeneration(FN.ERP_sensitivity,on=FN.Nodes)
        self.problem.SetAuxiliaryFieldGeneration(FN.ERP_density,on=FN.Nodes)
        self.problem.SetAuxiliaryScalarsbyOrderGeneration(FN.ERP,listSize=self.problem.GetNumberOfSolutions()/2)
        if self.problem.name=='AsterHarmonic':
            self.problem.SetAuxiliaryScalarsbyOrderGeneration(FN.eigen_Frequencies,listSize=self.problem.harmonicParams["Numbermaxfreq"])
            self.problem.SetAuxiliaryScalarGeneration(FN.ModalCumulMassFractiondX)
            self.problem.SetAuxiliaryScalarGeneration(FN.ModalCumulMassFractiondY)
            self.problem.SetAuxiliaryScalarGeneration(FN.ModalCumulMassFractiondZ)
        assert self.params is not None,"This criteria can not be computed without its associated parameters."
        self.problem.criteriaParams=self.params

        if self.nonOptimSurfZone is None and self.IsSurfaceOptimizable(levelSet):
            raise Exception("Radiating surface is not properly followed through iteration")
        elif self.nonOptimSurfZone is not None and self.IsSurfaceOptimizable(levelSet):
            self.problem.followRadiaTag = True
            self.problem.fixedZone = self.nonOptimSurfZone

    def UpdateValues(self,levelSet):
        """
        .. py:classmethod:: UpdateValues(self, point)

        Update values of criterion and sensitivity for the current levelset

        :param LevelSet levelSet : current level set
        :return: paramater idicating if the update was successfull
        :rtype: bool
        """
        #Compute value of criteria
        self.f_val = self.ComputeObjective()

        #Compute Sensitivity
        self.fSensitivity_val=self.ComputeSensitivity(levelSet)
        return RETURN_SUCCESS

    def ComputeObjective(self):
        """
        .. py:classmethod:: ComputeObjective()

        Compute value of criterion
        :return: value of the criterion
        :rtype: float
        """
        erps = self.problem.GetAuxiliaryScalarsbyOrders(FN.ERP)
        #Compute average of erp over range of frequency
        harmonicParams=self.problem.harmonicParams
        pulsations=[2*math.pi*(harmonicParams["freqExcitMin"]+delta*harmonicParams["freqExcitStep"]) for delta in range(len(erps))]
        avg_erp=np.trapz(erps,x=pulsations)/(2*math.pi*harmonicParams["freqExcitMax"]-2*math.pi*harmonicParams["freqExcitMin"])
        return avg_erp

    def SolutionAndGradientFromDisp(self,solId):
            #Compute solution and gradient of solution at nodes
            solution=self.GetSolution(solId,onCleanMesh=True)
            from OpenPisco.Unstructured.LevelsetTools import ComputeGradientOnTetrahedrons
            numByComponent={"x":0,"y":1,"z":2}
            solByComponent={}
            derivByComponent={}
            for name,numId in numByComponent.items():
                solCompo=solution[:,numId]
                solByComponent["u"+name]=solCompo
                dsol_dnum =ComputeGradientOnTetrahedrons(self.problem.cleanMesh,solCompo)
                for nameDeriv,numIdDeriv in numByComponent.items():
                    derivByComponent["u"+name+nameDeriv]=dsol_dnum[:,numIdDeriv]
            return solByComponent,derivByComponent

    def ExtendFieldFromCleanMeshToMesh(self,field,nbdofPerNodes,extendedMesh):
            usedNodes = len(self.problem.cleanMeshToOriginalNodal)
            if nbdofPerNodes==1:
                extendedField = np.zeros(extendedMesh.GetNumberOfNodes() ,dtype=MuscatFloat)
                extendedField[self.problem.cleanMeshToOriginalNodal] = field[0:usedNodes]
            else:
                extendedField = np.zeros((extendedMesh.GetNumberOfNodes(),nbdofPerNodes) ,dtype=MuscatFloat)
                extendedField[self.problem.cleanMeshToOriginalNodal] = field[0:usedNodes,:]
            return extendedField


    def SensitivityGradJ(self,extendedMesh,sol,grad):
        #U
        #Concatenation of displacements (nbDof vector)
        ux,uy,uz=sol["ux"],sol["uy"],sol["uz"]
        disp=np.stack((ux,uy,uz),axis=0).transpose()

        #dj_Erp=(U.n)n
        normals=self.normalAtNodes
        proj_un=np.einsum('ij,ij->i', disp, normals)
        dj_Erp =np.einsum("i,ij->ij",proj_un,normals)

        #grad(U).transpose() 
        exx,exy,ezx=grad["uxx"],grad["uxy"],grad["uzx"]
        ex=np.stack((exx,exy,ezx),axis=0).transpose()
        eyx,eyy,eyz=grad["uyx"],grad["uyy"],grad["uyz"]
        ey=np.stack((eyx,eyy,eyz),axis=0).transpose()
        ezx,ezy,ezz=grad["uzx"],grad["uzy"],grad["uzz"]
        ez=np.stack((ezx,ezy,ezz),axis=0).transpose()
        gradTranspose=np.dstack((ex,ey,ez))

        #(grad(U).transpose()dj_Erp).n
        grad_JERP=np.einsum('kij,kj->ki', gradTranspose, dj_Erp)
        surfSensitivity=np.einsum('ij,ij->i', grad_JERP, normals)

        surfSensitivity=self.ExtendFieldFromCleanMeshToMesh(field=surfSensitivity,nbdofPerNodes=1,extendedMesh=extendedMesh)
        return surfSensitivity


    def SensitivityJ(self,extendedMesh,sol_re,sol_imag):
        #Extract real displacement
        ux,uy,uz=sol_re["ux"],sol_re["uy"],sol_re["uz"]
        disp_re=np.stack((ux,uy,uz),axis=0).transpose()

        #Extract imag displacement
        ux,uy,uz=sol_imag["ux"],sol_imag["uy"],sol_imag["uz"]
        disp_im=np.stack((ux,uy,uz),axis=0).transpose()

        #Compute squared modulus of normal displacement
        normals=self.normalAtNodes
        ure_n=np.einsum('ij,ij->i', disp_re, normals)
        uim_n=np.einsum('ij,ij->i', disp_im, normals)
        ucomp_n=ure_n+1j*uim_n
        u_sqmodulus=np.absolute(ucomp_n)**2
        u_sqmodulus=self.ExtendFieldFromCleanMeshToMesh(field=u_sqmodulus,nbdofPerNodes=1,extendedMesh=extendedMesh)
        curvatureTerm=np.einsum('i,i->i',u_sqmodulus,self.meanCurveTria)
        return curvatureTerm

    def IsSurfaceOptimizable(self,levelSet):
        if self.optimizableRadiatingSurface:
            return True 
        else:
            from Muscat.Containers.Filters.FilterObjects import ElementFilter
            ff = ElementFilter( zone = self.onzone,zones=[])
            ff.SetDimensionality(2)
            for _,_,ids in ff(levelSet.support):
                onzoneTria=ids

            tris = levelSet.support.GetElementsOfType(ED.Triangle_3)
            surfacicTag = tris.tags[self.params["radia_Surface"]]
            radiaIds=surfacicTag.GetIds()

            intersect=set(radiaIds) & set(onzoneTria)
            compInter=set(radiaIds)
            surfaceOptimizable= (intersect!=compInter)
            return surfaceOptimizable

    def ComputeNormalsAtNodes(self):
        mesh=self.PhyProblemMesh
        tria = mesh.GetElementsOfType(ED.Triangle_3)
        surfconns = tria.connectivity
        surfPointsInElems = mesh.nodes[surfconns]
        normals=triangle_3d_normals(surfPointsInElems)
        normals=normals.transpose()/np.linalg.norm(normals,axis=1)
        normals[np.isnan(normals)] = 0.0
        normals=normals.transpose()
        self.normalAtNodes=CellToPointDataOnSurface(mesh,self.params["radia_Surface"],normals)

    def ComputeSensitivity(self,levelSet):
        """
        .. py:classmethod:: ComputeSensitivity()

        Compute value of the sensitivity
        :return: value of the sensitivity
        :rtype: numpy array
        """
        #Classical component of ERP sensitivity
        if not self.pureSurfacicOptim:
            res = self.problem.GetAuxiliaryField(FN.ERP_sensitivity,on="Nodes").flatten()
        else:
            res = np.zeros((levelSet.support.GetNumberOfNodes()))

        if self.IsSurfaceOptimizable(levelSet):
            #Build ERP extra term for optimizable radiating surface
            self.PhyProblemMesh=self.problem.cleanMesh
            mesh=levelSet.support
            from Muscat.FE.Spaces.FESpaces import LagrangeSpaceP1
            from Muscat.FE.DofNumbering import ComputeDofNumbering
            self.numbering = ComputeDofNumbering(mesh,LagrangeSpaceP1,fromConnectivity=True)

            #Compute normals at nodes
            self.ComputeNormalsAtNodes()

            #ComputeCurvature at nodes
            from OpenPisco.Unstructured.LevelsetTools import ComputeMeanCurvature
            self.meanCurveTria=ComputeMeanCurvature(levelSet)

            #Compute surface sensitivity for each frequency
            nbfreq=int(self.GetNumberOfSolutions()/2)
            harmonicParams=self.problem.harmonicParams
            factorERP=self.params["density_flu"]*self.params["veloc_flu"]*self.params["radia_fact"]
            pulsations=[2*math.pi*(harmonicParams["freqExcitMin"]+delta*harmonicParams["freqExcitStep"]) for delta in range(nbfreq)]
            surfSensitivity = np.zeros((nbfreq,mesh.GetNumberOfNodes()))

            for i,pulsation in enumerate(pulsations):
                multfactor=factorERP*pulsation**2
                sol_re,grad_re=self.SolutionAndGradientFromDisp(solId=2*i)
                derSensitivRe=self.SensitivityGradJ(extendedMesh=mesh,sol=sol_re,grad=grad_re)
                sol_im,grad_im=self.SolutionAndGradientFromDisp(solId=2*i+1)
                derSensitivIm=self.SensitivityGradJ(extendedMesh=mesh,sol=sol_im,grad=grad_im)
                curvSensitiv=self.SensitivityJ(extendedMesh=mesh,sol_re=sol_re,sol_imag=sol_im)
                surfSensitivity[i,:]=multfactor*(derSensitivRe+derSensitivIm+0.5*curvSensitiv)

            #Integration on all pulsations
            avg_surfSensitivityNodes=np.trapz(surfSensitivity,x=pulsations,axis=0)

            #Remove surfacic contribution of radiating surface in ERP sensitivity
            tris = mesh.GetElementsOfType(ED.Triangle_3)
            surfacicTag = tris.tags[self.params["radia_Surface"]]
            surfconns = tris.connectivity[surfacicTag.GetIds(),:]
            res[surfconns]=0

            #Compute full sensitivity
            res+=avg_surfSensitivityNodes

        return res


RegisterCriteriaClass("ERP", TopoCriteriaERP,CreateTopoCriteriaERP)


def CheckIntegrity(GUI=False):
    criteriaParams={"density_flu":1.189e-12,"veloc_flu":3.43e5,"radia_fact":1.0,"radia_Surface":"Surface"}
    f = TopoCriteriaERP()
    f.params=criteriaParams
    return "OK"

if __name__ == "__main__":# pragma: no cover
    print(CheckIntegrity())
