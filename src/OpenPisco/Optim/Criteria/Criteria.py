# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
import abc

import numpy as np
import copy

import Muscat.Helpers.ParserHelper as PH
from OpenPisco import RETURN_SUCCESS

def UnimplementedFunction(name):
    from functools import partial
    def func(_name):
        raise Exception("Please set the function '"+_name+ "' !!!")
    return partial(func,name)

class CriteriaBase(metaclass=abc.ABCMeta):
     def __init__(self, other=None):
        super(CriteriaBase, self).__init__()

        self.__name__ = None
        self.upperBound = np.inf
        self.targetValue = np.inf
        self.__hasSolution__ = False

        if other is not None:
            self.__name__ = other.__name__
            self.upperBound = other.upperBound
            self.targetValue = other.targetValue
            self.SetName(other.GetName())
            self.__hasSolution__ = other.__hasSolution__

     def SetName(self,name):
         self.__name__ = name

     def SetUpperBound(self,bound):
         self.upperBound = PH.ReadFloat(bound)

     def GetUpperBound(self):
         return self.upperBound

     def SetTargetValue(self,value):
         self.targetValue = PH.ReadFloat(value)

     def GetTargetValue(self):
         return self.targetValue

     def GetName(self):
         return self.__name__

     def GetValue(self):
         """
         .. py:classmethod:: GetValue()

         Get the current value of the criterion
         :return: current value of the criterion
         :rtype: float
         """
         return self.f_val

     def GetSensitivity(self):
         """
         .. py:classmethod:: GetSensitivity()

         Get the current value of the sensitivity
         :return: current value of the sensitivity
         :rtype: ndarray
         """
         return self.fSensitivity_val

     @abc.abstractmethod
     def UpdateValues(self,point,phi):
         """
         .. py:classmethod:: UpdateValues(self, point)

         Update value and sensitivity of the criterion for the current levelset

         :param point: current level set
         :type point: LevelSet
         :return: paramater indicating if the update was successfull
         :rtype: bool
         """
         pass

     @abc.abstractmethod
     def GetNumberOfSolutions(self):
         """
         .. py:classmethod:: GetNumberOfSolutions(self)

         Retrieve the number of solutions of the current criterion
         :return: number of solutions
         :rtype: int
         """
         pass
     
     def AssertSuitableSolutionId(self,i):
         assert i < self.GetNumberOfSolutions(),"i out of bounds "

     def GetScalarsOutputs(self)->dict:
         return dict()

     def UpdateCriteriaWhenIterationAccepted(self):
         pass

class PhysicalCriteriaBase(CriteriaBase):
     def __init__(self, other=None):
        super(PhysicalCriteriaBase, self).__init__(other)
        self.problem = None
        self.adjointProblem = None

        if other is not None:
            self.problem = other.problem
            self.adjointProblem = other.adjointProblem

     def GetSolution(self,i):
         self.AssertSuitableSolutionId(i)
         return self.GetCriteriaSolution(i)

     @abc.abstractmethod
     def GetCriteriaSolution(self,i):
         """
         .. py:classmethod:: GetCriteriaSolution(self,i)

         Retrieve the solution of index i. The index i should be smaller than the total number of solutions
         :param int i : index of the current solution
         :return: solution
         :rtype: ndarray
         """
         pass

     def GetSolutionName(self,i):
         self.AssertSuitableSolutionId(i)
         return self.GetCriteriaSolutionName(i)

     @abc.abstractmethod
     def GetCriteriaSolutionName(self,i):
         """
         .. py:classmethod:: GetCriteriaSolutionName(self,i)

         Retrieve the name of the solution of index i. The index i should be smaller than the total number of solutions
         :param int i : index of the current solution
         :return: solution name
         :rtype: str
         """
         pass

     def SetProblem(self,problem):
         self.problem=problem

     def SetAdjointProblemLikeDirect(self,problemArg=None):
         if problemArg is None:
             self.adjointProblem=type(self.problem)()
         else:
             self.adjointProblem=type(self.problem)(problemArg)

     def SetAuxiliaryQuantities(self,point):
         pass

     def UpdateCriteriaValues(self,point):
         self.SetAuxiliaryQuantities(point)
         if self.problem.SolveByLevelSet(point) != RETURN_SUCCESS:
             print("Error solving " + str(self.problem) )
         self.UpdateValues(point)
         return RETURN_SUCCESS

class CriteriaFunctionPointer(CriteriaBase):
     def __init__(self, other=None):
        super(CriteriaFunctionPointer, self).__init__(other)
        if other is None:
            self.f = UnimplementedFunction('f')
            self.f_val = None
            self.fSensitivity = UnimplementedFunction('fSensitivity')
            self.fSensitivity_val = []
        else:
            self.f = other.f
            self.f_val = copy.deepcopy(other.f_val)
            self.fSensitivity = other.fSensitivity
            self.fSensitivity_val = copy.deepcopy(other.fSensitivity_val)

     def UpdateValues(self,point):
         self.f_val = self.f(point)
         self.fSensitivity_val = self.fSensitivity(point)
         return RETURN_SUCCESS

     def GetNumberOfSolutions(self):
         return 0

     def GetSolution(self,i):
         return None

def CheckIntegrity(GUI=False):
    return "OK"

if __name__ == "__main__":# pragma: no cover
    print(CheckIntegrity())
