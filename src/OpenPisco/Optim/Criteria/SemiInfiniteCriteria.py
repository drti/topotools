# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

# This file aims to describe some basic features to handle the so-called semi-infinite criteria,
# that is to say criteria whose value is not defined globally but localy, i.e. we expect a given condition
# to be fulfilled for each local value living on a specific geometrical support (be it nodes, elements, integration points etc)...
#
# However, we expect the user to provide both the local values and a way to aggregate in a single value these local values (P-norm for instance) in the original criteria.
# If the semi-infinite criteria methods called here are not used, the default behaviour is to use the aggregation method proposed in the original criterion.
#
# Note: all the methods proposed here assume that the criteria considered is to be used as a constraint, they are no feature available to do otherwise.

from OpenPisco.Optim.Criteria.Criteria import PhysicalCriteriaBase
from OpenPisco.Optim.Criteria.Regionalization import Regionalization
from OpenPisco.Optim.Criteria.Renormalization import Renormalization

class SemiInfiniteCriteriaBase(PhysicalCriteriaBase):
    """
    .. py:class:: SemiInfiniteCriteriaBase

    Basic class for semi-infinite criteria, it is a specialization of PhysicalCriteriaBase to take into account local values.

    Attributes:
        localValues: _, contains the local values inherent to a semi-infinite criteria
        method: String, method to be used to handle a semi-infinite criteria
        localValueSupport: String (FieldsNames), type of geometrical support for the local values field (namely nodes, integration points)
        computationalElementFilter: _, support where the local values field lives, enable the method to overide the definition of integration support if required
        dimensionalitySupport: Integer, dimension of the computational support (namely 2 for a surface, 3 for a volume)
    """
    def __init__(self, other=None):
        super(SemiInfiniteCriteriaBase, self).__init__(other)
        self.localValues=None
        self.method=None
        self.localValueSupport=None
        self.computationalElementFilter=None
        self.dimensionalitySupport=0

    @classmethod
    def CriteriaOperation(cls,*args,**nargs):
        """
        .. py:classmethod:: CriteriaOperation(cls,*args,**nargs)

        The main function of this class, it decorates a criteria class
        :param cls : a semi-infinite criteria class
        :return: a semi-infinite criteria instance decorated with the specific method chosen to handle local values
        """
        methodByName = {"Regionalization":Regionalization,"Renormalization":Renormalization }
        return methodByName[args[0]](cls,**nargs)

    def GetDiscreteImplementation(self):
        """
        .. py:method:: GetDiscreteImplementation()

        Accumulate the constraint(s) to be added in the optimization problem for each method to work
        :return: discrete implementations of constraint specific for each method
        """
        discrteImpl=[]
        if self.method is None:
            discrteImpl.append(self)
        elif self.method=="Regionalization":
            for regionID in range(self.numberOfRegions):
                objCritID=type(self)(self)
                objCritID.SetRegionID(regionID)
                objCritID.SetName("Reg"+str(self.GetName())+str(regionID))
                discrteImpl.append(objCritID)
        elif self.method=="Renormalization":
            discrteImpl.append(self)
        else:
            raise Exception("Method not recognized")
        return discrteImpl

    def ComputeLocalValues(self):
        """
        Compute specific local values, to be redefined according to criteria
        """
        raise NotImplementedError()

    def ComputeComputationalElementFilter(self):
        """
        .. py:method:: ComputeComputationalElementFilter(levelSet)

        Compute computational support, to be redefined according to criteria
        """
        raise NotImplementedError()


def CheckIntegrity(GUI=False):
    return "OK"

if __name__ == "__main__":# pragma: no cover
    print(CheckIntegrity())
