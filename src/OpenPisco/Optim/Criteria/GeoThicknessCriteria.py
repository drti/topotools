# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import numpy as np

from Muscat.Types import MuscatFloat
import Muscat.Containers.ElementsDescription as ED
from Muscat.Containers.Mesh import Mesh
from Muscat.Containers.Filters.FilterObjects import ElementFilter
from Muscat.Containers.MeshInspectionTools import GetVolumePerElement, ComputeMeshMinMaxLengthScale
from Muscat.Containers.MeshFieldOperations import PointToCellData
from Muscat.Containers.NativeTransfer import NativeTransfer

from OpenPisco.Optim.Criteria.CriteriaFactory import RegisterClass as RegisterCriteriaClass
from OpenPisco.Optim.Criteria.Criteria import CriteriaBase
import OpenPisco.TopoZones as TZ
from OpenPisco.Unstructured.LevelsetTools import ComputeGradientOnTetrahedrons, ComputeMeanCurvature, ComputeGaussianCurvature
from OpenPisco import RETURN_SUCCESS


def GetInterpolationOperator(mesh: Mesh) -> NativeTransfer:
    """Create a interpolator for point data

    Parameters
    ----------
    inputsuppmeshort : Mesh
        input mesh

    Returns
    -------
    NativeTransfer
        a NativeTransfer class with the following API

        nt.SetTargetPoints(interpolationPoints)
        nt.Compute()
        interpolationOperator = nt.GetOperator()
        interpolationStatus = nt.GetStatus()
        phiAtIntPoints = interpolationOperator.dot(PointField)
    """

    from Muscat.FE.FETools import PrepareFEComputation
    from Muscat.FE.Fields.FEField import FEField

    nt = NativeTransfer()
    nt.SetVerbose(False)
    nt.SetTransferMethod("Interp/Clamp")
    spaces, numberings, _, _ = PrepareFEComputation(mesh, numberOfComponents=1)
    field = FEField(name="", mesh=mesh, space=spaces, numbering=numberings[0])
    nt.SetSourceFEField(field, None)
    return nt


class TopoCriteriaMaxThickness(CriteriaBase):
    def __init__(self, other=None):
        super(TopoCriteriaMaxThickness, self).__init__(other)
        self.update = True
        self.f_val = 0.0
        self.fsensitivity_val = 0.0
        self.dmax = 0.0
        self.jacobianfree = True
        self.mean = None
        self.gaussian = None

        if other is None:
            self.SetName("MaxThickness")

    def SetMaxThick(self, maxthick):
        self.dmax = maxthick

    def ComputeObjective(self, levelset):
        vol = GetVolumePerElement(levelset.support, ElementFilter(elementType=ED.Tetrahedron_4))
        fieldAtCentroids = PointToCellData(levelset.support, levelset.phi, dim=3)
        fieldAtCentroids = np.minimum(fieldAtCentroids + 0.5 * self.dmax, 0.0)
        return np.sum(fieldAtCentroids**2 * vol, axis=0)

    def ComputeLayerContributionToRayIntegral(self, data, ids):
        toIntegrate = 2 * np.fmin(data + 0.5 * self.dmax, 0.0)
        if not self.jacobianfree:
            surfJacobian = 1 + 2 * data * self.mean[ids] + data**2 * self.gaussian[ids]
            toIntegrate *= surfJacobian
        return toIntegrate

    def ComputeSensitivity(self, levelset):
        assert levelset.conform,"MaxThickness criterion : unable to compute sensitivity in non conformal setting "
        res = np.zeros(levelset.support.GetNumberOfNodes(), dtype=MuscatFloat)

        if not self.jacobianfree:
            self.mean = ComputeMeanCurvature(levelset)
            self.gaussian = ComputeGaussianCurvature(levelset)

        tris = levelset.support.GetElementsOfType(ED.Triangle_3)
        surfconn = tris.connectivity[tris.tags[TZ.InterSurf].GetIds(), :]
        surfids = np.unique(surfconn.flatten())
        surfPoints = levelset.support.nodes[surfids]
        delta = ComputeMeshMinMaxLengthScale(levelset.support)[0]
        normal = ComputeGradientOnTetrahedrons(levelset.support, levelset.phi)
        intPoints = surfPoints - 0.5 * delta * normal[surfids]

        nt = GetInterpolationOperator(levelset.support)
        nt.SetTargetPoints(intPoints)
        nt.Compute()
        phiAtIntPoints = nt.GetOperator().dot(levelset.phi)

        toIntegrate = self.ComputeLayerContributionToRayIntegral(phiAtIntPoints, surfids)
        res[surfids] += delta * toIntegrate

        maxcpt = 500
        cpt = 1
        coef = np.ones_like(phiAtIntPoints)

        while True:

            coef.fill(1.0)
            intPoints = surfPoints - (cpt + 0.5) * delta * normal[surfids]
            nt.SetTargetPoints(intPoints)
            nt.Compute()
            newphiAtIntPoints = nt.GetOperator().dot(levelset.phi)

            if (newphiAtIntPoints >= phiAtIntPoints).all() or cpt == maxcpt:
                break
            coef[newphiAtIntPoints >= phiAtIntPoints] = 0.0

            toIntegrate = self.ComputeLayerContributionToRayIntegral(newphiAtIntPoints, surfids)
            res[surfids] += coef * delta * toIntegrate

            phiAtIntPoints = np.copy(newphiAtIntPoints)
            cpt += 1

        return -res

    def UpdateValues(self, point):
        if self.update:
            self.f_val = self.ComputeObjective(point)
            self.fsensitivity_val = self.ComputeSensitivity(point)
        return RETURN_SUCCESS

    def GetNumberOfSolutions(self):
        return 0


RegisterCriteriaClass("MaxThickness", TopoCriteriaMaxThickness)


class TopoCriteriaMinThickness(CriteriaBase):
    def __init__(self, other=None):
        super(TopoCriteriaMinThickness, self).__init__(other)
        self.update = True
        self.f_val = 0.0
        self.fsensitivity_val = 0.0
        self.method = "EnergyFormulation"
        self.jacobianfree = True  # only used with method energy formulation
        self.mean = None
        self.gaussian = None
        if other is None:
            self.SetName("MinThickness")

    def SetMinThick(self, minthick):
        self.dmin = minthick

    def ComputeObjective(self, levelset):
        tris = levelset.support.GetElementsOfType(ED.Triangle_3)
        tag = tris.tags[TZ.InterSurf]
        surfconn = tris.connectivity[tag.GetIds(), :]
        surfids = np.unique(surfconn.flatten())
        surfPoints = levelset.support.nodes[surfids]
        delta = ComputeMeshMinMaxLengthScale(levelset.support)[0]
        N = int(self.dmin / delta)
        field = np.zeros(levelset.support.GetNumberOfNodes(), dtype=MuscatFloat)
        normal = ComputeGradientOnTetrahedrons(levelset.support, levelset.phi)

        nt = GetInterpolationOperator(levelset.support)

        for i in range(N):
            intPoints = surfPoints - (0.5 + i) * delta * normal[surfids]
            nt.SetTargetPoints(intPoints)
            nt.Compute()
            phiAtIntPoints = nt.GetOperator().dot(levelset.phi)
            field[surfids] += delta * np.maximum(phiAtIntPoints, 0.0) ** 2
        return levelset.InterfaceIntegral(field)

    def ComputeSensitivity(self, levelset):
        assert levelset.conform,"MinThickness criterion : unable to compute sensitivity in non conformal setting "
        tris = levelset.support.GetElementsOfType(ED.Triangle_3)
        tag = tris.tags[TZ.InterSurf]
        surfconn = tris.connectivity[tag.GetIds(), :]
        surfids = np.unique(surfconn.flatten())
        surfPoints = levelset.support.nodes[surfids]
        delta = ComputeMeshMinMaxLengthScale(levelset.support)[0]
        N = int(self.dmin / delta)
        field = np.zeros(levelset.support.GetNumberOfNodes(), dtype=MuscatFloat)
        normal = ComputeGradientOnTetrahedrons(levelset.support, levelset.phi)
        kappa = ComputeMeanCurvature(levelset)

        nt = GetInterpolationOperator(levelset.support)

        for i in range(N):
            intPoints = surfPoints - (0.5 + i) * delta * normal[surfids]
            nt.SetTargetPoints(intPoints)
            nt.Compute()
            phiAtIntPoints = nt.GetOperator().dot(levelset.phi)
            gradPhiAtIntPoints = nt.GetOperator().dot(normal)
            gradProduct = np.sum(normal[surfids] * gradPhiAtIntPoints, axis=1)
            field[surfids] += np.maximum(phiAtIntPoints, 0.0) * (kappa[surfids] * np.maximum(phiAtIntPoints, 0.0) + 2 * gradProduct)

        return -field

    def ComputeLayerContributionToRayIntegral(self, data, ids):
        toIntegrate = 2 * data * (1 + data) * np.fmax(data + 0.5 * self.dmin, 0.0)
        if not self.jacobianfree:
            surfJacobian = 1 + 2 * data * self.mean[ids] + data**2 * self.gaussian[ids]
            toIntegrate *= surfJacobian
        return toIntegrate

    def ComputeObjectiveEnergyFormulation(self, levelset):
        # https://hal.archives-ouvertes.fr/hal-00985000v2/document
        # Sect. 9.2.2
        vol = GetVolumePerElement(levelset.support, ElementFilter(elementType=ED.Tetrahedron_4))
        fieldAtCentroids = PointToCellData(levelset.support, levelset.phi, dim=3)
        fieldAtCentroids = np.fmax(fieldAtCentroids + 0.5 * self.dmin, 0.0) * fieldAtCentroids
        return -np.sum(fieldAtCentroids**2 * vol, axis=0)

    def ComputeSensitivityEnergyFormulation(self, levelset):
        # https://hal.archives-ouvertes.fr/hal-00985000v2/document
        # Sect. 9.2.2

        assert levelset.conform,"MinThickness criterion : unable to compute sensitivity in non conformal setting "
        res = np.zeros(levelset.support.GetNumberOfNodes(), dtype=MuscatFloat)

        if not self.jacobianfree:
            self.mean = ComputeMeanCurvature(levelset)
            self.gaussian = ComputeGaussianCurvature(levelset)

        tris = levelset.support.GetElementsOfType(ED.Triangle_3)
        surfconn = tris.connectivity[tris.tags[TZ.InterSurf].GetIds(), :]
        surfids = np.unique(surfconn.flatten())
        surfPoints = levelset.support.nodes[surfids]
        delta = ComputeMeshMinMaxLengthScale(levelset.support)[0]
        normal = ComputeGradientOnTetrahedrons(levelset.support, levelset.phi)
        intPoints = surfPoints - 0.5 * delta * normal[surfids]

        nt = GetInterpolationOperator(levelset.support)
        nt.SetTargetPoints(intPoints)
        nt.Compute()
        phiAtIntPoints = nt.GetOperator().dot(levelset.phi)

        toIntegrate = self.ComputeLayerContributionToRayIntegral(phiAtIntPoints, surfids)
        res[surfids] += delta * toIntegrate

        maxcpt = 500
        cpt = 1
        coef = np.ones_like(phiAtIntPoints)

        while True:

            coef.fill(1)
            intPoints = surfPoints - (cpt + 0.5) * delta * normal[surfids]
            nt.SetTargetPoints(intPoints)
            nt.Compute()
            newphiAtIntPoints = nt.GetOperator().dot(levelset.phi)

            if (newphiAtIntPoints >= phiAtIntPoints).all() or cpt == maxcpt:
                break
            coef[newphiAtIntPoints >= phiAtIntPoints] = 0.0

            toIntegrate = self.ComputeLayerContributionToRayIntegral(phiAtIntPoints, surfids)
            res[surfids] += coef * delta * toIntegrate

            phiAtIntPoints = np.copy(newphiAtIntPoints)
            cpt += 1

        return res

    def UpdateValues(self, point):
        if self.update:
            if self.method == "EnergyFormulation":
                self.f_val = self.ComputeObjectiveEnergyFormulation(point)
                self.fsensitivity_val = self.ComputeSensitivityEnergyFormulation(point)
            elif self.method == "Original":
                self.f_val = self.ComputeObjective(point)
                self.fsensitivity_val = self.ComputeSensitivity(point)
            else:
                raise Exception("Unknown method " + str(self.method))
        return RETURN_SUCCESS

    def GetValue(self):
        return self.f_val

    def GetSensitivity(self):
        return self.fsensitivity_val

    def GetNumberOfSolutions(self):
        return 0


RegisterCriteriaClass("MinThickness", TopoCriteriaMinThickness)

from Muscat.Containers.MeshCreationTools import CreateCube
import Muscat.ImplicitGeometry.ImplicitGeometryObjects as ImplicitGeometry
from OpenPisco.Unstructured.MmgMesher import MmgAvailable
from OpenPisco.Unstructured.Levelset import LevelSet
import OpenPisco.Unstructured.MmgMesher as MmgMesher

def CheckIntegrity(GUI=False):

    if not MmgAvailable:
        return "Not Ok, mmg3d_O3 not found!!"


    UM = CreateCube(dimensions=[10,10,10],spacing=[1./9, 1./9, 1./9],origin=[0, 0, 0],ofTetras=True)
    ls = LevelSet(support=UM)
    init = ImplicitGeometry.ImplicitGeometrySphere(center=[0.5, 0.5, 0.5], radius=0.3)
    ls.phi = init.ApplyVector(UM)
    ls.conform = True
    MmgMesher.MmgMesherActionLevelset(ls, {"iso": 0.0, "hausd": 0.01, "hmin": 0.01, "hmax": 0.7, "nr": True})
    toypoints = np.zeros((5, 3), dtype=MuscatFloat)
    for i in range(5):
        toypoints[i, :] = [1.0 / (i + 2), 1.0 / (i + 2), 1.0 / (i + 2)]

    c1 = TopoCriteriaMaxThickness()
    c1.SetMaxThick(0.1)
    c1.UpdateValues(ls)

    c2 = TopoCriteriaMinThickness()
    c2.SetMinThick(0.1)
    c2.UpdateValues(ls)
    c2.method = "Original"
    c2.UpdateValues(ls)

    return "OK"


if __name__ == "__main__":  # pragma: no cover
    print(CheckIntegrity())
