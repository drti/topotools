# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
import numpy as np

import Muscat.Helpers.ParserHelper as PH
from Muscat.Types import MuscatFloat
import Muscat.Containers.ElementsDescription as ED
from Muscat.Containers.Filters.FilterObjects import ElementFilter, ZT_ALL_NODES
from Muscat.FE.Fields.IPField import IPField
from Muscat.Containers.MeshInspectionTools import ExtractElementsByElementFilter,GetVolume
from Muscat.FE.MaterialHelp import HookeIso
from Muscat.ImplicitGeometry.ImplicitGeometryOperators import ImplicitGeometryInsideOut

from OpenPisco.Optim.Criteria.CriteriaFactory import RegisterClass as RegisterCriteriaClass
from OpenPisco.Optim.Criteria.Criteria import PhysicalCriteriaBase
from OpenPisco.Optim.Criteria.SemiInfiniteCriteria import SemiInfiniteCriteriaBase
from OpenPisco.BaseElements import elementsTypeSupportedForDimension
import OpenPisco.PhysicalSolvers.FieldsNames as FN
from OpenPisco.PhysicalSolvers.PhysicalSolverFactory import PhysicalSolverFactory, InitAllPhysicalSolvers
from OpenPisco.PhysicalSolvers.PhysicalSolversTools import ComputeSymMatrixProduct
from OpenPisco.PhysicalSolvers.AsterStatic import AsterStatic, GetMaterialFields
from OpenPisco.MuscatExtentions.IPFieldIntegration import IPFieldIntegration
from OpenPisco import RETURN_SUCCESS

DesignSpaceSupportbyFESpaceName = {}
DesignSpaceSupportbyFESpaceName["LagrangeP1"] = ED.Tetrahedron_4
DesignSpaceSupportbyFESpaceName["LagrangeP2"] = ED.Tetrahedron_10

def HookeIsoModified(E,nu,dim):
    C = HookeIso(E,nu,dim)
    if dim==3:
        C[3,3] *= 2
        C[4,4] *= 2
        C[5,5] *= 2
    if dim==2:
        C[2,2] *= 2
    return C

def CreateTopoCriteriaVonMises(ops):

    """
    <Constraint type="VonMises" name="VonMises" useProblem="1" UpperBound="0.1*11387960" CriteriaOffZone="13" >
    <useMethod type="Regionalization" submethod="Interlacing" numberOfRegions="3"/>
    </Constraint>
    """
    method = None
    methodParams={}

    if "children" in ops:
        for tag,child in ops["children"]:
            if tag.lower() == "usemethod":
                method = PH.ReadString(child['type'])
                submethod = PH.ReadString(child['submethod'])
                if method.lower()=="regionalization" and "numberOfRegions" in child:
                    methodParams["numberOfRegions"] = PH.ReadInt(child['numberOfRegions'])
        del ops["children"]
    if method is None:
        res = TopoCriteriaVonMises()
    else:
        res = TopoCriteriaVonMises.CriteriaOperation(method,submethod=submethod,params=methodParams)

    if 'zone' in ops:
        zone = ops['zone']
        res.offZone = ImplicitGeometryInsideOut(zone)
        del(ops['zone'])
    PH.ReadProperties(ops,ops.keys(),res)

    return res

class TopoCriteriaVonMises(SemiInfiniteCriteriaBase):
    def __init__(self, other=None):
        super(TopoCriteriaVonMises, self).__init__(other)

        self.update = True
        self.localValueSupport = FN.IPField

        if other is None:
            self.SetName("VonMises")
            self.f_val = -1
            self.alpha = 2.
            self.fSensitivity_val = np.array([])
            self.offZone = None #implicit zone that should be excluded in the global measurement of stress
            self.firstTime = True
            self.hasUpperBound = False
            self.factor = 1.
            self.automaticNormalization = False
            self.dimensionalitySupport= -1
        else:
            self.f_val = other.f_val
            self.alpha = PH.ReadFloat(other.alpha)
            self.fSensitivity_val = np.copy(other.fSensitivity_val)
            self.offZone = other.offZone
            self.firstTime = other.firstTime
            self.factor = other.factor
            self.hasUpperBound = other.hasUpperBound
            self.firstTime = other.firstTime
            self.automaticNormalization = other.automaticNormalization
            self.dimensionalitySupport = other.dimensionalitySupport
        self.computationalElementFilter = None

    def SetUpperBound(self,bound):
        if self.automaticNormalization:
            self.hasUpperBound = True
            self.factor = PH.ReadFloat(bound)
            self.upperBound = 1.
        else:
            super(TopoCriteriaVonMises, self).SetUpperBound(bound)

    def GetNumberOfSolutions(self):
        return 3

    def GetCriteriaSolution(self,i):
        disp=self.problem.GetNodalSolution()
        vmisesAtNodes = self.problem.GetAuxiliaryField(FN.von_mises,on=FN.Nodes)
        disp_adjoint = self.adjointProblem.GetNodalSolution()
        solution=[
                disp,
                vmisesAtNodes,
                disp_adjoint,
                ]
        return solution[i]

    def GetCriteriaSolutionName(self,i):
        solutionName=[
                    "Dep",
                    "Vmises",
                    "Dep_Adjoint_VonMises"
                    ]
        return solutionName[i]

    def GetScalarsOutputs(self):
        self.problem.SetAuxiliaryFieldGeneration(FN.von_mises,on=self.localValueSupport)
        vmises = self.problem.GetAuxiliaryField(FN.von_mises,on=self.localValueSupport,onCleanMesh=True)
        dimensionality = self.problem.cleanMesh.GetElementsDimensionality()
        data = vmises.data[elementsTypeSupportedForDimension[dimensionality]]
        scalarOutput={"maxVonMisesAtIP": np.max(data)}
        return scalarOutput

    def CreateAdjointProblemDictProperties(self):
        import copy
        adjointDict = copy.deepcopy(self.problem.dictProperties)
        for tag,mat in adjointDict["children"]:
            if tag == "Force" or tag == "Temperature":
               adjointDict["children"].remove( (tag,mat) )
        adjointDict["hasPreStress"] = True
        adjointDict["type"] = 'static_elastic'
        for name in ["solveThermalProblem","linearSolverName"]:
            if name in adjointDict.keys():
               del adjointDict[name]
        return adjointDict

    def SetAdjointProblem(self):

        adjointProblemdictProperties = self.CreateAdjointProblemDictProperties()
        InitAllPhysicalSolvers()
        factory = PhysicalSolverFactory()
        self.adjointProblem = factory.Create('GeneralAster',ops=adjointProblemdictProperties)
        self.adjointProblem.conform=self.problem.conform
        self.adjointProblem.SetSolverInterface()
        self.adjointProblem.SetAuxiliaryFieldGeneration(FN.strain,on=FN.IPField)

    def UpdateAdjointProblem(self):
        self.adjointProblem.SetSupport(self.problem.originalSupport)
        self.adjointProblem.cleanMesh = self.problem.cleanMesh
        self.adjointProblem.cleanMeshToOriginalNodal = self.problem.cleanMeshToOriginalNodal

    def SetAuxiliaryQuantities(self,levelSet):
        self.problem.SetAuxiliaryFieldGeneration(FN.stress,on=FN.IPField)
        self.problem.SetAuxiliaryFieldGeneration(FN.von_mises,on=self.localValueSupport)
        self.problem.SetAuxiliaryFieldGeneration(FN.von_mises,on=FN.Nodes)

    def UpdateValues(self,levelSet):

        # if first time initialise the normalization factor using the first shape
        if self.firstTime:
            if self.automaticNormalization:
               self.InitialiseNormalizationFactor()
            if self.dimensionalitySupport == -1:
               self.dimensionalitySupport = self.problem.cleanMesh.GetElementsDimensionality()
            self.SetAdjointProblem()
            self.firstTime = False

        self.localValues=self.ComputeLocalValues()
        self.computationalElementFilter=self.ComputeComputationalElementFilter()

         # update volume info
        self.volume = GetVolume(ExtractElementsByElementFilter(self.problem.cleanMesh,self.computationalElementFilter))

        self.f_val = self.ComputeObjective()

        self.fSensitivity_val=self.ComputeSensitivity(levelSet)

        return RETURN_SUCCESS

    def ComputeLocalValues(self):
        vmises = self.problem.GetAuxiliaryField(FN.von_mises,on=self.localValueSupport,onCleanMesh=True)
        return vmises

    def ComputeComputationalElementFilter(self):
        if self.offZone is not None:
            support = ElementFilter(dimensionality=self.dimensionalitySupport,zone=[self.offZone],zonesTreatment=ZT_ALL_NODES)
        else:
            support=ElementFilter(dimensionality=self.dimensionalitySupport)
        return support

    def ComputeObjective(self):
        vmises = self.localValues
        res = IPFieldIntegration((vmises)**self.alpha,self.computationalElementFilter)
        res *= 1.0/self.factor**self.alpha
        self.internalintegral = res
        return (res/self.volume)**(1.0/self.alpha)

    def InitialiseNormalizationFactor(self):

        if not self.hasUpperBound:
            for elementName,_,ids in ElementFilter(dimensionality=self.problem.cleanMesh.GetElementsDimensionality())(self.problem.cleanMesh):
                data = self.localValues.data[elementName]
                self.factor = np.max(data[ids])

    def GetStressTensorData(self,sigma):
        if sigma.shape[2] == 4: # code aster
            sigmadata = np.empty((sigma.shape[0], sigma.shape[1], 3))
            sigmadata[:, :, 0] = sigma[:, :, 0]
            sigmadata[:, :, 1] = sigma[:, :, 1]
            sigmadata[:, :, 2] = sigma[:, :, 3]
            return sigmadata
        elif sigma.shape[2] == 3: # muscat
            return np.mean(sigma, axis=1, keepdims=True)
        else:
            return sigma

    def GetPreStressData(self,Ajprime):
        if Ajprime.shape[2] == 3:
            prestress = np.zeros((Ajprime.shape[0], Ajprime.shape[1], 4))
            prestress[:, :, 0] = Ajprime[:, :, 0]
            prestress[:, :, 1] = Ajprime[:, :, 1]
            prestress[:, :, 3] = Ajprime[:, :, 2]
            return prestress
        else:
            return Ajprime

    def ComputeAdjointLoad(self,sigma,materialFields):
        ######### assembly prestress matrix A*j'(sigma(u) ##########################

        Vmatrix3D = np.array([[ 1., -0.5, -0.5,0.,0.,0.],
               [ -0.5, 1., -0.5,0.,0.,0.],
               [ -0.5, -0.5, 1., 0,0.,0],
               [ 0., 0., 0.,3., 0., 0],
               [ 0., 0., 0.,0., 3., 0],
               [ 0., 0., 0.,0., 0., 3.]],dtype=MuscatFloat)

        Vmatrix2D = np.array([[ 1.,-0.5,0.],
               [ -0.5, 1., 0.],
               [ 0., 0.,1.5],],dtype=MuscatFloat)

        VmatrixByDim={"2":Vmatrix2D,"3":Vmatrix3D}

        vmises = self.localValues
        AdjLoading = {}
        youngFieldbyElemNames,poissonFieldbyElemNames = materialFields
        dim = self.problem.cleanMesh.GetElementsDimensionality()

        for name,elems,idx in self.computationalElementFilter(self.problem.cleanMesh):
            sigmadata =  self.GetStressTensorData(sigma.data[name])
            vmisesdata = vmises.data[name]
            jprime = np.zeros_like(sigmadata)
            Ajprime = np.zeros_like(sigmadata)

            youngField = youngFieldbyElemNames[name].ravel()
            poissonField = poissonFieldbyElemNames[name].ravel()

            ngauss = sigmadata.shape[1]

            for el in idx:
                C = HookeIsoModified(youngField[el],poissonField[el],dim=dim)
                for ip in range(ngauss):
                    jprime[el,ip,:] = np.transpose(sigmadata[el,ip,:]) @ VmatrixByDim[str(dim)]
                    jprime[el,ip,:] *= self.alpha*(vmisesdata[el,ip].ravel()**(self.alpha-2))/(self.factor)**self.alpha
                    Ajprime[el,ip,:] = np.matmul(C,jprime[el,ip,:])

            AdjLoading[name] = - self.GetPreStressData(Ajprime)

        return ["PreStress"],[AdjLoading]

    def ComputeSensitivity(self,levelSet):
        """
        sigma(u) = A*e(u)

        -div(sigma(p)) = div( A*j'(sigma(u))                 // Omega
                p = 0                                      // Gamma_D
        (sigma(p))*n =  -A*j'(sigma(u)*n                     // Gamma_N U Gamma

        equivalent to

        int_Omega { \sigma(p) : \epsi (v) } = - int_Omega { A*j'(sigma(u) : \epsi(v) }

        """

        sigma = self.problem.GetAuxiliaryField(FN.stress,on=FN.IPField,onCleanMesh=True)
        vmises = self.localValues

        materialFieldsNames,materialFields = GetMaterialFields(support=self.problem.cleanMesh,
                                                                                phi=levelSet.phi,
                                                                                materials=self.adjointProblem.materials,
                                                                                levelset=levelSet,
                                                                                eVoid=self.problem.eVoid)

        AdjointLoadFieldName, AdjointLoadField = self.ComputeAdjointLoad(sigma,materialFields)

        self.UpdateAdjointProblem()

        self.adjointProblem.WriteSupport(self.problem.cleanMesh,
                                    cellFieldsNames = materialFieldsNames + AdjointLoadFieldName,
                                    cellFields=materialFields + AdjointLoadField )

        self.adjointProblem.SolveStandard()


        strain = self.adjointProblem.GetAuxiliaryField(FN.strain,on=self.localValueSupport,onCleanMesh=True)
        energy = IPField("energy",mesh=self.problem.cleanMesh,rule=vmises.rule)
        energy.Allocate()

        for elementName,elems,ids in ElementFilter(dimensionality=self.problem.cleanMesh.GetElementsDimensionality())(self.problem.cleanMesh):
            sigmadata = self.GetStressTensorData(sigma.data[elementName])
            straindata = self.GetStressTensorData(strain.data[elementName])

            ngauss= straindata.shape[1]
            energydata = np.zeros( (elems.GetNumberOfElements(),ngauss), dtype=MuscatFloat)
            for tet in ids:
                for ip in range(ngauss):
                    energydata[tet,ip] = ComputeSymMatrixProduct(sigmadata[tet,ip,:], straindata[tet,ip,:])

            energy.data[elementName] = energydata

        sensitivityIPField = (vmises/self.factor)**self.alpha + energy
        sensitivity = sensitivityIPField.GetPointRepresentation()

        sensitivity = (sensitivity*self.volume - np.ones_like(sensitivity)*self.internalintegral)/(self.volume)**2
        sensitivity *= (self.f_val**(1-self.alpha))*1.0/self.alpha

        fSensitivity_val = np.zeros(levelSet.support.GetNumberOfNodes() ,dtype=MuscatFloat)
        usedNodes = len(self.problem.cleanMeshToOriginalNodal)
        fSensitivity_val[self.problem.cleanMeshToOriginalNodal] = sensitivity[0:usedNodes]

        return fSensitivity_val

RegisterCriteriaClass("VonMises",TopoCriteriaVonMises,CreateTopoCriteriaVonMises)


def CreateTopoCriteriaNodalTargetDisp(ops):
    res = TopoCriteriaNodalTargetDisp()
    if "dir" in ops:
        res.dir = np.array(PH.ReadFloats(ops["dir"]))
        del ops['dir']
    if "u0" in ops:
        res.u0 = PH.ReadFloat(ops["u0"])
        del ops['u0']
    if "nTag" in ops:
        res.nTag = PH.ReadString(ops["nTag"])
        del ops['nTag']
    PH.ReadProperties(ops,ops.keys(),res)

    return res

class TopoCriteriaNodalTargetDisp(PhysicalCriteriaBase):

    def __init__(self, other=None):
        super(TopoCriteriaNodalTargetDisp, self).__init__(other)

        if other is None:
            self.SetName("NodalTargetDisp")
            self.nTag = None
            self.alpha = 2
            self.dir =None
            self.u0 = 0.
            self.f_val = -1
        else:
            self.nTag = other.nTag
            self.alpha = other.alpha
            self.dir = np.array(PH.ReadFloats(other.dir))
            self.u0 = other.u0
            self.f_val = other.f_val
            self.fSensitivity_val = np.copy(other.fSensitivity_val)

    def SetAuxiliaryQuantities(self,levelSet):
        self.problem.SetAuxiliaryFieldGeneration(FN.stress,on=FN.IPField)

    def UpdateValues(self,levelSet):
        """
        J(\Omega) = (\int_{nTag}  |u*dir -u0|^2 dx)
        """
        solution = self.problem.GetNodalSolution(onCleanMesh=True)

        #retrieve node coordinates from nTag
        assert self.nTag in self.problem.cleanMesh.nodesTags,"Nodal Tag "+self.nTag+" not found in mesh"
        ids = self.problem.cleanMesh.nodesTags[self.nTag].GetIds()
        assert len(ids)!=0,"Need a non empty nodal Tag "+self.nTag+" tu update values of Target Displacement criterion "
        errorMessage = "Nodal target displacement criterion: need to use Code_Aster to solve the physical problem"
        assert isinstance(self.problem, AsterStatic),errorMessage

        solutionatnode = solution[ids,:]
        disperror = (np.dot(solutionatnode,self.dir) - self.u0)[0]
        self.f_val = disperror**2
        self.fSensitivity_val = np.zeros_like(levelSet.phi)

        sigma = self.problem.GetAuxiliaryField(FN.stress,on=FN.IPField,onCleanMesh=True)
        elemtype = DesignSpaceSupportbyFESpaceName[self.problem.space]
        sigmadata = sigma.data[elemtype]

        ngauss= sigmadata.shape[1]

        if self.adjointProblem is None:
            self.SetAdjointProblemLikeDirect()
        self.adjointProblem.conform=self.problem.conform
        self.adjointProblem.space=self.problem.space
        self.adjointProblem.SetSupport(self.problem.originalSupport)
        self.adjointProblem.SetAuxiliaryFieldGeneration(FN.strain,on=FN.IPField)
        self.adjointProblem.templateFileName = self.problem.templateFileName
        self.adjointProblem.SetSolverInterface()
        self.adjointProblem.cleanMesh = self.problem.cleanMesh
        self.adjointProblem.cleanMeshToOriginalNodal = self.problem.cleanMeshToOriginalNodal
        self.adjointProblem.materials = self.problem.materials
        self.adjointProblem.dirichlet= self.problem.dirichlet
        self.adjointProblem.problems = self.problem.problems
        loadcase = np.array([0.,0.,0.],dtype=MuscatFloat)

        loadcase[:] = -2*disperror*self.dir
        self.adjointProblem.forcenodale= {"idx1": [[self.nTag,loadcase]]}

        self.adjointProblem.WriteSupport(self.problem.cleanMesh,
                                    phi = levelSet.phi)

        self.adjointProblem.residual = self.problem.residual
        self.adjointProblem.maxAllowedRelaxedResidual = self.problem.maxAllowedRelaxedResidual

        self.adjointProblem.SolveWithRelaxedResidual()

        strain = self.adjointProblem.GetAuxiliaryField(FN.strain,on=FN.IPField,onCleanMesh=True)
        straindata = strain.data[elemtype]

        tetra = self.adjointProblem.cleanMesh.GetElementsOfType(elemtype)

        sensitivityAtTetra = np.zeros( (tetra.GetNumberOfElements(),ngauss), dtype=MuscatFloat)

        for tet in range(tetra.GetNumberOfElements()):
            for ip in range(ngauss):
                prod = np.dot(sigmadata[tet,ip,:][0:3],straindata[tet,ip,:][0:3])+2*np.dot(sigmadata[tet,ip,:][3:],straindata[tet,ip,:][3:])

                sensitivityAtTetra[tet,ip] = prod


        ipfield = IPField("sensitivity",mesh=self.adjointProblem.cleanMesh,ruleName=self.problem.space)
        ipfield.Allocate()
        ipfield.rule[elemtype] = strain.rule[elemtype]
        ipfield.data[elemtype] = sensitivityAtTetra

        sensitivity = ipfield.GetPointRepresentation()

        self.fSensitivity_val = np.zeros(levelSet.support.GetNumberOfNodes() ,dtype=MuscatFloat)
        usedNodes = len(self.problem.cleanMeshToOriginalNodal)
        self.fSensitivity_val[self.problem.cleanMeshToOriginalNodal] = sensitivity[0:usedNodes]

        return RETURN_SUCCESS

    def GetNumberOfSolutions(self):
        return 1

    def GetCriteriaSolution(self,i):
        return self.problem.GetNodalSolution()

    def GetCriteriaSolutionName(self,i):
        return "Dep"

RegisterCriteriaClass("NodalTargetDisp", TopoCriteriaNodalTargetDisp,CreateTopoCriteriaNodalTargetDisp)



def CheckIntegrity(GUI=False):
    b = TopoCriteriaNodalTargetDisp()
    print(b)
    c = TopoCriteriaVonMises()
    print(c)
    return "OK"

if __name__ == "__main__":# pragma: no cover
    print(CheckIntegrity())
