****************************************
Introduction
****************************************

OpenPisco is a research and development platform devoted to topology optimization relying on the level set method.
Its aims are the following:

* Improve the understanding of the numerical aspects inherent to topology optimization with level set
* Enable a fast prototyping of the developments driven by industrial/research needs
* Provide a user interface for several external tools used in the context of topology optimization

As such, its main characteristics are:

* Modularity: assembling of weakly coupled and highly cohesive brick. For instance, the physical solvers or remeshing tool can be used independently without any concern about topology optimization related aspect whatsoever
* Interoperability: easy to replace a brick by another (physical solver, optimization algorithm, optimization criteria)
* Extensibility: the overall modularity is driven by the possibility to adapt to futures needs. In particular, a plugins-like infrastructure enables a user to use his own functionalities (be it reading a mesh, use a different optimization algorithm or even provide a specific physical analysis) within the platform without interacting with the existing source code

Considering that Python remains quite user-friendly, a lof of developer and researcher are familiar with this language nowadays and that it is possible to rely on Numpy and Scipy to obtain acceptable performances, the platform is written almost exclusively in Python. The Python library provided by OpenPisco is named OpenPisco.
Still, note that the platform does rely on other tools not necessarily in Python nor even at least providing a Python API (e.g. finite element solvers).

Features/capabilities
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

OpenPisco provides the following components

* An algorithmic toolbox specialized in the treatment of level sets
* A generic interface to several Finite Element solvers, where various types of physical analysis are supported
* Algorithms specialized in the resolution of constrained optimization problems
* A collection of physical/geometrical optimization criteria
* An algorithmic toolbox dedicated to handle generic topology optimization problems
* An advanced interface to the remeshing tool mmg3d
* A graphic user interface


User interfaces
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
OpenPisco provides three user interfaces, ranked from the highest level to the lowest level.
The first and second one rely on terminal command line run while the last one is graphical.

Python API
------------------------------------------------------------------------------------------------------------------------------------------------
The lowest level API available. For an end user, it might not even be necessary to go that low, the GUI and CLApp should be enough. However, for future potential contributors and developers, it is a mandatory step.

OpenPiscoCL
------------------------------------------------------------------------------------------------------------------------------------------------
OpenPisco provides a DSL format allowing to easily create a new study or modify an existing one.

The terminal command line application OpenPiscoCL parses an xml-like formated file (i.e. organized with xml tags), describing the operations in an declarative way and execute them.
OpenPiscoCL is built on top of the Python API.
For more details about the actual use of OpenPiscoCL, we refer to the :doc:`dedicated documentation <OpenPiscoApplications>` and tutorials.

OpenPisco (GUI)
------------------------------------------------------------------------------------------------------------------------------------------------
The OpenPisco GUI is the most user-friendly way to run an optimization problem.
The GUI allows the user to visualize the 3D scene without installing external visualization tools.
You can load an existing study through a script using OpenPisco DSL.


.. figure:: images/PiscoGUI.PNG
    :width: 600px
    :align: center
    :height: 300px
    :alt: alternate text
    :figclass: align-center

    OpenPisco (GUI) overview


.. Visualization
.. """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.. Should we say something about suitable external tools (Paraview) here for those who don't use the GUI?
.. What about the pluggin for parawiew? Should it be in the repository? Should it be explained even further?


.. Key points
.. """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.. This R&D platform aims to enphasize the following points:

.. - Modularity: assembly of briques weakly coupled and strongly cohesive
.. - Interoperability: easy remplacement of a component by another
.. - Adaptability: platform devoted to topology optimization but most components can be used independtly
.. - Extensibility: relying on a plugins infrastructure, a user can use his own routines (mesh reading, optimisation algorithm, physical solver study) without modifying the source code
