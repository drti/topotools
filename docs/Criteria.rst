##########
Criteria
##########

Criteria are elements of the :doc:`optimization problem <OptimizationProblem>`.

The module :py:meth:`OpenPisco.Optim.Criteria` collects criteria available in OpenPisco.

Each criterion must be able to evaluate both the cost function value and sensitivity on a given optimization state. Thus, each criterion has three basic functions

* :py:func:`OpenPisco.Optim.Criteria.Criteria.UpdateValues`
* :py:func:`OpenPisco.Optim.Criteria.Criteria.GetValue`
* :py:func:`OpenPisco.Optim.Criteria.GetSensitivity`

There are two main classes of criteria: physical and geometrical criteria.


Physical criteria
==================

A physical criterion needs to run a physical analysis (here, a finite element analysis) to evaluate values and sensitivities on the current optimization point.
A base class describing physical criteria can be found in the following module

* :py:meth:`OpenPisco.Optim.Criteria.PhysicalCriteriaBase`

A physical criterion must have a mandatory attribute :py:attr:`OpenPisco.Optim.Criteria.problem` which is a physical solver class. Please read the :doc:`physical solver documentation <PhysicalSolvers>` for an overview of the physical solvers capabilities.
Note that some physical criteria need to solve an adjoint state to evaluate the sensitivity on the current levelset. In some cases, the computation of the adjoint state can repose on external finite element solvers interfaced with OpenPisco.

The physical criteria are collected in the following submodules

* :py:meth:`OpenPisco.Optim.Criteria.PhyCriteria`
* :py:meth:`OpenPisco.Optim.Criteria.PhyMecaCriteria`
* :py:meth:`OpenPisco.Optim.Criteria.PhyModalCriteria`
* :py:meth:`OpenPisco.Optim.Criteria.PhyHarmonicCriteria`
* :py:meth:`OpenPisco.Optim.Criteria.PhyBucklingCriteria`
* :py:meth:`OpenPisco.Optim.Criteria.PhyThermalCriteria`
* :py:meth:`OpenPisco.Optim.Criteria.PhyRobustCriteria`


In order to run as expected, a physical criterion must be able to compute auxiliary scalars and fields needed to evaluate the current state.
The auxiliary quantities needed by each criterion are detailed below.
See `FieldNames`_ for more details about auxiliary quantities names.

Elastic compliance
-------------------

This criterion evaluates the elastic compliance of the negative part of the level set in linear elasticity

.. math::

    J(\Omega) = \int_{\Omega}{\sigma(u):\varepsilon (u)\:dx}

where the quantities  :math:`\sigma(u)` and :math:`\varepsilon (u)` are related to a linear elasticity analysis.
See the :doc:`documentation <PhysicalSolvers>` for details about the associated physical analysis.

In order to run as expected, the :py:attr:`OpenPisco.Optim.Criteria.problem` class must be able to compute the following auxiliary quantities :

* potential_energy
* int_potential_energy on nodes

This criterion does not need an adjoint solver to update its values.

See also :py:class:`OpenPisco.Optim.Criteria.PhyCriteria.TopoCriteriaCompliance`.


Von Mises stress
------------------

This criterion evaluates the von Mises equivalent stress of the negative part of the level set in linear elasticity.
Since stress measurements are intrinsically of local nature, we rely here on a popular aggregation technique.
Therefore, we consider the following formulation based on the p-norm of the von Mises stress

.. math::

    J(\Omega) = \big( \frac{ \int_{\Omega}{ \sigma_{vm}^\alpha  \:dx} }{ \int_{\Omega}{ \:dx}  }\big)^{\frac{1}{\alpha}}

where :math:`\alpha` is a real positive parameter and :math:`\sigma_{vm}` is the Von Mises Stress related to a linear elasticity analysis.
See :doc:`physical solver <PhysicalSolvers>` for details about the associated physical analysis.

The :py:attr:`OpenPisco.Optim.Criteria.problem` solver must be able to compute the following auxiliary quantities :

* stress     on integration points
* von_mises  on nodes
* von_mises  on integration points

The adjoint state computation reposes on the solver Code_Aster.

The attribute :py:attr:`OpenPisco.Optim.Criteria.PhyMecaCriteria.TopoCriteriaVonMises.alpha` corresponds to the weight :math:`\alpha` appearing in the criterion functional.

It is possible to exclude a subregion from the computation of the integral value of the criterion. In order to do so, the attribute :py:attr:`OpenPisco.Optim.Criteria.PhyMecaCriteria.TopoCriteriaVonMises.offZone` must be initialized with an implicit geometry representing the zone to exclude.
The ImplicitGeometry module of the library muscat [1]_ is used to define specific zones.

See also :py:class:`OpenPisco.Optim.Criteria.PhyMecaCriteria.TopoCriteriaVonMises`.

Target displacement
----------------------

This criterion evaluates the discrepancy between the actual and target displacements in linear elasticity

.. math::

   J(\Omega) = |u_{nTag}*dir -u_0|^2


where :math:`u_{nTag}` is the elastic displacement on a mesh node specified by the nodal tag nTag, dir is an unit vector (direction) and :math:`u_0` is a target scalar value.
See the :doc:`documentation <PhysicalSolvers>` for details about the associated physical analysis.
The :py:attr:`OpenPisco.Optim.Criteria.problem` solver must be able to compute the following auxiliary quantities :

* stress     on integration points

The adjoint state computation reposes on the solver Code_Aster.

See also :py:class:`OpenPisco.Optim.Criteria.PhyMecaCriteria.TopoCriteriaNodalTargetDisp`.


k-th eigenfrequency
----------------------

This criterion aims at maximizing the k-th eigenfrequency of the negative part of the level set. In order to to so, the following functional is considered

.. math::

    J_k(\Omega)= -\omega_k^2.

where :math:`\omega_k > 0` is the k-th eigenpulsation. See the :doc:`documentation <PhysicalSolvers>` for details about the associated physical analysis.

The :py:attr:`OpenPisco.Optim.Criteria.problem` class must be able to compute the following auxiliary quantities :

* FN.EigenFreqSquared_sensitivity
* FN.EigenFreqSquared

This criterion does not need an adjoint solver to update its values.

See also :py:class:`OpenPisco.Optim.Criteria.PhyModalCriteria.TopoCriteriaModal`.


Effective radiated power (ERP)
------------------------------

This criterion evaluates the structure-borne noise to obtain an insight regarding the radiation of
components and panels for specific excitations, in the context of viscoelasticity.

.. math::

    J(\Omega) = \frac{1}{\omega_{max}-\omega_{min}}\int_{\omega_{min}}^{\omega_{max}}  \frac{1}{2} f \rho_f c_f \int_{S^e_k} |(\dot{u}_\Re +i \dot{u}_\Im)\cdot n|^2 \:ds \:d\omega,

See the :doc:`documentation <PhysicalSolvers>` for details and notations about the associated physical analysis.

The :py:attr:`OpenPisco.Optim.Criteria.problem` class must be able to compute the following auxiliary quantities :

* ERP_sensitivity        on Nodes
* ERP_density            on Nodes
* dominant_SolutionMode  on Nodes
* ERP
* ModalCumulMassFractiondX
* ModalCumulMassFractiondY
* ModalCumulMassFractiondZ
* ModalLastEnergyRatio

The adjoint state computation reposes on the solver Code_Aster.

See also :py:class:`OpenPisco.Optim.Criteria.PhyHarmonicCriteria.TopoCriteriaERP`.


Target temperature
----------------------

This criterion aims at controlling a global temperature indicator of the negative part of the level set.

.. math::
    J(\Omega)  = \int_{\Omega}  max(T-T_s,0.)^2

where  :math:`T(x)` is the calculated temperature field and :math:`T_s` is a scalar value equal to the temperature at solidus state.
See the :doc:`documentation <PhysicalSolvers>` for details and notations about the associated physical analysis.
See [4]_ for the theoretical background.


The :py:attr:`OpenPisco.Optim.Criteria.problem` class must be able to compute the following auxiliary quantities :

* temperature        on Nodes

The adjoint state computation reposes on the solver Code_Aster.

See also :py:class:`OpenPisco.Optim.Criteria.PhyThermalCriteria.TopoCriteriaTargetTemperature`.


Buckling
------------------

This criterion aims at avoiding linear buckling instabilities.

.. math::
  J(\Omega)=\gamma^{-1}(\Omega)

where :math:`\gamma` is the critical load coefficient.



The :py:attr:`OpenPisco.Optim.Criteria.problem` class must be able to compute the following auxiliary quantities :

* load_crit
* elastic_energy_buckling  on Centroids
* stress                   on Centroids
* strain_green_buckling    on Centroids
* strain_buckling          on Centroids
* mode_buckling            on FN.Nodes
* von_mises                on FN.Nodes

The adjoint state computation reposes on the solver Code_Aster.

See also :py:class:`OpenPisco.Optim.Criteria.PhyBucklingCriteria.TopoCriteriaBuckling`.



Worst-case elastic compliance
-------------------------------

This criterion evaluates the elastic compliance of the negative part of the level set in the context of linear elasticity under the assumption of uncertainty over surfacic loads.

See [2]_ for the theoretical background.

The :py:attr:`OpenPisco.Optim.Criteria.problem` class must be able to compute the following auxiliary quantities :

* elastic_energy on Nodes
* int_elastic_energy
* stress on Centroids

The adjoint state computation reposes on the solver Code_Aster.

See also :py:class:`OpenPisco.Optim.Criteria.PhyRobustCriteria.TopoCriteriaComplianceRobustLoadWorstCase`.


Geometrical criteria
====================

Geometrical criteria only need informations about the geometry of the shape to evaluate value and sensitivity on a given optimization point.
Thus, in most cases, these criteria do not need to call a finite element solver.
The geometrical criteria are collected in the following submodules

* :py:meth:`OpenPisco.Optim.Criteria.GeoCriteria`
* :py:meth:`OpenPisco.Optim.Criteria.GeoThicknessCriteria`


Volume
------------------

This criterion evaluates the volume of the negative part of the current level set.

.. math::

    J(\Omega) = \int_{\Omega}{ dx}

See also :py:class:`OpenPisco.Optim.Criteria.GeoCriteria.TopoCriteriaVolume`

Mass
------------------

This criterion evaluates the mass of the negative part of the current level set.

.. math::

    J(\Omega) = \rho \int_{\Omega}{ dx}

where :math:`\rho` is the constant density.

See also :py:class:`OpenPisco.Optim.Criteria.GeoCriteria.TopoCriteriaMass`.

Minimal thickness
------------------

This criterion evaluates the minimal thickness of the negative part of the current level set.

.. math::

     J(\Omega) = \int_{ \partial \Omega}{ \int_{0}^{d_{min}}{ \left[ ( d_{\Omega}(s - z n(s)))^+ \right]^2 } \:dz \:dx},



where :math:`d_{\Omega}` is the signed distance function, :math:`d_{min}` is the desired minimal thickness and :math:`n` is the normal vector.

See [3]_ for the theoretical background.

See also :py:class:`OpenPisco.Optim.Criteria.GeoThicknessCriteria.TopoCriteriaMinThickness`.


Maximal thickness
------------------

This criterion evaluates the maximal thickness of the negative part of the current level set.

.. math::

    J(\Omega) = \int_{\Omega}{\left[ \left( d_{\Omega} (x) + \frac{d_{max}}{2} \right)^{-}\right]^2 \:dx}.


where :math:`d_{\Omega}` is the signed distance function and :math:`d_{max}` is the desired maximal thickness.

See [3]_ for the theoretical background.


See also :py:class:`OpenPisco.Optim.Criteria.GeoThicknessCriteria.TopoCriteriaMaxThickness`.




Criteria operations
====================
It is possible to apply unary or binary operations on the set of available criteria.

The available unary operations are the following

* :py:class:`OpenPisco.Optim.Criteria.Criteria.CriteriaFactor`
* :py:class:`OpenPisco.Optim.Criteria.Criteria.Weighted`
* :py:class:`OpenPisco.Optim.Criteria.Criteria.PolynomialVanishingCriteria`
* :py:class:`OpenPisco.Optim.Criteria.Criteria.CriteriaPower`

The available binary operations are the following

* :py:class:`OpenPisco.Optim.Criteria.Criteria.CriteriaRatio`



.. [1] https://gitlab.com/drti/muscat
.. [2] G. Allaire, Ch. Dapogny, A linearized approach to worst-case design in parametric and geometric shape optimization, M3AS Vol. 24, No. 11 (2014) 2199-2257
.. [3] G. Allaire, F. Jouve, G. Michailidis, Thickness control in structural optimization via a level set method, SMO 53, 1349-1382 (2016)
.. [4] G. Michailidis, Manufacturing constraints and multi-phase shape and topology optimization via a level-set method. Diss. Ecole Polytechnique X (2014)
