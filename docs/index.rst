.. This file is subject to the terms and conditions defined in
   file 'LICENSE', which is part of this source code package.

Welcome to OpenPisco's documentation!
===========================================


.. figure:: images/OpenPiscoLogo.png
    :width: 90%
    :align: center
    :alt: OpenPisco Logo
    :figclass: align-center


Contents:
=========
.. toctree::
   :maxdepth: 4

   Intro
   Tutorials
   Installation
   OpenPiscoApplications
   SoftwareDesign
   TopologyOptimization
   LevelSet
   Remeshing
   LagrangianMeshDisplacement
   OptimizationProblem
   OptimizationAlgorithms
   PhysicalSolvers
   Criteria
   InterfaceYourOwnOptimizationAlgorithm
   InterfaceYourOwnPhysicalSolver

OpenPisco Package Tree
======================

.. toctree::
   :maxdepth: 4

   _source/OpenPisco

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
