****************************************
Software Design
****************************************

Interactions between macroscopic components
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

OpenPisco provides three main components:

* The optimization engine, used to drive the optimization iterations
* The level set engine, to handle the evolution and the computation of the level set function :math:`\phi`
* A collection of various criteria of interest, in the context of topology optimization

The figure below describes the interactions between this components from a macroscopical point of view.

.. figure:: images/MacroWorkflow.png
    :width: 660px
    :align: center
    :height: 264px
    :alt: alternate text
    :figclass: align-center

    Interactions between OpenPisco main components

Focus on criteria
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Within OpenPisco semantic, a criterion take as an input a levelSet and should be able to:

* Compute its value
* Compute its associated sensitivity

.. figure:: images/criteriaAbstractAPI.png
    :width: 660px
    :align: center
    :height: 138px
    :alt: alternate text
    :figclass: align-center

    Inner API used when updating criteria

Note that:

* When a criterion is used within an optimization problem, it makes no difference whether it is used as the objective or a constraint.
* Each criterion defines its internal physical simulator and potential adjoint solvers, if required.

RST directory Tree
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

We introduce briefly the repositories within the module and refer to the respective documentation for more details.
The library is organized as follows

.. code-block:: bash

    OpenPisco
    ├── Actions
    ├── MuscatExtentions
    ├── CLApp
    ├── Demos
    ├── ExternalTools
    │   ├── Code_Aster
    │   ├── FreeFem
    │   ├── Zset
    ├── Optim
    │   ├── Algorithms
    │   ├── Criteria
    │   ├── Problems
    ├── PhysicalSolvers
    ├── QtApp
    ├── Structured
    ├── TestData
    ├── Unstructured

Actions
--------------------------------------------------------------------------------------------------------------------------------
It is a collection of encapsulated operations used frequently to run a topology optimization problem with level sets but not exclusively.

MuscatExtentions
--------------------------------------------------------------------------------------------------------------------------------
Its content and features are somehow similar to Muscat [1]_, there are mainly:

* Support for the MED format, used with Code_Aster in particular
* Support for Code_Aster integration scheme
* Mesh quality related tools

CLApp
--------------------------------------------------------------------------------------------------------------------------------
Implementation of OpenPisco Domain Specific Language (DSL) based on a XML-like syntax.

Demos
--------------------------------------------------------------------------------------------------------------------------------
Collection of topology optimization examples from the scientific literature

ExternalTools
--------------------------------------------------------------------------------------------------------------------------------
Scripts related to the external tools used within the platform, mainly the physical solvers.
For instance, this is where one can find:

- For Code_Aster, the .comm and .export files
- For FreeFem, some .edp files

The core implementation of the physical analysis supported by the platform and relying on each solver DSL are there.

Optim
------------------------------------------------------------------------------------------------------------------------------------------------
As one could infer from the subdirectories name, there are:

* The optimization algorithms to pilot the optimization process
* The optimization criteria, both physical and geometrical, to evaluate a shape
* The optimization problem, to encapsulate all the components inherent to the optimization process (design space, objective, constraints...)

PhysicalSolvers
--------------------------------------------------------------------------------------------------------------------------------
Implementation of the physical analysis available within the platform using the external solvers (Code_aster,FreeFem, Zset) or internal solver (Muscat Solver [1]_).

QtApp
--------------------------------------------------------------------------------------------------------------------------------
Implementation of the OpenPisco Graphic User Interface

Structured
--------------------------------------------------------------------------------------------------------------------------------
Various scripts to handle level set on structured meshes

TestData
--------------------------------------------------------------------------------------------------------------------------------
Some data

Unstructured
--------------------------------------------------------------------------------------------------------------------------------
Script related to level set operations for unstructured meshes. That includes, in particular:

* The redistanciation
* The remeshing tools
* The advection


.. Diagrams
.. """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.. Because one picture is worth a thousand words.

.. Activity diagrams
.. --------------------------------------------------------------------------------------------------------------------------------
.. The following diagram describes the dynamical aspects of the platform;
.. its aim is to model the flow from one activity to another activity within the toplogy optimization procedure.

.. .. figure:: images/algoOptim.PNG
..     :width: 600px
..     :align: center
..     :height: 800px
..     :alt: alternate text
..     :figclass: align-center

..     Activity UML diagram of merit-function based optimization process

.. Note that the algorithm depicted here rely on a merit function for the evaluation, in order to assess whether the current shape is acceptable.

.. Communication diagram
.. --------------------------------------------------------------------------------------------------------------------------------

.. You can find below a communication diagram displaying the Interactions
.. between the modules related to topology optimization with the body-fitted approach.

.. .. figure:: images/architecture.PNG
..     :width: 700px
..     :align: center
..     :height: 350px
..     :alt: alternate text
..     :figclass: align-center

..     Communication UML diagram of OptimAlgoNullSpace.

.. Each block can be associated to actual modules within the platform.
.. For more information about either of these blocks, we refer to the dedicated documentation:

.. - Optimization algorithm (link to add)
.. - Optimization problem (link to add)
.. - Criteria (link to add)
.. - Physical Solvers (link to add)
.. - Level set (link to add)
.. - Remeshing (link to add)


.. [1] https://gitlab.com/drti/muscat
