############################
Lagrangian mesh displacement
############################

The library OpenPisco provides a Python interface to the remeshing software mmg [1]_ .
The interface allows to perform lagrangian mesh displacement while keeping the mesh connectivity fixed.
See the dedicated module :py:meth:`OpenPisco.Unstructured.MmgMeshMotion`.
This feature allows to deform a mesh with respect to a vector field defined on an surface interface.
In a shape optimization context this functionality allows to update the shape without the need of advection and remeshing.
The feature is activated with the option **-lag**. See the mmg documentation [2]_ for more details about this feature.

The following function contains an exemple of mesh displacement using the Python API

* :py:func:`OpenPisco.Unstructured.MmgMeshMotion.CheckIntegrity`


.. [1] https://www.mmgtools.org/
.. [2] https://www.mmgtools.org/mmg-remesher-try-mmg/mmg-remesher-tutorials/mmg-remesher-mmg3d/lagrangian-movement
