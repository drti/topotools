##########
Remeshing
##########

OpenPisco provides a Python interface to the remeshing software mmg [1]_ with pre-and post-processing routines.
Mmg allows to remesh

* unstructured thetrahedral meshes (mmg3d)
* surfacic triangular meshes (mmgs)
* simplicial meshes in 2d (mmg2d)

The interface is available in the dedicated module :py:meth:`OpenPisco.Unstructured.MmgMesher`.


Overview
=======================

A set of parameters and options is provided by the user to drive the remeshing process :

* **hmin**     minimal desired element size
* **hmax**     maximal desired element size
* **hausd**    geometrical quality of surfaces
* **hgrad**    desired size variation between adjacent elements
* **-opnbdy**  preservation of internal simplicial elements of dimension 2
* **-nr**      desactivation of ridges detection

Starting from these global user requirements, mmg3d improves the mesh quality performing local mesh operations.

Two additional remeshing functionalities are available : level set conformal remeshing and classical adaptive remeshing.

Level set remeshing
=======================

* :py:func:`OpenPisco.Unstructured.MmgMesher.MmgMesherActionLevelSet`


This functionality allows to generate an explicit mesh of an implicit domain.
See also the web documentation [2]_ of mmg3d.

A set of parameters and options is provided by the user to drive the remeshing process :

* **-ls [isovalue]**      activation of the level set mode (the default isovalue is 0)
* **-rmc**                deletion of connected components whose volumic fraction is smaller than a given threshold

The level set split preserves initial subregions specified in the input mesh. Each subregion is splitted into two parts corresponding respectively to positive and negative parts of the level set.
Thus, new mesh tags are added to the output

* **Outside3D**       dimension 3 elements belonging to the positive part of the level set
* **Inside3D**        dimension 3 elements belonging to the negative part of the level set
* **InterSurf**       triangles belonging to the level set conformal isosurface
* **InterBars**       bars belonging to the level set on the bounding box

The following minimal working examples illustrate the conformal remeshing using the Python API

* :py:func:`OpenPisco.Unstructured.MmgMesher.CheckIntegrity_LevelSetRemeshingWithMetric`
* :py:func:`OpenPisco.Unstructured.MmgMesher.CheckIntegrity_LevelSetRemeshingWithLocalParams`


Adaptive remeshing
===================

* :py:func:`OpenPisco.Optim.Criteria.Criteria.MmgMesherActionRemesh`

This routine performs mesh adaptation with respect to a given metric field.
The metric (isotropic or anisotropic) is a size field defined at mesh nodes specifing the desired local size.


It is also possible to combine the mesh adaptation with the conformal remeshing in a sole remeshing operation. To do so, the user must provide a distance field with the option **-ls** and a metric field with the option **-met** .

The following function contains two minimal working examples of adaptive remeshing using the Python interface:

* :py:func:`OpenPisco.Unstructured.MmgMesher.CheckIntegrity_LevelSetRemeshingWithMetric`


Required entities
===================

Mesh entities which must be preserved during remeshing should be labeled with the keyword "Required"

* **RequiredVertices**      nodal tag
* **RequiredEdges**         element tag (bars)
* **RequiredTriangles**     element tag (triangles)

Elements of dimension 0 ( of type "Point0") are automatically preserved during a remeshing operation.

Overlapping mesh tags
========================

Dedicated pre-and post-processing routines allow to preserve overlapping volumic tags during remeshing.



.. [1] https://www.mmgtools.org/
.. [2] https://www.mmgtools.org/mmg-remesher-try-mmg/mmg-remesher-tutorials/mmg-remesher-mmg3d/mmg-remesher-implicit-domain-meshing

